using System;
using Microsoft.SPOT;

namespace BioProject1._4.PalmSens
{
    class PSCommon
    {
        static const char[] HexTable = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        unsafe void ToHex(byte inp, char* buf)
        {
            buf[0] = HexTable[(inp >> 4) & 0xF];
            buf[1] = HexTable[inp & 0xF];
        }

        unsafe byte FromHex(char* inp)
        {
            byte ret = 0;
            if (inp[0] >= 'A' && inp[0] <= 'F')
                ret += (byte)(inp[0] - 'A' + 10);
            else
                ret += (byte)(inp[0] - '0');

            ret <<= 4;

            if (inp[1] >= 'A' && inp[1] <= 'F')
                ret += (byte)(inp[1] - 'A' + 10);
            else
                ret += (byte)(inp[1] - '0');

            return ret;
        }
    }
}