using System;
using Microsoft.SPOT;
using System.Collections;
using System.Threading;
using GHI.Glide;
using BioProject1._4.GUIs;
using BioProject1._4.Utils;
using BioProject1._4.RemoteControl;
using GHI.Hardware;
using GHI.Premium.Hardware;
using GHI.Premium.System;
using System.IO;
using Microsoft.SPOT.Presentation;

namespace BioProject1._4.ProgramHandeling
{

    class Stage
    {
        private Object thisLock = new Object();
        int m_iStageNum;
        int[] m_iObjectNmbers;
        string m_sResistors;
        string[] m_sTunnels;
        int m_iRunTime;
        int m_iDelayTime;
        string m_sScenarioType;
        bool m_bCreateImage;
        bool m_bIsStageRunning;
        GadgeteerAsACleint ClientInstance;
        public Stage(int iStageNumber, string[] arr_ObjType, string sPressure, string[] sTunnels, int iRunTime, int iDelayTime, string sScenarioType, bool bCreateImage)
        {
            m_iStageNum = iStageNumber;
            m_iObjectNmbers = new int[arr_ObjType.Length];
            for (int i = 0; i < arr_ObjType.Length; i++)
            {
                m_iObjectNmbers[i] = Convert.ToInt32(arr_ObjType[i]);
            }
            m_sScenarioType = sScenarioType;
            m_sResistors = sPressure;
            m_sTunnels = sTunnels;
            m_iRunTime = iRunTime;
            m_iDelayTime = iDelayTime;
            m_bCreateImage = bCreateImage;
            m_bIsStageRunning = false;
            ClientInstance = GadgeteerAsACleint.Instance("", 0);
        }
        public int getCurrentSwitch()
        {
            //TODO
            return m_iObjectNmbers[0];
        }

        public bool run(Queue imageQ)
        {
      //      lock (thisLock)
      //      {

            m_bIsStageRunning = true;
                string[] arr_resist = m_sResistors.Split(':');
                for (int i = 0; i < arr_resist.Length; i++)
                {
                    Program.TestingTool.getResistors()[i].setTap(Convert.ToInt32(arr_resist[i]));
                }

                int[][] jArrTunnels = new int[m_sTunnels.Length][];

                for (int i = 0; i < m_sTunnels.Length; i++)
                {
                    string[] arr_tempTunnels = m_sTunnels[i].Split(',');
                    jArrTunnels[i] = new int[arr_tempTunnels.Length];
                    for (int j = 0; j < arr_tempTunnels.Length; j++)
                    {
                        jArrTunnels[i][j] = Convert.ToInt32(arr_tempTunnels[j]);
                    }
                }

                if (m_sScenarioType.Equals("Manifold"))
                {
                    for (int iManifoldNum = 0; iManifoldNum < m_iObjectNmbers.Length; iManifoldNum++)
                    {
                        for (int iTunnelNum = 0; iTunnelNum < Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].getNumOfTunnels(); iTunnelNum++)
                        {
                            Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].setTunnelStatus(iTunnelNum, false);
                        }
                        for (int iTunnelNum = 0; iTunnelNum < jArrTunnels[iManifoldNum].Length; iTunnelNum++)
                        {
                            Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].setTunnelStatus(jArrTunnels[iManifoldNum][iTunnelNum], true);
                        }
                    }
                    GUITestingManifolds.showDisabled(); //  Glide.MainWindow = GUITestingManifolds.showDisabled();
                    //Show window TODO
                }
                else if (m_sScenarioType.Equals("Switchboard"))
                {
                    for (int iSwitchNum = 0; iSwitchNum < m_iObjectNmbers.Length; iSwitchNum++)
                    {
                        for (int iTunnelNum = 0; iTunnelNum < jArrTunnels[iSwitchNum].Length; iTunnelNum++)
                        {
                            Program.TestingTool.getSwitchboard()[m_iObjectNmbers[iSwitchNum]].setTunnelStatus(jArrTunnels[iSwitchNum][iTunnelNum], true);
                        }
                    }
                    //TODO
                    // Glide.MainWindow = GUITestingSwitchboards.showDisabled(m_iObjectNmbers[0], jArrTunnels[0], true);
                    GUITestingSwitchboards.showDisabled(m_iObjectNmbers[0], jArrTunnels[0], true);
                }
                Thread.Sleep(m_iRunTime);
                ////////////////Image Handleing//////////////////////////

                ////////////////////////////////////////////////////////
                if (m_iDelayTime <= 0)
                {
                    if (this.m_bCreateImage)
                    {
                        //Replaced
                        //imageQ.Enqueue(new ImageObj(takePicture(), this.m_iStageNum));
                        ClientInstance.startSession(Definitions.CommandsToRasp.enTakePicture);
                    }
                    return true;
                }
                if (m_sScenarioType.Equals("Manifold"))
                {
                    for (int iManifoldNum = 0; iManifoldNum < m_iObjectNmbers.Length; iManifoldNum++)
                    {
                        //       for (int iTunnelNum = 0; iTunnelNum < Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].getNumOfTunnels(); iTunnelNum++)
                        //      {
                        //         Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].setTunnelStatus(iTunnelNum, true);
                        //       }
                        for (int iTunnelNum = 0; iTunnelNum < jArrTunnels[iManifoldNum].Length; iTunnelNum++)
                        {
                            Program.TestingTool.getManifolds()[m_iObjectNmbers[iManifoldNum]].revertToInitialState();//.setTunnelStatus(jArrTunnels[iManifoldNum][iTunnelNum], false);
                        }
                    }
                    GUITestingManifolds.showDisabled(); // Glide.MainWindow = GUITestingManifolds.showDisabled();
                }
                else if (m_sScenarioType.Equals("Switchboard"))
                {
                    for (int iSwitchNum = 0; iSwitchNum < m_iObjectNmbers.Length; iSwitchNum++)
                    {
                        for (int iTunnelNum = 0; iTunnelNum < jArrTunnels[iSwitchNum].Length; iTunnelNum++)
                        {
                            Program.TestingTool.getSwitchboard()[m_iObjectNmbers[iSwitchNum]].setTunnelStatus(jArrTunnels[iSwitchNum][iTunnelNum], false);
                        }
                    }
                    //TODO
                    //     Glide.MainWindow = GUITestingSwitchboards.showDisabled(m_iObjectNmbers[0], jArrTunnels[0], true);
                    GUITestingSwitchboards.showDisabled(m_iObjectNmbers[0], jArrTunnels[0], true);
                }
                m_bIsStageRunning = false;    
                if (this.m_bCreateImage)
                {
                    //Replaced
                    //imageQ.Enqueue(new ImageObj(takePicture(), this.m_iStageNum));
                    ClientInstance.startSession(Definitions.CommandsToRasp.enTakePicture);
                }
                Thread.Sleep(m_iDelayTime);
                /*
                int[] arr_changedTunnels = new int[arr_Tunnels.Length];
                for (int i = 0; i < arr_Tunnels.Length; i++)
                {
                    arr_changedTunnels[i] = Convert.ToInt32(arr_Tunnels[i]);
                    Program.RunningTool.getSwitchboard()[m_iSwitchbaord].setTunnelStatues(arr_changedTunnels[i], true);
                }
                Glide.MainWindow = GUITestingSwitchboards.showDisabled(m_iSwitchbaord, arr_changedTunnels, true);
                Thread.Sleep(m_iRunTime);
                for (int i = 0; i < arr_Tunnels.Length; i++)
                {
                    Program.RunningTool.getSwitchboard()[m_iSwitchbaord].setTunnelStatues(arr_changedTunnels[i], false);
                }
                Glide.MainWindow = GUITestingSwitchboards.showDisabled(m_iSwitchbaord, arr_changedTunnels, true);
                Thread.Sleep(m_iDelayTime);
                */
            
            return false;
        }

        /*
        public static byte[] takePicture()
        {
            Debug.Print("Save Image Procces Start");
            Bitmap LCD = new Bitmap(SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            // if (!Program.camera.isNewImageReady) ==> this should be in while -loop until image is ready
            Program.camera.StartStreaming();
            while (!Program.camera.isNewImageReady)
            {
                Thread.Sleep(100);
            }
            Program.camera.DrawImage(LCD, 0, 0, SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            Program.camera.StopStreaming();
            //    d.SimpleGraphics.DisplayImage(LCD, 0, 0);
            byte[] bmp42 = new byte[SystemMetrics.ScreenWidth * SystemMetrics.ScreenHeight * 3 + 54];
            if (!File.Exists(Definitions.IMAGES_DIRECTORY))
            {
                System.IO.Directory.CreateDirectory(Definitions.IMAGES_DIRECTORY);
            }
            DateTime DT = RealTimeClock.GetTime();
            string date = DT.ToString();
            string newDate = "";
            char[] tempDate = date.ToCharArray();
            for (int i = 0; i < date.Length; i++)
            {
                if (tempDate[i] == '/' || tempDate[i] == ' ' || tempDate[i] == ':')
                {
                    newDate += '_';
                }
                else
                {
                    newDate += tempDate[i];
                }

            }
            string sBmpFullName = Definitions.IMAGES_DIRECTORY + "Image" + newDate + "_new.bmp";
            Util.BitmapToBMPFile(LCD.GetBitmap(), SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight, bmp42);
            //    GT.Picture picture = new GT.Picture(bmp42, GT.Picture.PictureEncoding.BMP);
            if (Program.g_bSaveImageToFile)
            {
                File.WriteAllBytes(sBmpFullName, bmp42);          //TO SAVE TO FILE
                Program._sd.UnmountSDCard();
                Program._sd.MountSDCard();
            }
              
            Debug.Print("Save Image Procces Done");
            return bmp42;
        }
        */
        public void setObjects()
        {

        }

        public void loadWindow()
        {
        }

        /*
        string[] _pins;
        int _runTime;
        int _waitTime;
        int[] _pressure;
        public Stage(string pins, string runTime, string pressure, string waitTime)
        {
            //  _pins = new ArrayList();
            String[] p = pins.Split(':');
            string[] pp = p[1].Split(',');
            string last = "";
            if (Contains(pp[pp.Length - 1], "\r"))
            {
                last = pp[pp.Length - 1];
                pp[pp.Length - 1] = last.Substring(0, last.Length - 1);
            }
            
            _pins = new string[pp.Length];
            
            for (int i = 0; i < pp.Length; i++)
            {
                _pins[i] = Convert.ToInt32(pp[i]);
            }
           
          
        _pins = pp;
            last = runTime.Split(':')[1];
            if (Contains(last, "\r"))
            {

                last = last.Substring(0, last.Length - 1);
            }
            _runTime = Convert.ToInt32(last);


            last = waitTime.Split(':')[1];
            if (Contains(last, "\r"))
            {
                last = last.Substring(0, last.Length - 1);
            }
            _waitTime = Convert.ToInt32(last);


            last = pressure.Split(':')[1];
            if (Contains(last, "\r"))
            {
                last = last.Substring(0, last.Length - 1);
            }
            string[] tempPressure = last.Split(',');
            _pressure[0] = Convert.ToInt32(tempPressure[0]);
            _pressure[1] = Convert.ToInt32(tempPressure[1]); 
        }
        public int[] getPressure()
        {
            return _pressure;
        }
        public string[] getPinsForStage()
        {
            return _pins;
        }
        public int getRunTimeStage()
        {
            return _runTime;
        }
        public int getWaitTimeStage()
        {
            return _waitTime;
        }
        public static bool Contains(string s, string value)
        {
            return s.IndexOf(value) > 0;
        }
         */
    }

}
