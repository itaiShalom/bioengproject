using System;
using Microsoft.SPOT;

namespace BioProject1._4.ProgramHandeling
{
    class ImageObj
    {
        public byte[] imageData;
        public int iStageNumber;
        public ImageObj(byte[] data, int iStageNum)
        {
            imageData = data;
            iStageNumber = iStageNum;
        }

    }
   
}
