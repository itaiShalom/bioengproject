using System;
using Microsoft.SPOT;
using System.Collections;
using System.Threading;
using System.Xml;
using System.IO;
using GHI.Glide;
using System.Text;
using System.Ext.Xml;
using Gadgeteer.Modules.ItaiShalom;
using BioProject1._4.GUIs;
using BioProject1._4.Wrappers;
using BioProject1._4.Utils;
namespace BioProject1._4.ProgramHandeling
{
    class Scenario
    
    {
        private static Queue imageQ;
        double CodeId;
        ArrayList m_AL_Stages;
        ArrayList m_AL_allObjects;
        string m_sScenarioType;
        int m_iTotalTime=0;
        public static Definitions.LastGui enSender;
        public bool m_bIsOn;
        public Scenario(string xmlStr)
        {
            imageQ = new Queue();
            m_AL_Stages = new ArrayList();
            m_AL_allObjects = new ArrayList();
            parseScenario(xmlStr);
            m_bIsOn = false;
        }

        public string getScenarioType()
        {
            return m_sScenarioType;
        }
        public ImageObj getImage()
        {
            var vImage =imageQ.Dequeue();
            ImageObj ioData = vImage as ImageObj;
            return ioData;
        }
        public bool isEmpty()
        {
            return imageQ.Count <= 0;
        }

        public int iTotalTime
        {
            get { return m_iTotalTime; }
        }
        public int getTotalStages()
        {
            return m_AL_Stages.Count;
        }
        private void parseScenario(string xmlStr)
        {

            byte[] xmlBytes = UTF8Encoding.UTF8.GetBytes(xmlStr);
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.IgnoreComments = true;
            xmlReaderSettings.IgnoreProcessingInstructions = true;
            xmlReaderSettings.IgnoreWhitespace = true;

            MemoryStream stream = new MemoryStream(xmlBytes);

            XmlReader reader = XmlReader.Create(stream, xmlReaderSettings);


            if (!reader.ReadToDescendant("Scenario"))
                throw new ArgumentException("Scenario not detected.");

            CodeId = Convert.ToDouble(reader.GetAttribute(0));

            stream.Seek(0, SeekOrigin.Begin);
            reader = XmlReader.Create(stream, xmlReaderSettings);

            if (!reader.ReadToDescendant("TestInstructions"))
                throw new ArgumentException("XML does not contain a TestInstructions element.");
            m_sScenarioType = reader.GetAttribute("ScenarioType");

            bool bFoundComponent;
            while (reader.Read() && !(reader.NodeType == XmlNodeType.EndElement && reader.Name == "TestInstructions"))
            {
                bFoundComponent = GetComponent(reader);

                if (bFoundComponent == false)
                    throw new Exception(reader.Name + " is not a valid UI component.");
            }

            reader.Close();
        }

        private  bool GetComponent(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Stage":
                    return parseStage(reader);
                default:
                    return false;
            }
        }

        private  bool parseStage(XmlReader reader)
        { 
            int iStageNumber = Convert.ToInt32(reader.GetAttribute("Number"));
            string[] arr_sObjectNumbers = reader.GetAttribute(m_sScenarioType).Split(':');
            int iTemp;
            for (int i = 0; i < arr_sObjectNumbers.Length; i++)
            {
                iTemp = Convert.ToInt32(arr_sObjectNumbers[i]);
                if (!m_AL_allObjects.Contains(iTemp))
                    m_AL_allObjects.Add(iTemp);
            }
            string sPressure = reader.GetAttribute("Pressures");
            string[] arr_sTunnels = reader.GetAttribute("Tunnels").Split(':');
            int iRunTime = Convert.ToInt32(reader.GetAttribute("RunTime"));
            int iDelayTime = Convert.ToInt32(reader.GetAttribute("DelayTime"));
            string sSaveImage = reader.GetAttribute("Image");
            bool bSaveImage =false;
            if (sSaveImage.Equals("1"))
            {
                bSaveImage = true;
            }
            m_iTotalTime += iRunTime + iDelayTime;
            m_AL_Stages.Add(new Stage(iStageNumber, arr_sObjectNumbers, sPressure, arr_sTunnels, iRunTime, iDelayTime, m_sScenarioType, bSaveImage));
            return true;
        }



        public void run(Definitions.LastGui enLast)
        {
            enSender = enLast;
            if (m_sScenarioType.Equals("Switchboard"))
                runForSwitch();
            else
                runForMan();
          //  imageQ.Enqueue(new ImageObj(null, -1));
            GuiCreator.GoBackGUI(enSender);
        }
        public void runForMan()
        {
            bool bFreeze = false;
            for (int i = 0; i < m_AL_allObjects.Count; i++)
            {
                int iManifoldNum = (int)m_AL_allObjects[i];
                Program.TestingTool.getManifolds()[iManifoldNum].revertToInitialState();
            }
            for (int i = 0; i < m_AL_Stages.Count; i++)
            {
                Stage stCurrentStage = m_AL_Stages[i] as Stage;
                if (i == 0)
                    GUITestingManifolds.showDisabled();//Glide.MainWindow = GUITestingManifolds.showDisabled();
                m_bIsOn = true;
                bFreeze = stCurrentStage.run(imageQ);
                m_bIsOn = false;
            }
            if (!bFreeze)
            {
                for (int i = 0; i < m_AL_allObjects.Count; i++)
                {
                    int iManifoldNum = (int)m_AL_allObjects[i];
                    Program.TestingTool.getManifolds()[iManifoldNum].revertToInitialState();
                }
            }
           // GUILoadProgram.show();
        }


        //TODO fix manifold numbers and defaults vals
        public void runForSwitch()
        {
            bool bFreeze =false;
            int iCurrentMan = -1; //= getCurrentSwitch()
            
            for (int i = 0; i < m_AL_Stages.Count; i++)
            {
                Stage stCurrentStage = m_AL_Stages[i] as Stage;
                if (i == 0)
                {
                    iCurrentMan = stCurrentStage.getCurrentSwitch();
                    Program.TestingTool.getSwitchboard()[iCurrentMan].setInitialStatus();
                    int[] initialArray = new int[Program.TestingTool.getSwitchboard()[iCurrentMan].getTunnelsAmount()];
                   // Glide.MainWindow = GUITestingSwitchboards.showDisabled(iCurrentMan,initialArray,false);
                    GUITestingSwitchboards.showDisabled(iCurrentMan, initialArray, false);
                }
                else
                {
                    if (iCurrentMan != stCurrentStage.getCurrentSwitch())
                    {
                        iCurrentMan = stCurrentStage.getCurrentSwitch();
                        Program.TestingTool.getSwitchboard()[iCurrentMan].setInitialStatus();                            
                    }
                }
                m_bIsOn = true;
                bFreeze = stCurrentStage.run(imageQ);
                m_bIsOn = false;
            }
            if (!bFreeze)
                Program.TestingTool.getSwitchboard()[iCurrentMan].setInitialStatus();
        }
    }
      
}
