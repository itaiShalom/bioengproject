using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
namespace BioProject1._4
{
    class RectButton
    {
        int _x1;
        int _x2;
        int _y1;
        int _y2;
    public    CheckBox _cb;
        public RectButton(int x1, int x2, int y1,int y2,CheckBox cb)
        {
            _x1 = x1;
            _x2 = x2;
            _y1 = y1;
            _y2 = y2;
            _cb = cb;
        }
        public bool isContained(int x,int y)
        {
            if ((x >= _x1) && (x <= _x2) && (y >= _y1) && (y <= _y2))
            {
                return true;
            }
            return false;
        }
        public void printMe()
        {
            Debug.Print("the box coordinats");
            Debug.Print(_x1.ToString());
            Debug.Print(_x2.ToString());
            Debug.Print(_y1.ToString());
            Debug.Print(_y2.ToString());
        }
    }
}
