﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Collections;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;
using GHI.Glide;
using GHI.Glide.Display;
using System.Text;
using GHI.Glide.UI;
using GHI.Premium.Net;
using GHI.Premium;
//using GHI.Premium.System;  //Added manually
using Gadgeteer.Networking;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer.Modules.ItaiShalom;
using BioProject1._4.Utils;
using BioProject1._4.Wrappers;
using BioProject1._4.GUIs;
using BioProject1._4.RemoteControl;
using GHI.Hardware;
using System.Runtime.InteropServices;  
using GHI.Premium.Hardware;
using GHI.Premium.System;

namespace BioProject1._4
{

    public partial class Program
    {
       public static Wrapper TestingTool;
     //  public static Wrapper RunningTool;
       public static Definitions.LastGui enSenderGUI;// = Definitions.LastGui.enMainManu;

        ////////
       public static bool g_bSaveImageToFile = false;
        static Font small = Resources.GetFont(Resources.FontResources.small);
        static GT.Color White = GT.Color.White;
        static GT.Color Cyan = GT.Color.Cyan;
        static GT.Color Green = GT.Color.Green;
        static GT.Color Red = GT.Color.Red;
        static GT.Color Yellow = GT.Color.Yellow;
//        static USBH_Mouse mouse;
  //      static USBH_Keyboard keyb;
 //       public static String connectionFrom = "No Connection";
        public static int port = 3000;
        static bool mouseCliked;
//        public static string ipAddress = "0.0.0.0";
        /// ///////////////
        public static SerialCameraL1 camera;
        public static int numOfManifolds = 2;
        public static int pinsPerManifold = 12;
        static GHI.Glide.UI.Image mouseGlide;
        static int currentSocket;
        static RectButton[] pinsRects;
        public static String programDir;
        public static int pinsOffSet = 4;
        public static int numOfPins = 6;
        public static int[] socketsNum = new int[] { 1, 2, 3, 4 };
        public static int[] resistorsVal = new int[] { 92, 118 };
        //   static int[] resistorsSockets = { 6, 9 };
        public static GHI.Glide.Display.Window window;
        public static MySocket[] mySockets;
        public static MCP4131[] MCP_Resistors;
        public static SDCard _sd;
        public static HubAP5 _hub;
        public static MCP4131 mcpResistor6;
        public static MCP4131 mcpResistor9;
        static int mainManuOptions = 4;
        static int programManuOptions = 3;
        static string createProgramPins = "PINS:";
        static string createProgramResistors = "PRESSURE:";
        static string createProgramWait = "WAIT:";
        static string createProgramRun = "RUN:";
        static string[] createProgramCommands;
        static int createProgramCycle;
        static Display_T43 d;
        static int numOfCycles;
     //   public static string rootDirectory;
        static string[] allPrograms;
        public static bool bLoadingIsDone = false;
        static DataGrid programsList;
        static string selectedProgram = "No Program Selected";
        //  static string programDir;
        //            rootDirectory = sdCard.GetStorageDevice().RootDirectory;
        //  programDir = rootDirectory 

        // This method is run when the mainboard is powered up or reset.   
        void ProgramStarted()
        {
            camera = serialCameraL1;
            Program.camera.SetImageSize(Gadgeteer.Modules.GHIElectronics.SerialCameraL1.Camera_Resolution.SIZE_VGA);

      //      d = display_T43;

          
            mcpResistor9 = mCP41312;
            mcpResistor6 = mCP4131;
           // MCP_Resistors = new MCP4131[2] { mCP41316, mCP41319 };
            _hub = hubAP5;
      //      camera = serialCameraL1;
        //    Program.camera.SetImageSize(Gadgeteer.Modules.GHIElectronics.SerialCameraL1.Camera_Resolution.SIZE_QVGA);
            _sd = sdCard;
       

            ethernet_J11D.UseDHCP();
            ethernet_J11D.NetworkUp += new GTM.Module.NetworkModule.NetworkEventHandler(ethernetJ11D_NetworkUp);
            ethernet_J11D.NetworkDown += new GTM.Module.NetworkModule.NetworkEventHandler(ethernetJ11D_NetworkDown);
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Open));
            GlideTouch.Initialize();
            InitWin();
            Glide.MainWindow = window;
            //Loading will Call Config reader which will call for the first gui
            Loading();

        //    takePicture();

            Debug.Print("Program Started");
        }

        public static byte[] takePicture()
        {
            Bitmap LCD = new Bitmap(SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            // if (!Program.camera.isNewImageReady) ==> this should be in while -loop until image is ready
            Program.camera.StartStreaming();
            while (!Program.camera.isNewImageReady)
            {
                Thread.Sleep(100);
            }
            Program.camera.DrawImage(LCD, 0, 0, SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            Program.camera.StopStreaming();
        //    d.SimpleGraphics.DisplayImage(LCD, 0, 0);
            byte[] bmp42 = new byte[SystemMetrics.ScreenWidth * SystemMetrics.ScreenHeight * 3 + 54];
            if (!File.Exists(Definitions.IMAGES_DIRECTORY))
            {
                System.IO.Directory.CreateDirectory(Definitions.IMAGES_DIRECTORY);
            }
            DateTime DT = RealTimeClock.GetTime();
            string date = DT.ToString();
            string newDate = "";
            char[] tempDate = date.ToCharArray();
            for (int i = 0; i < date.Length; i++)
            {
                if (tempDate[i] == '/' || tempDate[i] == ' ' || tempDate[i] == ':')
                {
                    newDate += '_';
                }
                else
                {
                    newDate += tempDate[i];
                }

            }
            string sBmpFullName = Definitions.IMAGES_DIRECTORY + "Image"+newDate+"_new.bmp";
            Util.BitmapToBMPFile(LCD.GetBitmap(), SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight, bmp42);
        //    GT.Picture picture = new GT.Picture(bmp42, GT.Picture.PictureEncoding.BMP);
            File.WriteAllBytes(sBmpFullName, bmp42);
      //      _sd.UnmountSDCard();
        //    _sd.MountSDCard();
            return bmp42;
        }

        public static byte[] takePicture2()
        {
            Bitmap LCD = new Bitmap(SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            // if (!Program.camera.isNewImageReady) ==> this should be in while -loop until image is ready
            Program.camera.StartStreaming();
            while (!Program.camera.isNewImageReady)
            {
                Thread.Sleep(100);
            }
            Program.camera.DrawImage(LCD, 0, 0, SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight);
            Program.camera.StopStreaming();
            d.SimpleGraphics.DisplayImage(LCD, 0, 0);
            byte[] bmp42 = new byte[SystemMetrics.ScreenWidth * SystemMetrics.ScreenHeight * 3 + 54];
            Util.BitmapToBMPFile(LCD.GetBitmap(), SystemMetrics.ScreenWidth, SystemMetrics.ScreenHeight, bmp42);
            //    GT.Picture picture = new GT.Picture(bmp42, GT.Picture.PictureEncoding.BMP);
            File.WriteAllBytes(_sd.GetStorageDevice().RootDirectory+"\\image.bmp", bmp42);
            _sd.UnmountSDCard();
            _sd.MountSDCard();
            return bmp42;
        }

        void ethernetJ11D_NetworkDown(GTM.Module.NetworkModule sender, GTM.Module.NetworkModule.NetworkState state)
        {
            Definitions.SERVER_IP = "";
            Debug.Print("network down");
            if (Definitions.WAS_REMOTLEY_CONTROLED)
                GUIMainManu.show();
            //MainManuGUI.show();
        }

        public static void GUI3()
        {
            Program.steadyState();
            Program.window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUI3));
            TextBlock text = (TextBlock)Program.window.GetChildByName("connection");
      //      text.Text = connectionFrom;
            int m = 1;
            for (int p = 1; p < Program.pinsPerManifold + 1; p++)
            {
                TextBlock pin = (TextBlock)Program.window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                pin.BackColor = GHI.Glide.Colors.Red;

            }
            Slider sliderGUI2;
            TextBlock sliderValue;
            for (int i = 1; i <= MCP_Resistors.Length; i++)
            {
                sliderGUI2 = (Slider)window.GetChildByName(i.ToString() + "s");
                sliderGUI2.Value = resistorsVal[i - 1];
                sliderGUI2.ValueChangedEvent += new OnValueChanged(sliderGUI2_ValueChangedEvent);
                sliderValue = (TextBlock)window.GetChildByName("R" + i + "V");
                sliderValue.Text = sliderGUI2.Value.ToString();
            }
            Program.loadLogo();
            Program.loadIP();
            GHI.Glide.UI.Image switchboard = (GHI.Glide.UI.Image)Program.window.GetChildByName("switch");
            switchboard.Bitmap = Resources.GetBitmap(Resources.BitmapResources.switchb);

            Button backToMainManu = (Button)window.GetChildByName("backFromSwitch");
            backToMainManu.TapEvent += new OnTap(backToMainManu_TapEvent);


            GlideTouch.Initialize();
            Glide.MainWindow = Program.window;
        }
        /*
        public static void show()
        {
            Program.enLast = Definitions.LastGui.enMainManu;
            Program.window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.MainManu));

            //            Glide.MainWindow = Program.window;
            GuiCreator.loadLogo(Program.window);
            Button mainManuButtons;
            for (int i = 1; i <= Definitions.MAINMANUOPTIONS; i++)
            {
                mainManuButtons = (Button)Program.window.GetChildByName("option" + i.ToString());
                mainManuButtons.TapEvent += new OnTap(mainManuButtons_TapEvent);
            }
            GlideTouch.Initialize();
            Glide.MainWindow = Program.window;
      //      
        }
          
        private static void mainManuButtons_TapEvent(object sender)
        {
            var option = sender as Button;

            if (option.Name.Equals("option1"))
            {
                TestingGUI.show();
                return;
            }
            if (option.Name.Equals("option2"))
            {
                //     programsManu();
                return;
            }
            if (option.Name.Equals("option3"))
            {
                //       GUI3();
                return;
            }
            if (option.Name.Equals("option4"))
            {
                //     GUI4();
                return;
            }
        }
        */
        public static void GUI2()
        {
            /*
            for (int i = 0; i < mySockets.Length; i++)
            {
                mySockets[i].allPinsStatus(false);
            }
            */
            steadyState();
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUI2));
            for (int m = 1; m < numOfManifolds + 1; m++)
            {

                for (int p = 1; p < pinsPerManifold + 1; p++)
                {
                    TextBlock pin = (TextBlock)window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                    if (m == 1)
                    {
                        pin.BackColor = GHI.Glide.Colors.Green;
                    }
                    else
                    {
                        pin.BackColor = GHI.Glide.Colors.Red;
                    }



                    //  GHI.Glide.UI.Image pin = (GHI.Glide.UI.Image)window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                    //  pin.Bitmap = Resources.GetBitmap(Resources.BitmapResources.redb);
                }
            }
            TextBlock text = (TextBlock)Program.window.GetChildByName("connection");
      //      text.Text = connectionFrom;
            loadIP();
            loadLogo();
            Button save = (Button)Program.window.GetChildByName("save");
            save.Alpha = 0;
            save.Text = "";
            TextBox up = (TextBox)Program.window.GetChildByName("upTime");
            up.Alpha = 0;
            up.Text = "";
            TextBox wait = (TextBox)Program.window.GetChildByName("waitTime");
            wait.Alpha = 0;
            wait.Text = "";
            TextBox status = (TextBox)Program.window.GetChildByName("status");
            status.Alpha = 0;
            status.Text = "";
            Glide.MainWindow = window;
        }

        void ethernetJ11D_NetworkUp(GTM.Module.NetworkModule sender, GTM.Module.NetworkModule.NetworkState state)
        {

            Debug.Print("Network Up!");
            Thread.Sleep(200);
            string sIP = ethernet_J11D.NetworkSettings.IPAddress;
            Debug.Print(sIP);
            int j = 0;

            while (sIP.Equals("0.0.0.0"))
            {
                Thread.Sleep(50);
                sIP = ethernet_J11D.NetworkSettings.IPAddress;
                j++;
                Debug.Print("j =" + j.ToString());
            }

            Definitions.SERVER_IP = sIP;
            Listener thread = new Listener();
            thread.Start();
            Timer MyTimer = new Timer(new TimerCallback(ClockTimer_Tick), null, 5000, 0);
            Glide.MessageBoxManager.Show("Connected to Internet","Notification");

            GUIMainManu.show();

          // getConnections();
        }
        static void ClockTimer_Tick(object sender)
        {
            if (Glide.MessageBoxManager.IsOpen)
                Glide.MessageBoxManager.Hide();
     //       GUIMainManu.showDisabled();
        }
        /*
        static void getConnections()
        {
//            Program.window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUI1));
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            System.Net.IPAddress ipaddressReady = System.Net.IPAddress.Parse(Definitions.IP);
            System.Net.IPEndPoint localEndPoint = new System.Net.IPEndPoint(ipaddressReady, port);

  //          loadIP();
  //          loadLogo();

   //         Glide.MainWindow = window;


            server.Bind(localEndPoint);
            server.Listen(1);

            while (true)
            {
                // Wait for a client to connect.
                Debug.Print("waiting for connection");

                try
                {

                    Socket clientSocket = server.Accept();


                    Debug.Print("Connection accpeted, new thread");
                    // Process the client request.  true means asynchronous.
                    new ProcessClientRequest(clientSocket, true);

                }
                catch (System.Net.Sockets.SocketException e)
                {
                    Debug.Print("Socket erorr");
                    server.Close();
                    mainManu();
                    break;


                }

            }
        }
        */
        /*
        public static void loadConnection()
        {
            TextBlock text = (TextBlock)Program.window.GetChildByName("connection");
            text.Text = connectionFrom;
        }
        */
        static void mainManu()
        {
         //   string a = Resources.GetString(Resources.StringResources.MainManu);
     //       window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.MainManu));
       //     window = GlideLoader.LoadWindow((GuiCreator.createWelcomeScreen()));
      //      loadLogo();
       //     Button mainManuButtons;
        //    for (int i = 1; i <= mainManuOptions; i++)
            {
          //      mainManuButtons = (Button)window.GetChildByName("option" + i.ToString());
         //       mainManuButtons.TapEvent +=new OnTap(mainManuButtons_TapEvent);
            }
            GlideTouch.Initialize();
            Glide.MainWindow = Program.window;
        }
/*
        static void mainManuButtons_TapEvent(object sender)
        {
            var option = sender as Button;
            if (option.Name.Equals("option1"))
            {
                socketPage();
                return;
            }
            if (option.Name.Equals("option2"))
            {
                programsManu();
                return;
            }
            if (option.Name.Equals("option3"))
            {
                GUI3();
                return;
            }
            if (option.Name.Equals("option4"))
            {
                GUI4();
                return;
            }
        }
        */
        /*
        static void programsManu()
        {
            Program.window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.programManu));
            loadLogo();

            Button programManuButtons;
            for (int i = 1; i <= programManuOptions; i++)
            {
                programManuButtons = (Button)window.GetChildByName("option" + i.ToString());
                programManuButtons.TapEvent += new OnTap(programManuButtons_TapEvent);
            }

            TextBlock loadedProgram = (TextBlock)window.GetChildByName("loadedProgram");
            loadedProgram.Text = selectedProgram;
            Button backToMainManu = (Button)window.GetChildByName("backFromProgram");
            backToMainManu.TapEvent += new OnTap(backToMainManu_TapEvent);
            GlideTouch.Initialize();
            Glide.MainWindow = Program.window;
        }
         * */
        /*
        static void programManuButtons_TapEvent(object sender)
        {
            var option = sender as Button;
            if (option.Name.Equals("option1"))
            {
                TextBox cycles = (TextBox)Program.window.GetChildByName("cycles");
                cycles.Text = "Cycles?";
                Glide.MainWindow = window;
                cycles.Text = "";
                cycles.TapEvent += new OnTap(Glide.OpenKeyboard);

                // Add a value changed handler.
                cycles.ValueChangedEvent += new OnValueChanged(cycles_ValueChangedEvent);

            }
            if (option.Name.Equals("option2"))
            {
                loadProgramManu();
            }
        }*/
        /*
        public static void loadProgramManu()
        {
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.loadProgram));

            // Activate touch
            GlideTouch.Initialize();

            // Initialize the window.

            allPrograms = Directory.GetFiles(programDir);
            // Assigning a window to MainWindow flushes it to the screen.
            // This also starts event handling on the window.
            Glide.MainWindow = window;
            programsList = (DataGrid)window.GetChildByName("dataGrid");

            // Possible configurations...
            //dataGrid.ShowHeaders = false;
            //dataGrid.SortableHeaders = false;
            //dataGrid.TappableCells = false;
            //dataGrid.Draggable = false;
            //dataGrid.ShowScrollbar = false;
            //dataGrid.ScrollbarWidth = 4;

            // Listen for tap cell events.
            programsList.TapCellEvent += new OnTapCell(dataGrid_TapCellEvent);

            // Create our three columns.
            programsList.AddColumn(new DataGridColumn("Num", 50));
            programsList.AddColumn(new DataGridColumn("Program Name", 250));
            Populate(true);

            // Populate the data grid with random data.


            // Add the data grid to the window before rendering it.
            window.AddChild(programsList);
            programsList.Render();

            // Setup the button controls.


            Button UpButton = (Button)window.GetChildByName("UpButton");
            UpButton.TapEvent += new OnTap(UpButton_TapEvent);

            Button DownButton = (Button)window.GetChildByName("DownButton");
            DownButton.TapEvent += new OnTap(DownButton_TapEvent);


            Button del = (Button)window.GetChildByName("del");
            del.TapEvent += new OnTap(del_TapEvent);

            Button backFromLoad = (Button)window.GetChildByName("backFromLoad");
            backFromLoad.TapEvent += new OnTap(backFromLoad_TapEvent);


            Button selectButtonProg = (Button)window.GetChildByName("selectButton");
            selectButtonProg.TapEvent += new OnTap(selectButtonProg_TapEvent);
        }
        */
        /*
        static void selectButtonProg_TapEvent(object sender)
        {
            selectedProgram = allPrograms[programsList.SelectedIndex];
            programsManu();
        }

        static void backFromLoad_TapEvent(object sender)
        {
            programsManu();
        }
        
        static void Populate(bool invalidate)
        {
            // Add items with random data
            for (int i = 0; i < allPrograms.Length; i++)
            {

                // DataGridItems must contain an object array whose length matches the number of columns.
                programsList.AddItem(new DataGridItem(new object[2] { i, allPrograms[i] }));
            }

            if (invalidate)
                programsList.Invalidate();
        }

        static void dataGrid_TapCellEvent(object sender, TapCellEventArgs args)
        {
            // Get the data from the row we tapped.
            object[] data = programsList.GetRowData(args.RowIndex);
            if (data != null)
                GlideUtils.Debug.Print("GetRowData[" + args.RowIndex + "] = ", data);
        }



        static void UpButton_TapEvent(object sender)
        {
            if (programsList.SelectedIndex > 0)
                programsList.SelectedIndex--;
        }

        static void DownButton_TapEvent(object sender)
        {
            if (programsList.SelectedIndex < programsList.NumItems - 1)
                programsList.SelectedIndex++;
        }


        static void del_TapEvent(object sender)
        {
            ModalResult result = Glide.MessageBoxManager.Show("Are you sure?",
                "Delete program", ModalButtons.YesNo);

            Thread.Sleep(1);

            if (result == ModalResult.Yes)
            {

                File.Delete(allPrograms[programsList.SelectedIndex]);

                //     File f = new File(allPrograms[dataGrid.SelectedIndex]);
                programsList.RemoveItemAt(programsList.SelectedIndex);
                programsList.Invalidate();
            }

        }

        */
        static void cycles_ValueChangedEvent(object sender)
        {
            TextBox cycles = (TextBox)Program.window.GetChildByName("cycles");
            numOfCycles = Convert.ToInt32(cycles.Text);
            createProgram(numOfCycles);
        }


        public static void createProgram(int numOfCycles)
        {
            createProgramCycle = 0;
            createProgramCommands = new string[numOfCycles * 4];

            GUI2(numOfCycles);
        }

        public static void GUI2(int numOfCycles)
        {
            for (int i = 0; i < mySockets.Length; i++)
            {
                mySockets[i].setStatusToallPins(false);
            }
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUI2));
            for (int m = 1; m < numOfManifolds + 1; m++)
            {
                for (int p = 1; p < pinsPerManifold + 1; p++)
                {
                    TextBlock pinBut = (TextBlock)window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                    pinBut.BackColor = GHI.Glide.Colors.Red;
                    pinBut.TapEvent += new OnTap(pinBut_TapEvent);

                    //  GHI.Glide.UI.Image pin = (GHI.Glide.UI.Image)window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                    //  pin.Bitmap = Resources.GetBitmap(Resources.BitmapResources.redb);
                }
            }


            Slider sliderGUI2;
            TextBlock sliderValue;
            for (int i = 1; i <= MCP_Resistors.Length; i++)
            {
                sliderGUI2 = (Slider)window.GetChildByName(i.ToString() + "s");
                sliderGUI2.Value = resistorsVal[i - 1];
                sliderGUI2.ValueChangedEvent += new OnValueChanged(sliderGUI2_ValueChangedEvent);
                sliderValue = (TextBlock)window.GetChildByName("R" + i + "V");
                sliderValue.Text = sliderGUI2.Value.ToString();
            }

            TextBlock text = (TextBlock)Program.window.GetChildByName("connection");
  //          text.Text = connectionFrom;
            loadIP();
            loadLogo();
            Button save = (Button)Program.window.GetChildByName("save");
            save.TapEvent += new OnTap(save_TapEvent);
            TextBox up = (TextBox)Program.window.GetChildByName("upTime");
            up.TapEvent += new OnTap(Glide.OpenKeyboard);
            //  up.ValueChangedEvent += new OnValueChanged(up_ValueChangedEvent);

            TextBox wait = (TextBox)Program.window.GetChildByName("waitTime");
            wait.TapEvent += new OnTap(Glide.OpenKeyboard);
            //    wait.ValueChangedEvent += new OnValueChanged(wait_ValueChangedEvent);

            TextBox status = (TextBox)Program.window.GetChildByName("status");
            status.Text = (createProgramCycle + 1).ToString() + "/" + numOfCycles.ToString();
            GlideTouch.Initialize();
            Glide.MainWindow = window;
        }

        static void wait_ValueChangedEvent(object sender)
        {
            //throw new NotImplementedException();
        }



        static void up_ValueChangedEvent(object sender)
        {
            //   throw new NotImplementedException();
        }

        static void save_TapEvent(object sender)
        {
            TextBlock pinBut;
            for (int m = 1; m < numOfManifolds + 1; m++)
            {
                for (int p = 1; p < pinsPerManifold + 1; p++)
                {
                    pinBut = (TextBlock)window.GetChildByName("m" + m.ToString() + "p" + p.ToString());
                    if (pinBut.BackColor == GHI.Glide.Colors.Green)
                    {
                        createProgramPins += pinBut.Name + ",";
                    }
                }
            }
            createProgramPins = createProgramPins.Substring(0, createProgramPins.Length - 1);
            TextBlock sliderValue;
            for (int i = 1; i <= MCP_Resistors.Length; i++)
            {
                sliderValue = (TextBlock)window.GetChildByName("R" + i + "V");
                if (i != MCP_Resistors.Length) { createProgramResistors += sliderValue.Text + ","; }
                else { createProgramResistors += sliderValue.Text; }
            }
            TextBox up = (TextBox)Program.window.GetChildByName("upTime");
            createProgramRun = up.Text;
            TextBox wait = (TextBox)Program.window.GetChildByName("waitTime");
            createProgramWait = wait.Text;
            createProgramCommands[0 + createProgramCycle * 4] = createProgramPins;
            createProgramCommands[1 + createProgramCycle * 4] = createProgramResistors;
            createProgramCommands[2 + createProgramCycle * 4] = createProgramRun;
            createProgramCommands[3 + createProgramCycle * 4] = createProgramWait;
            createProgramCycle++;
            createProgramPins = "PINS:";
            createProgramResistors = "PRESSURE:";
            createProgramWait = "WAIT:";
            createProgramRun = "RUN:";
            if (createProgramCycle + 1 <= numOfCycles)
            {
                GUI2(numOfCycles);
            }
            else
            {
                saveProgram();
            }
        }

        static void saveProgram()
        {
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.saveProgram));
            Glide.MessageBoxManager.Show("Please choose a name for your program");
            TextBox name = (TextBox)window.GetChildByName("programName");
            name.TapEvent += new OnTap(Glide.OpenKeyboard);
     //       name.ValueChangedEvent += new OnValueChanged(name_ValueChangedEvent);
            GlideTouch.Initialize();
            Glide.MainWindow = window;
        }
        /*
        static void name_ValueChangedEvent(object sender)
        {
            TextBox name = (TextBox)window.GetChildByName("programName");
     //       Scenario.saveScenario(createProgramCommands, name.Text);
            programsManu();
        }
        */
        static void sliderGUI2_ValueChangedEvent(object sender)
        {
            var slide = sender as Slider;
            
            int t = Convert.ToInt32(slide.Name[0].ToString());
            resistorsVal[t - 1] = (int) slide.Value;
            TextBlock status = (TextBlock)window.GetChildByName("R" + t + "V");
            window.FillRect(status.Rect);
            status.Text = slide.Value.ToString("F0");
            status.Invalidate();
            //createProgramResistors
        }

        static void pinBut_TapEvent(object sender)
        {
            TextBlock pinBut = (TextBlock)sender;
            if (pinBut.BackColor == GHI.Glide.Colors.Red)
            {
                pinBut.BackColor = GHI.Glide.Colors.Green;
            }
            else
            {
                pinBut.BackColor = GHI.Glide.Colors.Red;
            }
            Glide.MainWindow = window;
        }


        public static void GUI4()
        {
            Program.window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUI4));
            //     byte[] array = takePicture();
            //    sendMessage(m_clientSocket, array);
            TextBlock text = (TextBlock)Program.window.GetChildByName("connection");
   //         text.Text = connectionFrom;
            DataGrid dataGrid = (DataGrid)Program.window.GetChildByName("Table");

            // Create our three columns.
            dataGrid.AddColumn(new DataGridColumn("Color", 50));
            dataGrid.AddColumn(new DataGridColumn("Value", 125));
            Program.window.AddChild(dataGrid);
            dataGrid.Render();
            Glide.MainWindow = Program.window;
            //    dataGrid.AddColumn(new DataGridColumn("Last Name", 125));
        }

        static void socketPage()
        {
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.chooseSocket));

            loadLogo();
            for (int i = 0; i < mySockets.Length; i++)
            {
                TextBox socketStatus = (TextBox)window.GetChildByName("t" + i.ToString());
                socketStatus.Text = mySockets[i].getStatusList();
                Button sock = (Button)window.GetChildByName("s" + i.ToString());
                sock.TapEvent += new OnTap(sock_TapEvent);
            }
            Button res = (Button)window.GetChildByName("s" + 4);
            res.TapEvent += new OnTap(res_TapEvent);

            Button backToMainManu = (Button)window.GetChildByName("BackFromSocket");
            backToMainManu.TapEvent += new OnTap(backToMainManu_TapEvent);

            GlideTouch.Initialize();
            Glide.MainWindow = window;
            Debug.Print(window.Name);
        }

        static void backToMainManu_TapEvent(object sender)
        {
            mainManu();
        }

        static void res_TapEvent(object sender)
        {
            ResistorPage();
        }

        static void sock_TapEvent(object sender)
        {
            var sock = sender as Button;
            currentSocket = Convert.ToInt32(sock.Name[1].ToString());
            pinPage();
        }

        public static void loadLogo()
        {
            GHI.Glide.UI.Image logo = (GHI.Glide.UI.Image)window.GetChildByName("hujiLogo");
            logo.Bitmap = Resources.GetBitmap(Resources.BitmapResources.hujiLogo);
        }

        public static void loadIP()
        {
            TextBlock myIP = (TextBlock)window.GetChildByName("IPBox");
            myIP.Text = Definitions.SERVER_IP;
        }
        public static int[] stringToSocketPin(String mxpx)
        {
            int[] values = new int[2];
            mxpx = mxpx.Substring(1, mxpx.Length);
            string[] vals = mxpx.Split('p');
            int manifold = Convert.ToInt16(vals[0]);
            int pin = Convert.ToInt16(vals[1]);
            if (manifold == 1) //Sockets 1/2
            {
                if (pin > 6) //Socket 2
                {
                    values[0] = 1;
                    values[1] = pin - 7;
                }
                else
                {       //Socket 1
                    values[0] = 0;
                    values[1] = pin - 1;
                }
            }
            else
            {
                if (pin > 6) //Socket 3
                {
                    values[0] = 2;
                    values[1] = 5 - (pin - 7);
                }
                else //Socket 4
                {
                    values[0] = 3;
                    values[1] = 5 - (pin - 1);
                }
            }
            return values;
        }

        static void pinPage()
        {

            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.choosePin));
            loadLogo();
            TextBox socketNumber = (TextBox)window.GetChildByName("sNum");
            //   currentSocket = 1;
            int socketToDisplayed = currentSocket + 1;
            socketNumber.Text = "Socket " + socketToDisplayed;
            socketNumber.Invalidate();
            CheckBox cb;
            pinsRects = new RectButton[numOfPins + 1];//+1 is for all button
            bool allUP = true;
            for (int i = 0; i < numOfPins + 1; i++) //+1 is for all button
            {

                cb = (CheckBox)window.GetChildByName("p" + i.ToString());
                if (i != numOfPins)
                {
                    bool status = mySockets[currentSocket].getPinStatus(i);
                    cb.setStatus(status);
                    if (!status)
                    {
                        allUP = false;
                    }
                }
                else
                {
                    if (allUP)
                    {
                        cb.setStatus(allUP);
                    }
                }

                //  int[] arr = new int[] { cb.X, cb.X + cb.Width, cb.Y, cb.Y+cb.Height };

                cb.TapEvent += new OnTap(cb_TapEvent);
                RectButton rb = new RectButton(cb.X, cb.X + cb.Width, cb.Y, cb.Y + cb.Height, cb);
                pinsRects[i] = rb;
            }
            Button backFromPin = (Button)window.GetChildByName("backButton");
            backFromPin.TapEvent += new OnTap(back_TapEvent);
            mouseGlide = (GHI.Glide.UI.Image)window.GetChildByName("mouse");

            mouseGlide.Bitmap = Resources.GetBitmap(Resources.BitmapResources.mouseLogo);
            GlideTouch.Initialize();
            Glide.MainWindow = window;

        }

        static void back_TapEvent(object sender)
        {
            socketPage();
        }

        static void cb_TapEvent(object sender)
        {

            var cb = sender as CheckBox;
            //  Debug.Print(cb.Name);
            int pinIndex = Convert.ToInt32(cb.Name[1].ToString());
            Debug.Print(pinIndex.ToString());
            if (pinIndex < numOfPins) { mySockets[currentSocket].getPin(pinIndex).setStatus(!mySockets[currentSocket].getPin(pinIndex).status()); }
            else
            {
                mySockets[currentSocket].setStatusToallPins(cb.Checked);
                for (int i = 0; i < pinsRects.Length; i++)
                {
                    pinsRects[i]._cb.setStatus(cb.Checked);
                }
            }
            if (mouseCliked)
            {
                cb.changeStatus();
                mouseCliked = false;
            }
        }



        static void InitWin()
        {

            ProgressBar progressBar = (ProgressBar)window.GetChildByName("progressBar");
            progressBar.Value = 0;
            progressBar.MaxValue = 1000;
            GHI.Glide.UI.Image image = (GHI.Glide.UI.Image)window.GetChildByName("pic");

            image.Bitmap = Resources.GetBitmap(Resources.BitmapResources.bigSwitchOpen);
        }

        static void ResistorPage()
        {
            window = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.Resistors));
            loadLogo();
            GlideTouch.Initialize();

            Slider slider;
            TextBlock text;
            for (int i = 0; i < MCP_Resistors.Length; i++)
            {
                slider = (Slider)window.GetChildByName(i + "s");
                slider.Value = resistorsVal[i];
                slider.ValueChangedEvent += new OnValueChanged(slider_ValueChangedEvent);
                text = (TextBlock)window.GetChildByName("R" + i + "T");
                text.Text = resistorsVal[i].ToString();
            }
            Glide.MainWindow = window;
            Button backFormRes = (Button)window.GetChildByName("BackBut");
            backFormRes.TapEvent += new OnTap(backFormRes_TapEvent);
        }

        public static void steadyState()
        {
            mySockets[0].setStatusToallPins(true);   //Manifold 1
            mySockets[1].setStatusToallPins(true);   //Manifold 1
            mySockets[2].setStatusToallPins(false);  //Manifold 2
            mySockets[3].setStatusToallPins(false);  //Manifold 2
        }

        static void backFormRes_TapEvent(object sender)
        {
            socketPage();
        }

        static void slider_ValueChangedEvent(object sender)
        {
            var slide = sender as Slider;
            Debug.Print(slide.Name);
            int t = Convert.ToInt32(slide.Name[0].ToString());
            TextBlock status = (TextBlock)window.GetChildByName("R" + t + "T");
            window.FillRect(status.Rect);
            status.Text = slide.Value.ToString("F0");
            status.Invalidate();
            MCP_Resistors[t].setTap((int)System.Math.Round(slide.Value));
            resistorsVal[t] = (int)slide.Value;
            Debug.Print("done");
        }

        public static void Loading()
        {
            TestingTool = new Wrapper();
         //   TestingTool = new Wrapper();
            ProgressBar progressBar = (ProgressBar)window.GetChildByName("progressBar");
            bool runThread = false;

            ConfigReader thread = new ConfigReader();
            
            int jump = 15;

                if (runThread == false)
                {

                    // THIS Triggers the DeviceConnected event 

                    mouseCliked = false;
                    thread.Start();
                    runThread = true;
                    progressBar.Value += progressBar.MaxValue / 20;
                }

                while (Definitions.ROOT_DIRECTORY == "")
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }

                while (thread.resistorsArrayReady() == null)
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }
          //      MCP_Resistors = thread.resistorsArrayReady();
                TestingTool.setResistors(thread.resistorsArrayReady());
         //       RunningTool.setResistors(thread.resistorsArrayReady());
            while (thread.socketsArrayReady() == null)
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }
              //  mySockets = thread.socketsArrayReady();
            TestingTool.setSockets(thread.socketsArrayReady());
                while (thread.ManifoldsArrayReady() == null)
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }
                TestingTool.setManifolds(thread.ManifoldsArrayReady());
        //        RunningTool.setManifolds(thread.ManifoldsArrayReady());
                while (thread.SwitchboardArrayReady() == null)
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }
                TestingTool.setSwitchboard(thread.SwitchboardArrayReady());
           //     RunningTool.setSwitchboard(thread.SwitchboardArrayReady());
                progressBar.Value += progressBar.MaxValue / 12;

             

             while (GUIMainManu.m_bDoneLoading == false)
                {
                    Thread.Sleep(100);
                    progressBar.Value += jump;
                    progressBar.Invalidate();
                }

                while (progressBar.Value < progressBar.MaxValue)
                {
                  progressBar.Value += jump*3;
                  
                  progressBar.Invalidate();
                  Thread.Sleep(50);
                }
                
            Debug.Print("done loading");
            bLoadingIsDone = true;
            thread.Join();
            

        }

        private static void UpdateRTC()
        {
            DateTime DT;
            try
            {
                DT = RealTimeClock.GetTime();
                Debug.Print("Current Real-time Clock " + DT.ToString());
            }
            catch // If the time is not good due to powerloss or being a new system an exception will be thrown and a new time will need to be set
            {
                Debug.Print("The date was bad and caused a bad time");
                DT = new DateTime(2014, 1, 1, 1, 1, 1); // This will set a time for the Real-time Clock clock to 1:01:01 on 1/1/2014
                RealTimeClock.SetTime(DT); //This will set the hardware Real-time Clock to what is in DT
            }

            if (DT.Year < 2011)
            {
                Debug.Print("Time is not resonable");
            }

            Debug.Print("Current Real-time Clock " + RealTimeClock.GetTime().ToString());
            DT = new DateTime(2015, 9, 15, 14, 49, 20); // This will set the clock to 9:30:00 on 9/15/2014
            RealTimeClock.SetTime(DT); //This will set the hardware Real-time Clock to what is in DT
            Debug.Print("New Real-time Clock " + RealTimeClock.GetTime().ToString());
        }
    }

   
    


}
