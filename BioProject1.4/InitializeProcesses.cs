using System;
using System.Collections;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using Gadgeteer.Networking;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer.Modules.ItaiShalom;
using BioProject1._4.Wrappers;
using BioProject1._4.Utils;
namespace BioProject1._4
{

    class InitializeProcesses
    {
        HubAP5 _myHub;
        MySocket[] _mySockets;
        bool socketStatus = false;
        
        SDCard _sdCard;
        private Thread _thread = null;
        public bool[] process;
        public InitializeProcesses(ref HubAP5 myHub, ref MySocket[] ms, ref SDCard sd)
        {
            process = new bool[] { false, false, false };
            _sdCard = sd;
            _mySockets = ms;
            _myHub = myHub;
        }

        public void Start()
        {

            _thread = new Thread(_Run);
            _thread.Start();
        }

        public void Abort()
        {
            _thread.Abort();
        }
        public void Join()
        {
            _thread.Join();
        }
        private void _Run()
        {
         //   setUpSdCard();
         //   setUpResistors();
         //   setUpPinControl();
            
        }

        /*
        public void setUpResistors()
        {
            Debug.Print("Resistor start");
            for (int i = 0; i < Program.Resistors.Length; i++)
           {
               Program.Resistors[i].setTap(Program.resistorsVal[i]);
            }
            Debug.Print("Resistors Set");
           process[0] = true;
        }

        */
        /*
        void setUpPinControl()
        {

            Debug.Print("Socket function Start");
            _mySockets = new MySocket[Program.socketsNum.Length];
            for (int i = 0; i < Program.socketsNum.Length; i++)
            {
                _mySockets[i] = new MySocket(_myHub, Program.socketsNum[i]);
            }
            socketStatus = true;
            Debug.Print("All sockets and pins are functional");
            process[1] = true;
        }
        */
        /*
        void setUpSdCard()
        {
            Debug.Print("SDcard Start");
            if (_sdCard.IsCardInserted)
            {
                _sdCard.MountSDCard();
                System.Threading.Thread.Sleep(500);
                Program.rootDirectory = _sdCard.GetStorageDevice().RootDirectory;
                Program.programDir = Program.rootDirectory + "\\Programs";
                Debug.Print("SD Mounted");
            }
            process[2] = true;
        }
        */
        public bool getSockStatus()
        {
            return socketStatus;
        }
        public MySocket[] getSockets()
        {
            return _mySockets;
        }
    };
}