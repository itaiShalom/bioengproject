using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using System.Collections;
using BioProject1._4.Utils;

namespace BioProject1._4.GUIs
{
    class GUITestingManifolds
    {
        public static Definitions.LastGui enSender;
       // static bool first = true;
        static Window manu =null;
        static Window manuDisabled = null;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show()
        {
            if (manu ==null)
            {

                manu = GlideLoader.LoadWindow(GuiCreator.createGuiManifoldTesting());
              //  manuDisabled = GlideLoader.LoadWindow(GuiCreator.createGuiManifoldTesting());
                GuiCreator.loadLogo(manu);
           //     GuiCreator.loadLogo(manuDisabled);
                int iNumOfManifolds = Program.TestingTool.getManifolds().Length;
                string sName;
                TextBlock tb;
                for (int i = 0; i < iNumOfManifolds; i++)
                {
                    int iNumOfTunnels = Program.TestingTool.getManifolds()[i].getNumOfTunnels();
                    for (int j = 0; j < iNumOfTunnels; j++)
                    {
                        sName = "M:" + (i ) + ":P:" + (j );
                        tb = (TextBlock)manu.GetChildByName(sName);
                        tb.TapEvent += new OnTap(tb_TapEvent);
                    }
                }

                Button back = (Button)manu.GetChildByName(Definitions.MANU_OPTION + "0");
                back.TapEvent += new OnTap(back_TapEvent);
            }
            loadManifoldStatus(manu);

                Glide.MainWindow = manu;
        }

     

        private static void loadManifoldStatus(Window win)
        {
            TextBlock tb;
            for (int i = 0; i < Program.TestingTool.getManifolds().Length; i++)
            {
                for (int j = 0; j < Program.TestingTool.getManifolds()[i].getNumOfTunnels(); j++)
                {
                    tb = (TextBlock)win.GetChildByName("M:" + (i) + ":P:" + j);
                    if (Program.TestingTool.getManifolds()[i].getTunnelStatus(j))
                    {
                        
                        tb.BackColor = Definitions.OPEN_COLOR;
                    }
                    else
                    {
                        tb.BackColor = Definitions.CLOSE_COLOR;
                    }
                }
            }
        }

        static void tb_TapEvent(object sender)
        {
            var option = sender as TextBlock;
            int iManifold = Convert.ToInt16(option.Name.Split(':')[1]);
            int iTunnel = Convert.ToInt16(option.Name.Split(':')[3]);
            bool bStatus = Program.TestingTool.getManifolds()[iManifold].getTunnelStatus(iTunnel);
            Program.TestingTool.getManifolds()[iManifold ].setTunnelStatus(iTunnel, !bStatus);
            if (!bStatus == false)
            {
                option.BackColor = Definitions.CLOSE_COLOR;
            }
            else
            {
                option.BackColor = Definitions.OPEN_COLOR;
            }
            Glide.MainWindow = manu;
        }
        static void back_TapEvent(object sender)
        {
            //  Program.enSenderGUI = Definitions.LastGui.enTestingPins;
            GuiCreator.GoBackGUI(enSender);
            return;
        }

        public static void showDisabled()
        {
            defineDisableManu();
            loadManifoldStatus(manuDisabled);
            Glide.MainWindow = manuDisabled;
        }
        private static void defineDisableManu()
        {
            if (manuDisabled == null)
            {
                manuDisabled = new Window();
                manuDisabled = GlideLoader.LoadWindow(GuiCreator.createGuiManifoldTesting());
                GuiCreator.loadLogo(manuDisabled);

            }
        }
    }
}
