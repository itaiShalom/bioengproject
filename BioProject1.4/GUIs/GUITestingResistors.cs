using System;
using BioProject1._4.Utils;
using GHI.Glide;
using GHI.Glide.UI;
using GHI.Glide.Display;
using Microsoft.SPOT;

namespace BioProject1._4.GUIs
{
    class GUITestingResistors
    {
        public static Definitions.LastGui enSender;
        static Window manu = null;
        private static Slider[] arr_ResistorSliders;
        private static TextBlock[] arr_ResistorText;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show()
        {
            if (manu == null)
            {
                manu = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUITestingResistors));
                GuiCreator.loadLogo(manu);
                Button but = (Button)manu.GetChildByName(Definitions.MANU_OPTION + "0");
                but.TapEvent += new OnTap(but_TapEvent);
                Slider slider;
                TextBlock sliderValue;
                arr_ResistorSliders = new Slider[Program.TestingTool.getResistors().Length];
                arr_ResistorText = new TextBlock[Program.TestingTool.getResistors().Length];
                for (int i = 0; i < Program.TestingTool.getResistors().Length; i++)
                {
                    slider = (Slider)manu.GetChildByName("slider" + i.ToString());

                  //  sliderGUI2.Value = resistorsVal[i - 1];
                    slider.ValueChangedEvent += new OnValueChanged(slider_ValueChangedEvent);
                    arr_ResistorSliders[i] = slider;
                    sliderValue = (TextBlock)manu.GetChildByName("val" + i.ToString());
                    arr_ResistorText[i] = sliderValue;
                    //  sliderValue.Text = slider.Value.ToString();
                }
                
                
            }
            loadValuesForResistors();
            Glide.MainWindow = manu;
        }

        static void slider_ValueChangedEvent(object sender)
        {
            var slide = sender as Slider;
            int i = Convert.ToInt32(slide.Name[slide.Name.Length-1].ToString());
            Program.TestingTool.getResistors()[i].setTap((int)slide.Value);
            manu.FillRect(arr_ResistorText[i].Rect);
            arr_ResistorText[i].Text = slide.Value.ToString("F0");      
            arr_ResistorText[i].Invalidate();
        }

        private static void loadValuesForResistors()
        {
            for (int i = 0; i < arr_ResistorSliders.Length; i++)
            {
                arr_ResistorSliders[i].Value = Program.TestingTool.getResistors()[i].getTap();
                arr_ResistorText[i].Text = arr_ResistorSliders[i].Value.ToString("F0");
                arr_ResistorText[i].Invalidate();
                arr_ResistorSliders[i].Invalidate();
            }
        }
        static void but_TapEvent(object sender)
        {
            GuiCreator.GoBackGUI(enSender);
            return;
        }
    }
}
