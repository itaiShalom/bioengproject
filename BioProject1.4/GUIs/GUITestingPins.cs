using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using BioProject1._4.Utils;
namespace BioProject1._4.GUIs
{
    class GUITestingPins
    {
        static Window[] manu = null;
        public static Definitions.LastGui enSender;
        private static CheckBox[][] m_jarr_allCheckBox;
        private static int m_iSocketNum = 0;
               
        public static void show(int iSocketIndex, Definitions.LastGui en)
        {
            enSender = en;
            m_iSocketNum = iSocketIndex;
            if (manu == null)
            {
                m_jarr_allCheckBox = new CheckBox[Program.TestingTool.getSockets().Length][];
                manu = new Window[Program.TestingTool.getSockets().Length];
            }
            if (manu[iSocketIndex] == null)
            {
                enSender = en;
                manu[iSocketIndex] = GlideLoader.LoadWindow(GuiCreator.createGuiPinsTesting(iSocketIndex));
                Debug.Print(GuiCreator.createGuiPinsTesting(iSocketIndex));
                GuiCreator.loadLogo(manu[iSocketIndex]);
                Button but;
                m_jarr_allCheckBox[iSocketIndex] = new CheckBox[Program.TestingTool.getSockets()[iSocketIndex].getPinArr().Length + 1];
                for (int i = 0; i < Program.TestingTool.getSockets()[iSocketIndex].getPinArr().Length + 1; i++)
                {
                    m_jarr_allCheckBox[iSocketIndex][i] = (CheckBox)manu[iSocketIndex].GetChildByName(Definitions.MANU_OPTION + i.ToString());
                    m_jarr_allCheckBox[iSocketIndex][i].TapEvent += new OnTap(cb_TapEvent);
                }
                but = (Button)manu[iSocketIndex].GetChildByName(Definitions.MANU_OPTION + (Program.TestingTool.getSockets()[iSocketIndex].getPinArr().Length + 1).ToString());
                but.TapEvent += new OnTap(but_TapEvent);
            }
            loadPinsStatus();
            Glide.MainWindow = manu[iSocketIndex];
        }

        private static void loadPinsStatus()
        {
            int counter = 0;
            for (int i = 0; i < m_jarr_allCheckBox[m_iSocketNum].Length - 1; i++)
            {
                m_jarr_allCheckBox[m_iSocketNum][i].Checked = Program.TestingTool.getSockets()[m_iSocketNum].getPinStatus(i);
                if (m_jarr_allCheckBox[m_iSocketNum][i].Checked == true)
                {
                    counter++;
                }
            }
            if (counter == m_jarr_allCheckBox[m_iSocketNum].Length)
            {
                m_jarr_allCheckBox[m_iSocketNum][m_jarr_allCheckBox[m_iSocketNum].Length].Checked = true;
            }
        }

        static void but_TapEvent(object sender)
        {
          //  Program.enSenderGUI = Definitions.LastGui.enTestingPins;
            GuiCreator.GoBackGUI(enSender);
            return;
        }

        static void cb_TapEvent(object sender)
        {
            var option = sender as CheckBox;
            int iOptionSelected = Convert.ToInt32(option.Name.Substring(Definitions.MANU_OPTION.Length));
            if (iOptionSelected == Program.TestingTool.getSockets()[m_iSocketNum ].getPinArr().Length )
            {
                Program.TestingTool.getSockets()[m_iSocketNum ].setStatusToallPins(option.Checked);
                for (int i = 0; i < m_jarr_allCheckBox[m_iSocketNum].Length - 1; i++)
                { m_jarr_allCheckBox[m_iSocketNum][i].Checked = option.Checked; }
            }
            else
            {
                Program.TestingTool.getSockets()[m_iSocketNum ].setPinStatus(iOptionSelected, option.Checked);
            }
        }
    }
}
