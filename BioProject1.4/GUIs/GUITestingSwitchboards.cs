using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using System.Collections;
using BioProject1._4.Utils;


namespace BioProject1._4.GUIs
{
    class GUITestingSwitchboards
    {
        static Window[] manu = null;
        static Window[] manuDisabled = null;
        public static Definitions.LastGui enSender;
        //     private static CheckBox[] m_arr_allCheckBox;
       private static int m_iCurrentSwitchboardNum;

        public static void show( Definitions.LastGui en,int iSwitchNum)
        {
            enSender = en;
           //TODO
            if (manu == null)
            {
                manu = new Window[Program.TestingTool.getSwitchboard().Length];
                manuDisabled = new Window[Program.TestingTool.getSwitchboard().Length];
            }
            if (manu[iSwitchNum] == null)
            {
                enSender = en;
                manuDisabled[iSwitchNum] = GlideLoader.LoadWindow(GuiCreator.createGuiSwitchboardTesting(iSwitchNum));
                manu[iSwitchNum] = GlideLoader.LoadWindow(GuiCreator.createGuiSwitchboardTesting(iSwitchNum));
             
         //       Debug.Print(GuiCreator.createGuiPinsTesting(m_iSwitchboardNum));

                GuiCreator.loadLogo(manu[iSwitchNum]);
                GuiCreator.loadLogo(manuDisabled[iSwitchNum]);

                GuiCreator.loadSwitchboard(manu[iSwitchNum]);
                GuiCreator.loadSwitchboard(manuDisabled[iSwitchNum]);

                TextBlock tb;
                //               m_arr_allCheckBox = new CheckBox[Program.TestingTool.getSockets()[m_iSwitchboardNum].getPinArr().Length + 1];
                for (int i = 0; i < Program.TestingTool.getSwitchboard()[iSwitchNum].getTunnelsAmount(); i++)
                {
                    tb = (TextBlock)manu[iSwitchNum].GetChildByName(Definitions.MANU_OPTION + i.ToString());
                    tb.TapEvent += new OnTap(tb_TapEvent);
                }
                Button but = (Button)manu[iSwitchNum].GetChildByName(Definitions.MANU_OPTION + (Program.TestingTool.getSwitchboard()[iSwitchNum].getTunnelsAmount()).ToString());
                but.TapEvent += new OnTap(but_TapEvent);
            }
            loadTunnelStatus(manu,iSwitchNum,null,false);
            m_iCurrentSwitchboardNum = iSwitchNum;
            Glide.MainWindow = manu[iSwitchNum];
        }



        static void but_TapEvent(object sender)
        {
            GuiCreator.GoBackGUI(enSender);
            return;
        }

        static void tb_TapEvent(object sender)
        {
            var option = sender as TextBlock;
            int iOptionSelected = Convert.ToInt32(option.Name.Substring(Definitions.MANU_OPTION.Length));
  //          TextBlock tb = (TextBlock)manu[m_iSwitchboardNum].GetChildByName(Definitions.MANU_OPTION + iOptionSelected.ToString());
            if (option.BackColor == Definitions.NOT_VALID_COLOR || option.BackColor == Definitions.OPEN_COLOR)
            {
                Program.TestingTool.getSwitchboard()[m_iCurrentSwitchboardNum].setTunnelStatus(iOptionSelected, false);
                option.BackColor = Definitions.CLOSE_COLOR;
             //   return;
            }
            else if (option.BackColor == Definitions.CLOSE_COLOR)
            {
                Program.TestingTool.getSwitchboard()[m_iCurrentSwitchboardNum].setTunnelStatus(iOptionSelected, true);
                option.BackColor = Definitions.OPEN_COLOR;
              //  return;
            }
            Glide.MainWindow = manu[m_iCurrentSwitchboardNum];
        }
        /*
        private static void loadTunnelStatus()
        {
            TextBlock tb;
            for (int i = 0; i < Program.TestingTool.getSwitchboard()[m_iSwitchboardNum].getTunnelsAmount(); i++)
            {
                tb = (TextBlock)manu[m_iSwitchboardNum].GetChildByName(Definitions.MANU_OPTION + i.ToString());
                Definitions.TunnelStatus enTunnel = Program.TestingTool.getSwitchboard()[m_iSwitchboardNum].getTunnelStatues(i);
                switch (enTunnel)
                {
                    case Definitions.TunnelStatus.OPEN:
                        {
                            tb.BackColor = Definitions.OPEN_COLOR;
                            break;
                        }
                    case Definitions.TunnelStatus.CLOSE:
                        {
                            tb.BackColor = Definitions.CLOSE_COLOR;
                            break;
                        }
                    case Definitions.TunnelStatus.NOT_VALID:
                        {
                            tb.BackColor = Definitions.NOT_VALID_COLOR;
                            break;
                        }
                }
            }
        }
        */
        public static void showDisabled(int iSwitchboardNum)
        {
            defineDisableManu(iSwitchboardNum);
            loadTunnelStatus(manuDisabled, iSwitchboardNum, null, false);
            Glide.MainWindow = manuDisabled[iSwitchboardNum];
        }

        public static void showDisabled(int iSwitchboardNum,int[] arr_iChanged,bool bArrChanged)
        {
            defineDisableManu(iSwitchboardNum);
            loadTunnelStatus(manuDisabled, iSwitchboardNum, arr_iChanged, bArrChanged);
            Glide.MainWindow =  manuDisabled[iSwitchboardNum] ;
        }

        private static void defineDisableManu(int iSwitchboardNum)
        {
            if (manuDisabled==null)
                manuDisabled = new Window[Program.TestingTool.getSwitchboard().Length];
            if (manuDisabled[iSwitchboardNum] !=null)
                return;
             manuDisabled[iSwitchboardNum] = GlideLoader.LoadWindow(GuiCreator.createGuiSwitchboardTesting(iSwitchboardNum));
             GuiCreator.loadLogo(manuDisabled[iSwitchboardNum]);
             GuiCreator.loadSwitchboard(manuDisabled[iSwitchboardNum]);
        }

        public static void loadTunnelStatus(Window[] win,int iSwitchboardNum,int[] arr_iChanged, bool bRunOnChanged)
        {
             TextBlock tb;
            int iRunOnChanged=0;
            for (int i = 0; i < Program.TestingTool.getSwitchboard()[iSwitchboardNum].getTunnelsAmount(); i++)
            {
                if (bRunOnChanged)
                {
                    if (iRunOnChanged>=arr_iChanged.Length)
                        return;
                    if (i != arr_iChanged[iRunOnChanged])
                    {
                        continue;
                    }
                    else
                    {
                        iRunOnChanged++;
                    }
                    
                }
                tb = (TextBlock)win[iSwitchboardNum].GetChildByName(Definitions.MANU_OPTION + i.ToString());
                Definitions.TunnelStatus enTunnel = Program.TestingTool.getSwitchboard()[iSwitchboardNum].getTunnelStatues(i);
                switch (enTunnel)
                {
                    case Definitions.TunnelStatus.OPEN:
                        {
                            tb.BackColor = Definitions.OPEN_COLOR;
                            break;
                        }
                    case Definitions.TunnelStatus.CLOSE:
                        {
                            tb.BackColor = Definitions.CLOSE_COLOR;
                            break;
                        }
                    case Definitions.TunnelStatus.NOT_VALID:
                        {
                            tb.BackColor = Definitions.NOT_VALID_COLOR;
                            break;
                        }
                }
            }
        }
        }
    }
