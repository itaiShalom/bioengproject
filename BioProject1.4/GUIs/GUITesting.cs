using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using BioProject1._4.Utils;

namespace BioProject1._4.GUIs
{
    class GUITesting
    {
        public static Definitions.LastGui enSender;
        static Window manu = null;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show()
        {
            if (manu ==null)
            {
                manu = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.GUITesting));
            GuiCreator.loadLogo(manu);
            Button but;
            for (int i = 1; i <= Definitions.TESTING_MANU_OPTIONS; i++)
            {
                but = (Button)manu.GetChildByName("option" + i.ToString());
                but.TapEvent += new OnTap(but_TapEvent);
            }
            }
            Glide.MainWindow = manu;
        }
        
        private static void but_TapEvent(object sender)
        {
            var option = sender as Button;

            if (option.Name.Equals("option1"))
            {
                
                GUITestingSockets.show(Definitions.LastGui.enTestingGUI);
                return;
            }
            if (option.Name.Equals("option2"))
            {
               //TODO
                GUITestingSwitchboards.show(Definitions.LastGui.enTestingGUI,0);
                return;
            }
            if (option.Name.Equals("option3"))
            {
                GUITestingManifolds.show(Definitions.LastGui.enTestingGUI);
              //  Program.enSenderGUI = Definitions.LastGui.enTestingGUI;
                //       GUI3();
                return;
            }
            if (option.Name.Equals("option4"))
            {
                GUITestingResistors.show(Definitions.LastGui.enTestingGUI);
           //     GuiCreator.GoBackGUI(enSender);
                return;
            }
            
            if (option.Name.Equals("option5"))
            {
                
                GuiCreator.GoBackGUI(enSender);
                return;
            }
        }
    }
}
