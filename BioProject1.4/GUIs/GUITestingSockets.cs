using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using BioProject1._4.Utils;
namespace BioProject1._4.GUIs
{
    class GUITestingSockets
    {
        public static Definitions.LastGui enSender;
        static Window manu =null;
        private static bool m_bFirst;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show(){
            if (manu == null)
            {
                m_bFirst = true;
                manu = GlideLoader.LoadWindow(GuiCreator.createGuiSocketsTesting());
                GuiCreator.loadLogo(manu);
                Button but;
                for (int i = 0; i < Program.TestingTool.getSockets().Length+1; i++)
                {
                    but = (Button)manu.GetChildByName("option" + i.ToString());
                    but.TapEvent += new OnTap(but_TapEvent);
                   
                    TextBox socketStatus = (TextBox)manu.GetChildByName("T" + i.ToString());
                    if (i  == Program.TestingTool.getSockets().Length)
                        continue;
                    socketStatus.Text = Program.TestingTool.getSockets()[i].getStatusList();
                    Debug.Print(i.ToString());
                }
            }
            if (!m_bFirst)
            {
                updateList();
            }
            m_bFirst = false;
            Glide.MainWindow = manu;
        }
        private static void updateList()
        {
             for (int i = 0; i < Program.TestingTool.getSockets().Length ; i++)
              {
                    TextBox socketStatus = (TextBox)manu.GetChildByName("T" + i.ToString());
                    if (i  == Program.TestingTool.getSockets().Length)
                        continue;
                    socketStatus.Text = Program.TestingTool.getSockets()[i].getStatusList();
             }
        }

        static void but_TapEvent(object sender)
        {
            var option = sender as Button;
            int iOptionSelected = Convert.ToInt32(option.Name.Substring(Definitions.MANU_OPTION.Length));
            if (iOptionSelected == (Program.TestingTool.getSockets().Length))
            { 
                GuiCreator.GoBackGUI(enSender);
                return;
            }
            else
            {
                GUITestingPins.show(iOptionSelected, Definitions.LastGui.enTestingSocketsGui);
                return;
            }

        }
    }
}
