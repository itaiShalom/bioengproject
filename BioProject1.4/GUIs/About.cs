using System;
using BioProject1._4.Utils;
using GHI.Glide;
using GHI.Glide.UI;
using GHI.Glide.Display;
using Microsoft.SPOT;

namespace BioProject1._4.GUIs
{
    class About
    {
        public static Definitions.LastGui enSender;
        static Window manu = null;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show()
        {
            if (manu == null)
            {
                manu = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.About));
                GuiCreator.loadLogo(manu);
                GHI.Glide.UI.Image aboutImage = (GHI.Glide.UI.Image)manu.GetChildByName("aboutImage");
                aboutImage.Bitmap = Resources.GetBitmap(Resources.BitmapResources.aboutimage);
                Button but = (Button)manu.GetChildByName("option0");
             
                   
                    but.TapEvent += new OnTap(but_TapEvent);
                
            }
            Glide.MainWindow = manu;
        }

        static void but_TapEvent(object sender)
        {
            GuiCreator.GoBackGUI(enSender);
            return;
        }
    }
}
