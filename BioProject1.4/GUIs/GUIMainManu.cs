using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using System.Threading;
using BioProject1._4.Utils;
using BioProject1._4.ProgramHandeling;
namespace BioProject1._4.GUIs
{
    class GUIMainManu
    {
        static Window manu = null;
        static Window manuDisabled = null;
        static string sGuiXml = "";
        public static bool m_bDoneLoading = false;
        public static void show()
        {
            if (manu == null)
            {
                if (sGuiXml == "")
                    sGuiXml = Resources.GetString(Resources.StringResources.MainManu);
                manu = GlideLoader.LoadWindow(sGuiXml);
                GuiCreator.loadLogo(manu);
                Button mainManuButtons;
                for (int i = 0; i < Definitions.MAIN_MANU_OPTIONS; i++)
                {
                    mainManuButtons = (Button)manu.GetChildByName("option" + i.ToString());
                    mainManuButtons.TapEvent += new OnTap(mainManuButtons_TapEvent);
                
                }

            }
            updateInternetStatus(manu);
            m_bDoneLoading = true;
            while (Program.bLoadingIsDone == false)
                Thread.Sleep(100);
            Glide.MainWindow = manu;
        }

        public static void showDisabled()
        {
            if (manuDisabled == null)
            {
                if (sGuiXml == "")
                {
                    sGuiXml = Resources.GetString(Resources.StringResources.MainManu); 
                }
                //
                string sTemp = ReplaceEx(sGuiXml, "TintColor=\"000000\" TintAmount=\"0\"", "TintColor=\"000005\" TintAmount=\"50\"");
                manuDisabled = GlideLoader.LoadWindow(sTemp);
               GuiCreator.loadLogo(manuDisabled);

            }
            updateInternetStatus(manuDisabled);
            Glide.MainWindow = manuDisabled;
        }

        private static string ReplaceEx(string original,
                    string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;
            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();
            int inc = (original.Length / pattern.Length) *
                      (replacement.Length - pattern.Length);
            char[] chars = new char[original.Length + System.Math.Max(0, inc)];
            while ((position1 = upperString.IndexOf(upperPattern,
                                              position0)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                    chars[count++] = original[i];
                for (int i = 0; i < replacement.Length; ++i)
                    chars[count++] = replacement[i];
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0) return original;
            for (int i = position0; i < original.Length; ++i)
                chars[count++] = original[i];
            return new string(chars, 0, count);
        }



        private static void updateInternetStatus(Window win)
        {
            TextBlock tb = (TextBlock)win.GetChildByName(Definitions.SYSTEM_IP_XML);
            if (Definitions.SERVER_IP == "")
                tb.Text = Definitions.INITIAL_INTERNET_STATUS;
            else
                tb.Text = Definitions.INTERNET_STATUS + Definitions.SERVER_IP;
            TextBlock tb2 = (TextBlock)win.GetChildByName(Definitions.CLIENT_IP_XML);
            if (Definitions.CLIENT_IP == "")
                tb2.Text = Definitions.NO_CLIENT_CONNECTED;
            else
                tb2.Text = Definitions.INTERNET_STATUS + Definitions.CLIENT_IP;
        }

        private static void mainManuButtons_TapEvent(object sender)
        {
            Program.enSenderGUI = Definitions.LastGui.enMainManu;
            var option = sender as Button;
            int iOptionSelected = Convert.ToInt32(option.Name.Substring(Definitions.MANU_OPTION.Length));
            switch (iOptionSelected)
            {
                case 0:
                    GUITesting.show(Definitions.LastGui.enMainManu);
                    return;
                case 1:
                    GUILoadProgram.show(Definitions.LastGui.enMainManu);
                    //    Scenario sc = new Scenario();
                    //     sc.parseScenario(ConfigReader.readConfigFileToString(Definitions.PROGRAM_DIRECTORY + "Program1.0.txt"));
                    //    sc.run();
                    return;
                case 2:
                    About.show(Definitions.LastGui.enMainManu);
                    return;
            }
        }
    }
}
