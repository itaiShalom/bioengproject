using System;
using Microsoft.SPOT;
using GHI.Glide;
using GHI.Glide.Display;
using GHI.Glide.UI;
using System.IO;
using System.Threading;
using BioProject1._4.Utils;
using BioProject1._4.ProgramHandeling;
namespace BioProject1._4.GUIs
{
    class GUILoadProgram
    {
        private static string[] allPrograms;
        static int i_ItemsAdded = 0;
        private static DataGrid programsList;
        public static Definitions.LastGui enSender;
        private static Window manu = null;
        public static void show(Definitions.LastGui en)
        {
            enSender = en;
            show();
        }
        public static void show()
        {
            if (manu == null)
            {
                manu = GlideLoader.LoadWindow(Resources.GetString(Resources.StringResources.loadProgram));
                //    GuiCreator.loadLogo(manu);
                Button mainManuButtons;
                for (int i = 0; i < Definitions.LOAD_PROGRAM_OPTIONS; i++)
                {
                    mainManuButtons = (Button)manu.GetChildByName("option" + i.ToString());
                    mainManuButtons.TapEvent += new OnTap(mainManuButtons_TapEvent);
                }
                programsList = (DataGrid)manu.GetChildByName("dataGrid");

                // Possible configurations...
                //dataGrid.ShowHeaders = false;
                //dataGrid.SortableHeaders = false;
                //dataGrid.TappableCells = false;
                //dataGrid.Draggable = false;
                //dataGrid.ShowScrollbar = false;
                //dataGrid.ScrollbarWidth = 4;

                // Listen for tap cell events.
                programsList.TapCellEvent += new OnTapCell(dataGrid_TapCellEvent);
                programsList.AddColumn(new DataGridColumn("Num", 50));
                programsList.AddColumn(new DataGridColumn("Program Name", 350));
                manu.AddChild(programsList);
            }
            allPrograms = Directory.GetFiles(Definitions.PROGRAM_DIRECTORY);
            // Assigning a window to MainWindow flushes it to the screen.
            // This also starts event handling on the window.

 

            // Create our three columns.

            Populate(false);
            // Add the data grid to the window before rendering it.
          
      //      programsList.Render();
            Glide.MainWindow = manu;
        }
        static void dataGrid_TapCellEvent(object sender, TapCellEventArgs args)
        {
            // Get the data from the row we tapped.
            object[] data = programsList.GetRowData(args.RowIndex);
            if (data != null)
                GlideUtils.Debug.Print("GetRowData[" + args.RowIndex + "] = ", data);
        }
        static void Populate(bool invalidate)
        {
            
            // Clear list all programs
            for (int i = 0; i < i_ItemsAdded; i++)
            {
                programsList.RemoveItemAt(0);
            }
            //Add again
                for (int i = 0; i < allPrograms.Length; i++)
                {
                    // DataGridItems must contain an object array whose length matches the number of columns.

                    programsList.AddItem(new DataGridItem(new object[2] { i, allPrograms[i] }));
                }
                i_ItemsAdded = allPrograms.Length;
            if (invalidate)
                programsList.Invalidate();
        }
        private static void mainManuButtons_TapEvent(object sender)
        {

            var option = sender as Button;
            int iOptionSelected = Convert.ToInt32(option.Name.Substring(Definitions.MANU_OPTION.Length));
            switch (iOptionSelected)
            {
                case 0:
                    if (programsList.SelectedIndex > 0)
                        programsList.SelectedIndex--;
                    return;
                case 1:
                    if (programsList.SelectedIndex < programsList.NumItems - 1)
                        programsList.SelectedIndex++;
                    return;
                case 2:
                    if (programsList.SelectedIndex != -1)
                    {
                        object[] obj = programsList.GetRowData(programsList.SelectedIndex);
                        string path = (string)obj[1];
                        Scenario sc = new Scenario(ConfigReader.readConfigFileToString(path));

                        ModalResult resultRun = Glide.MessageBoxManager.Show("Estimated run time: " 
                            + sc.iTotalTime+ " [ms] and " + sc.getTotalStages()+ 
                            " stages","Run program", ModalButtons.OkCancel);
                        Thread.Sleep(1);
                        if (resultRun == ModalResult.Ok)
                        {
                            sc.run(Definitions.LastGui.enLoadProgramGUI);
                        }
                    }
                    return;
                case 3:
                    ModalResult resultDel = Glide.MessageBoxManager.Show("Are you sure?",
               "Delete program", ModalButtons.YesNo);
                    Thread.Sleep(1);
                    if (resultDel == ModalResult.Yes)
                    {
                        File.Delete(allPrograms[programsList.SelectedIndex]);
                        programsList.RemoveItemAt(programsList.SelectedIndex);
                        programsList.Invalidate();
                    }
                    return;
                case 5:
                    GuiCreator.GoBackGUI(enSender);
                    return;
            }
        }
    }
}
