using System;
using Microsoft.SPOT;
using System.Collections;
namespace BioProject1._4.Wrappers
{
    public class MyManifold
    {
        class SocketPin : IComparable
        {
            /*
            enum Comparison
            {
                LessThan = -1, Equal = 0, GreaterThan = 1
            };
            */
            public int iSocket;
            public int iPin;
            public SocketPin(int iSock, int iP) { iSocket = iSock; iPin = iP; }
            public bool Equals(SocketPin other)
            {
                if (other == null) return false;
                return (this.iPin == other.iPin && this.iSocket == other.iSocket);
            }
            
            public int CompareTo(Object compare)
            {
                // A null value means that this object is greater. 
                SocketPin compareSocketPin = (SocketPin)compare;
                if (compareSocketPin == null)
                    return 1;

                else
                {
                    if (this.iPin == compareSocketPin.iPin && this.iSocket == compareSocketPin.iSocket)
                        return 0;
                    if (this.iPin == compareSocketPin.iPin && this.iSocket > compareSocketPin.iSocket)
                        return 1;
                    if (this.iPin > compareSocketPin.iPin && this.iSocket == compareSocketPin.iSocket)
                        return 1;
                }
                return -1;
            }
        }
        int m_iManifoldNumber;
        MySocket[] m_arr_ms_MyManiflod_RelatedSockets;
        Hashtable m_HT_TunnelToPin;
        bool m_ToOpen;
        public MyManifold(int iManifoldNumber, MySocket[] arr_ms_input,string toOpen) 
        {
            if (toOpen.ToUpper().Equals("FALSE"))
                m_ToOpen = false;
            else
                m_ToOpen = true;
            m_arr_ms_MyManiflod_RelatedSockets = arr_ms_input;
            m_iManifoldNumber = iManifoldNumber;
            initHT();
        }
        public bool getToOpen()
        {
            return m_ToOpen;
        }
        public void setManifoldStatus(bool bStatus)
        {
            for (int i = 0; i < m_arr_ms_MyManiflod_RelatedSockets.Length; i++)      
                m_arr_ms_MyManiflod_RelatedSockets[i].setStatusToallPins(bStatus);           
        }

        public void revertToInitialState()
        {
            for (int i = 0; i < m_arr_ms_MyManiflod_RelatedSockets.Length; i++)
                m_arr_ms_MyManiflod_RelatedSockets[i].revertToInitialState();
        }

        public int getNumOfTunnels()
        {
            return m_HT_TunnelToPin.Count;
        }
        
        private void initHT()
        {
            m_HT_TunnelToPin = new Hashtable();
            int k = 0;
            for (int i=0;i<m_arr_ms_MyManiflod_RelatedSockets.Length;i++)
            {
                for (int j = 0; j < m_arr_ms_MyManiflod_RelatedSockets[i].getPinArr().Length; j++)
                {
                   // SocketPin spTemp = new SocketPin(i, j);
                    m_HT_TunnelToPin.Add(k, new SocketPin(i, j));
                    k++;
                }
            }
        }

        public void setTunnelStatus(int iTunnel,bool bStatus)
        {
            SocketPin spTemp = (SocketPin) m_HT_TunnelToPin[iTunnel];
            m_arr_ms_MyManiflod_RelatedSockets[spTemp.iSocket].setPinStatus(spTemp.iPin,bStatus);
        }

        public bool getTunnelStatus(int iTunnel)
        {
            SocketPin spTemp = (SocketPin)m_HT_TunnelToPin[iTunnel];
           return m_arr_ms_MyManiflod_RelatedSockets[spTemp.iSocket].getPinStatus(spTemp.iPin);
        }

        public int getNum()
        {
            return m_iManifoldNumber;
        }
    }
}
