using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Touch;

using GTI = Gadgeteer.Interfaces;
using Gadgeteer.Interfaces;
using GT = Gadgeteer;
using BioProject1._4.Utils;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;

namespace BioProject1._4.Wrappers
{
    public class MySwitchboard
    {
        class MyTunnel
        {
            Definitions.TunnelStatus enTunnelStat;
            MyManifold[] m_arr_mmRearAndFrontManifolds;
            int m_iTunnelNumber;
            public MyTunnel(int iTunnelNumber, MyManifold[] arr_mm_inputManifolds)
            {
                //  enTunnelStat = null;
                m_iTunnelNumber = iTunnelNumber;
                m_arr_mmRearAndFrontManifolds = arr_mm_inputManifolds;
            }
            private void checkStatusOfTunnel()
            {
                bool[] arr_bPinStatus = new bool[m_arr_mmRearAndFrontManifolds.Length];
                bool[] arr_bToOpenStatus = new bool[m_arr_mmRearAndFrontManifolds.Length];
                for (int i = 0; i < m_arr_mmRearAndFrontManifolds.Length; i++)
                {
                    arr_bPinStatus[i] = m_arr_mmRearAndFrontManifolds[i].getTunnelStatus(this.m_iTunnelNumber);
                    arr_bToOpenStatus[i] = m_arr_mmRearAndFrontManifolds[i].getToOpen();
                }
                //TODO

                if (arr_bPinStatus[0] == arr_bPinStatus[1])
                {
                    enTunnelStat = Definitions.TunnelStatus.NOT_VALID;
                    return;
                }
                if ((arr_bPinStatus[0] && arr_bToOpenStatus[0]) || (arr_bPinStatus[1] && arr_bToOpenStatus[1]))
                {
                    enTunnelStat = Definitions.TunnelStatus.OPEN;
                }
                else
                {
                    enTunnelStat = Definitions.TunnelStatus.CLOSE;
                }
            }


            public void setTunnelStatus(bool bTunnelStatus)
            {
                if (bTunnelStatus)
                {
                    enTunnelStat = Definitions.TunnelStatus.OPEN;
                }
                else
                {
                    enTunnelStat = Definitions.TunnelStatus.CLOSE;
                }
                for (int i = 0; i < m_arr_mmRearAndFrontManifolds.Length; i++)
                {
                    if (bTunnelStatus)
                    {
                        m_arr_mmRearAndFrontManifolds[i].setTunnelStatus(this.m_iTunnelNumber, m_arr_mmRearAndFrontManifolds[i].getToOpen());
                    }
                    else
                    {
                        m_arr_mmRearAndFrontManifolds[i].setTunnelStatus(this.m_iTunnelNumber, !m_arr_mmRearAndFrontManifolds[i].getToOpen());
                    }
                }
            }

            public Definitions.TunnelStatus getTunnelStatus()
            {
                checkStatusOfTunnel();
                return enTunnelStat;
            }

            public void setInitialStatus()
            {
                for (int i = 0; i < m_arr_mmRearAndFrontManifolds.Length; i++)
                {
                    m_arr_mmRearAndFrontManifolds[i].revertToInitialState();
                }
                bool t = m_arr_mmRearAndFrontManifolds[0].getTunnelStatus(this.m_iTunnelNumber);
                if (t)
                {
                    enTunnelStat = Definitions.TunnelStatus.OPEN;
                }
                else
                {
                    enTunnelStat = Definitions.TunnelStatus.CLOSE;
                }
            }
        }

        private MyTunnel[] m_arr_mtSwitchTunnels;
        int m_iSwitchNum;
        public MySwitchboard(MyManifold[] arr_mmManfolds, int iNum, int iNumOfTunnels)
        {
            m_iSwitchNum = iNum;
            m_arr_mtSwitchTunnels = new MyTunnel[iNumOfTunnels];
            for (int i = 0; i < m_arr_mtSwitchTunnels.Length; i++)
            {
                m_arr_mtSwitchTunnels[i] = new MyTunnel(i, arr_mmManfolds);
            }
        }

        public void setTunnelStatus(int iTunnel, bool bStatus)
        {
            m_arr_mtSwitchTunnels[iTunnel].setTunnelStatus(bStatus);
        }

        public void setInitialStatus()
        {
            for (int i = 0; i < m_arr_mtSwitchTunnels.Length; i++)
                m_arr_mtSwitchTunnels[i].setInitialStatus();
        }
        public Definitions.TunnelStatus getTunnelStatues(int iTunnel)
        {
            return m_arr_mtSwitchTunnels[iTunnel].getTunnelStatus();
        }

        public int getSwitchboardNumber()
        {
            return m_iSwitchNum;
        }
        public int getTunnelsAmount()
        {
            return m_arr_mtSwitchTunnels.Length;
        }
    }
}
