using System;
using Microsoft.SPOT;
using Gadgeteer.Modules.ItaiShalom;
using BioProject1._4.Wrappers;
namespace BioProject1._4
{
    public class Wrapper
    {
      
        MyManifold[] m__arr_allManifolds;
        MySocket[] m_arr_allSockets;
        MySwitchboard[] m_arr_allSwitchboards;
        MCP4131[] m_arr_Resistors;

        public Wrapper() { }
        public void setManifolds(MyManifold[] mm){m__arr_allManifolds = mm;}
        public void setSockets(MySocket[] ms) {m_arr_allSockets = ms;}
        public void setSwitchboard(MySwitchboard[] msw){ m_arr_allSwitchboards = msw;}
        public void setResistors(MCP4131[] res){m_arr_Resistors = res;}

        public MyManifold[] getManifolds() {return m__arr_allManifolds; }
        public MySocket[] getSockets() {return m_arr_allSockets; }
        public MySwitchboard[] getSwitchboard() {return m_arr_allSwitchboards; }
        public MCP4131[] getResistors() {return m_arr_Resistors; }
    }
}
