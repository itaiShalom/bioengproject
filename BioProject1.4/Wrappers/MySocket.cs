using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Touch;

using GTI = Gadgeteer.Interfaces;
using Gadgeteer.Interfaces;
using GT = Gadgeteer;

using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
namespace BioProject1._4.Wrappers
{

    public class MySocket
    {
        int m_iNumOfPins;
        int m_iSocketNumber;
        GT.Socket m_GTSsocket;
        bool m_bFromHub;
        string m_sName;
        MyPin[] m_arr_mpSocketsPins;

        public MySocket(HubAP5 hubAP5, int SockNumber,string[] sInputPins,bool bFromHub)
        {
        
            m_iNumOfPins = sInputPins.Length;
          
             m_iSocketNumber = SockNumber;
             if (bFromHub)
             {
                 m_sName = "S:" + SockNumber + "_Hub";
                 m_GTSsocket = GT.Socket.GetSocket(socket_Number_To_Hub_int(hubAP5, m_iSocketNumber), true, null, null);
             }
             else
             {
                m_sName = "S:" + SockNumber + "_Brd";
                m_GTSsocket = GT.Socket.GetSocket(m_iSocketNumber, true, null, null);
             }

             initializePins(sInputPins);
        }


        public MySocket( int SockNumber, string[] sInputPins)
        {

            m_iNumOfPins = sInputPins.Length;

            m_iSocketNumber = SockNumber;
            if (m_iSocketNumber<0)
            {
                m_bFromHub = true;
                m_sName = "S:" + SockNumber + "_Hub";
                m_GTSsocket = GT.Socket.GetSocket(socket_Number_To_Hub_int(Program._hub, m_iSocketNumber), true, null, null);
            }
            else
            {
                m_bFromHub = false;
                m_sName = "S:" + SockNumber + "_Brd";
                m_GTSsocket = GT.Socket.GetSocket(m_iSocketNumber, true, null, null);
            }

            initializePins(sInputPins);
        }
       
        private void initializePins(string[] sInputPins)
        {
            m_arr_mpSocketsPins = new MyPin[m_iNumOfPins];
            for (int i = 0; i<m_iNumOfPins; i++)
            {
                string[] arr_sPinNumberAndStatus = sInputPins[i].Split(':');
                int iPinNumber = Convert.ToInt32(arr_sPinNumberAndStatus[0]);
                int iPinStatus = Convert.ToInt32(arr_sPinNumberAndStatus[1]);
                m_arr_mpSocketsPins[i] = new MyPin(iPinNumber, m_iSocketNumber, new GT.Interfaces.DigitalOutput(m_GTSsocket, (GT.Socket.Pin)iPinNumber, false, null), iPinStatus);
            }
        }
        
        public string getName()
        {
            return m_sName;
        }

        public void setStatusToallPins(bool status)
        {
            for (int i = 0; i < m_arr_mpSocketsPins.Length; i++)
            {
                m_arr_mpSocketsPins[i].setStatus(status);
            }
        }
        public String getStatusList()
        {
            String list = "";
            for (int i = 0; i < m_arr_mpSocketsPins.Length; i++)
            {
                if (getPinStatus(i) == true)
                {
                 //   list += getPin(i).get_pinNum() + ",";
                    list += getPin(i).get_pinNum() + ",";
                }
            }
            //  String LastChar = list.Substring(list.Length);
            if (list.Length > 0)
            {
                list = list.Substring(0, list.Length - 1);
            }
            else
            {
                list = "All Open";
            }
            return list;
        }


        public MyPin getPin(int i)
        {
            return m_arr_mpSocketsPins[i];
        }

        public bool getPinStatus(int i)
        {
            return m_arr_mpSocketsPins[i].status();
        }

        public void setPinStatus(int i, bool writeStatus)
        {
            m_arr_mpSocketsPins[i].setStatus(writeStatus);
        }

        public void changePinStatus(int i)
        {
            m_arr_mpSocketsPins[i].setStatus(!m_arr_mpSocketsPins[i].status());
        }

        public void revertToInitialState()
        {
            for (int i = 0; i < m_arr_mpSocketsPins.Length; i++)
              m_arr_mpSocketsPins[i].revertToInitialState();
        }
        public int getNum()
        {
            return m_iSocketNumber;
        }
        private int socket_Number_To_Hub_int(HubAP5 hubAP5,int iSocketNumber)
        {
            if (iSocketNumber < 0)
                iSocketNumber = iSocketNumber * (-1);
            switch (iSocketNumber)
            {
                case 1: 
                    return hubAP5.HubSocket1;
                case 2: 
                    return hubAP5.HubSocket2;
                case 3: 
                    return hubAP5.HubSocket3;
                case 4: 
                    return hubAP5.HubSocket4;
                case 5: 
                    return hubAP5.HubSocket5;
                case 6: 
                    return hubAP5.HubSocket6;
                case 7:
                    return hubAP5.HubSocket7;
                case 8: 
                    return hubAP5.HubSocket8;
            }
            return hubAP5.HubSocket1;
        }
        public MyPin[] getPinArr()
        {
            return m_arr_mpSocketsPins;
        }
    }
}
