using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Touch;

using GTI = Gadgeteer.Interfaces;
using Gadgeteer.Interfaces;
using GT = Gadgeteer;

using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
namespace BioProject1._4.Wrappers
{
   public class MyPin
    {
       private bool bInitialState;
        private string Name;
        bool m_bIsopen;
        private int pressed;
        private int m_iSocketNum;
        private int m_iPinNum;
        private GT.Interfaces.DigitalOutput m_GT_I_DigOut_Pin;
        public MyPin(int pinNum, int socketNum, GT.Interfaces.DigitalOutput GT_I_DigOut_Pin )
        {
            m_iSocketNum = socketNum;
            m_GT_I_DigOut_Pin = GT_I_DigOut_Pin;
            m_iPinNum = pinNum;
            Name = "S" + socketNum + "P" + pinNum;
            pressed = 0;

            m_bIsopen = false;
        }

        public MyPin(int pinNum, int socketNum, GT.Interfaces.DigitalOutput GT_I_DigOut_Pin, int iStatus)
        {
            m_iSocketNum = socketNum;
            m_GT_I_DigOut_Pin = GT_I_DigOut_Pin;
            m_iPinNum = pinNum;
            Name = "S" + socketNum + "P" + pinNum;
            pressed = 0;
            bInitialState = (iStatus == 1);
            setStatus(bInitialState);
            
        }

        public bool status()
        {
            return m_bIsopen;
        }

        public string get_name()
        {
            return Name;
        }
       
        public int get_socketNum()
        {
            return m_iSocketNum;
        }

        public int get_pinNum()
        {
            return m_iPinNum;
        }


        public int get_pressed()
        {
            return pressed;
        }
        public void incPressed()
        {
            pressed++;
        }

        public void setStatus(bool op)
        {
            m_GT_I_DigOut_Pin.Write(op);
            m_bIsopen = op;
        }
        public void setStatus(int op)
        {
            m_GT_I_DigOut_Pin.Write(m_bIsopen = (op == 1));
        //    m_bIsopen = (op == 1);
        }
        public void revertToInitialState()
        {
            setStatus(bInitialState);
        }

    }
}
