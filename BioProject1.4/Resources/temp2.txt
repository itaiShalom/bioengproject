﻿<Glide Version="1.0.7">
  <Window Name="GUI2" Width="320" Height="240" BackColor="FFFFFF">
    <TextBlock Name="m2p12" X="285" Y="130" Width="20" Height="20" Alpha="255" Text="12" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p11" X="260" Y="130" Width="20" Height="20" Alpha="255" Text="11" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p10" X="235" Y="130" Width="20" Height="20" Alpha="255" Text="10" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p9" X="210" Y="130" Width="20" Height="20" Alpha="255" Text="9" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p8" X="185" Y="130" Width="20" Height="20" Alpha="255" Text="8" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p7" X="160" Y="130" Width="20" Height="20" Alpha="255" Text="7" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p6" X="135" Y="130" Width="20" Height="20" Alpha="255" Text="6" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p5" X="110" Y="130" Width="20" Height="20" Alpha="255" Text="5" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p4" X="85" Y="130" Width="20" Height="20" Alpha="255" Text="4" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p3" X="60" Y="130" Width="20" Height="20" Alpha="255" Text="3" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p2" X="35" Y="130" Width="20" Height="20" Alpha="255" Text="2" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p1" X="10" Y="130" Width="20" Height="20" Alpha="255" Text="1" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
	 <TextBlock Name="m2p12" X="10"  Y="55" Width="20" Height="20" Alpha="255" Text="12" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p11" X="35"  Y="55" Width="20" Height="20" Alpha="255" Text="11" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p10" X="60"  Y="55" Width="20" Height="20" Alpha="255" Text="10" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p9" X="85"  Y="55" Width="20" Height="20" Alpha="255" Text="9" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p8" X="110"  Y="55" Width="20" Height="20" Alpha="255" Text="8" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p7" X="135"  Y="55" Width="20" Height="20" Alpha="255" Text="7" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p6" X="160"  Y="55" Width="20" Height="20" Alpha="255" Text="6" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p5" X="185"  Y="55" Width="20" Height="20" Alpha="255" Text="5" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p4" X="210"  Y="55" Width="20" Height="20" Alpha="255" Text="4" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p3" X="235"  Y="55" Width="20" Height="20" Alpha="255" Text="3" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p2" X="260"  Y="55" Width="20" Height="20" Alpha="255" Text="2" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <TextBlock Name="m2p1" X="285"  Y="55" Width="20" Height="20" Alpha="255" Text="1" TextAlign="Center" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="True"/>
    <Slider Name="slider1" X="5" Y="210" Width="140" Height="20" Alpha="255" Direction="horizontal" SnapInterval="1" TickInterval="128" TickColor="000000" KnobSize="20" Minimum="0" Maximum="128" Value="64"/>
    <Slider Name="slider2" X="140" Y="210" Width="140" Height="20" Alpha="255" Direction="horizontal" SnapInterval="1" TickInterval="128" TickColor="000000" KnobSize="20" Minimum="0" Maximum="128" Value="64"/>
    <TextBlock Name="R6C" X="23" Y="180" Width="40" Height="20" Alpha="255" Text="R6" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R9C" X="160" Y="180" Width="40" Height="20" Alpha="255" Text="R9" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R1V" X="63" Y="180" Width="40" Height="20" Alpha="255" Text="64" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R2V" X="200" Y="180" Width="40" Height="20" Alpha="255" Text="64" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="connection" X="170" Y="1" Width="134" Height="32" Alpha="255" Text="No connection" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <Image Name="hujiLogo" X="280" Y="0" Width="40" Height="44" Alpha="255"/>
    <TextBlock Name="IPBox" X="5" Y="0" Width="150" Height="32" Alpha="255" Text="IP: 0.0.0.0" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="n1" X="285" Y="73" Width="20" Height="20" Alpha="255" Text="1" TextAlign="Center" TextVerticalAlign="Top" Font="4" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="m1" X="10" Y="24" Width="65" Height="20" Alpha="255" Text="Manifold 1" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="m2" X="10" Y="100" Width="65" Height="20" Alpha="255" Text="Manifold 2" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="fc2a05" ShowBackColor="False"/>
    <Button Name="save" X="288" Y="208" Width="32" Height="32" Alpha="255" Text="Go" Font="2" FontColor="000000" DisabledFontColor="808080" TintColor="01255" TintAmount="0"/>
    <TextBox Name="status" X="84" Y="0" Width="80" Height="32" Alpha="255" Text="Status" TextAlign="Left" Font="4" FontColor="000000"/>
    <TextBlock Name="upTime" X="90" Y="100" Width="100" Height="20" Alpha="255" Text="Up Time:" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="waitTime" X="188" Y="100" Width="100" Height="20" Alpha="255" Text="Wait Time:" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
  </Window>
</Glide>