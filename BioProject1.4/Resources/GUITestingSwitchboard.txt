<Glide Version="1.0.7">
  <Window Name="GUI3" Width="480" Height="272" BackColor="FFFFFF">
    <TextBlock Name="option0" X="20" Y="60" Width="20" Height="20" Alpha="255" Text="0" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option1" X="57" Y="60" Width="20" Height="20" Alpha="255" Text="1" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option2" X="92" Y="60" Width="20" Height="20" Alpha="255" Text="2" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option3" X="129" Y="60" Width="20" Heightcal="20" Alpha="255" Text="3" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option4" X="166" Y="60" Width="20" Height="20" Alpha="255" Text="4" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option5" X="203" Y="60" Width="20" Height="20" Alpha="255" Text="5" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option6" X="240" Y="60" Width="20" Height="20" Alpha="255" Text="6" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option7" X="277" Y="60" Width="20" Height="20" Alpha="255" Text="7" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option8" X="314" Y="60" Width="20" Height="20" Alpha="255" Text="8" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option9" X="351" Y="60" Width="20" Height="20" Alpha="255" Text="9" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option10" X="388" Y="60" Width="20" Height="20" Alpha="255" Text="10" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="option11" X="425" Y="74" Width="20" Height="20" Alpha="255" Text="11" TextAlign="Center" TextVerticalAlign="Center" Font="4" FontColor="0" BackColor="000000" ShowBackColor="True"/>
    <TextBlock Name="connection" X="119" Y="0" Width="134" Height="32" Alpha="255" Text="Awaiting orders" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="IPBox" X="5" Y="0" Width="150" Height="32" Alpha="255" Text="IP" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <Image Name="hujiLogo" X="440" Y="0" Width="40" Height="44" Alpha="255"/>
    <Image Name="Switchboards" X="20" Y="98" Width="460" Height="100" Alpha="255"/>
    <Button Name="backFromSwitch" X="440" Y="240" Width="40" Height="32" Alpha="255" Text="Back" Font="4" FontColor="000000" DisabledFontColor="808080" TintColor="000000" TintAmount="0"/>
    <Slider Name="1s" X="2" Y="240" Width="140" Height="20" Alpha="255" Direction="horizontal" SnapInterval="128" TickInterval="128" TickColor="000000" KnobSize="20" Minimum="0" Maximum="128" Value="64"/>
    <Slider Name="2s" X="240" Y="240" Width="140" Height="20" Alpha="255" Direction="horizontal" SnapInterval="128" TickInterval="128" TickColor="000000" KnobSize="20" Minimum="0" Maximum="128" Value="64"/>
    <TextBlock Name="R6C" X="20" Y="210" Width="40" Height="20" Alpha="255" Text="R6" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R9C" X="262" Y="210" Width="40" Height="20" Alpha="255" Text="R9" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R1V" X="97" Y="210" Width="40" Height="20" Alpha="255" Text="64" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
    <TextBlock Name="R2V" X="332" Y="210" Width="40" Height="20" Alpha="255" Text="64" TextAlign="Left" TextVerticalAlign="Top" Font="2" FontColor="0" BackColor="000000" ShowBackColor="False"/>
  </Window>
</Glide>