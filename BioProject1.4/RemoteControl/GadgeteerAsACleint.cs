
using System;
using Microsoft.SPOT;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using BioProject1._4.Utils;
namespace BioProject1._4.RemoteControl
{
    public sealed class GadgeteerAsACleint
    {
        private static  GadgeteerAsACleint instance;
   private static object syncRoot = new Object();

        string ip = "192.168.1.17";

        int port = 3000;

        IPEndPoint hostep;

        Socket s;

        byte[] buffer = new byte[32768];
        
        public GadgeteerAsACleint(string sIP, int iPort) 
        {
            IPAddress host = IPAddress.Parse(ip);

            hostep = new IPEndPoint(host, port);
        }
        public static GadgeteerAsACleint Instance(string sIP, int iPort) 
        {
         if (instance == null) 
         {
            lock (syncRoot) 
            {
               if (instance == null) 
                  instance = new GadgeteerAsACleint(sIP,iPort);
            }
         }

         return instance;
      }
   
       
   

    public void startSession(Definitions.CommandsToRasp command)
    {
       //     Connecting to the server.

            s = new Socket(AddressFamily.InterNetwork, 
            SocketType.Stream, ProtocolType.Tcp);

            s.Connect(hostep);
            switch (command)
            {
                case Definitions.CommandsToRasp.enTakePicture:
                    askForImageSession();
                    break;
            }
        }

        private void askForImageSession()
        {

            Debug.Print("\n[CLIENT] We are connected to the server: " + ip + " on port: " + port);

            Send("Hello from gadgeteer");
            Send(Definitions.CommandsToRasp.enTakePicture.ToString());
           Thread readTread = new Thread(new ThreadStart(Read));
      //      Read();
          readTread.Start();    
        }
        

        private void Read()
        {
            while (true)
            {
                try
                {
                    // Reading server messages.

                    int receivedDataLength = s.Receive(buffer, SocketFlags.None);

                    string message = 
                    //Encoding.ASCII.GetString(buffer, 0, receivedDataLength);
                     new String(System.Text.Encoding.UTF8.GetChars(buffer));
                    Debug.Print("\nMessage from the server: " + message);
                }
                catch (Exception e)
                {

                    Debug.Print(e.Message);
                }
            }
        }
        private void Send(string msg)
        {
            // Sending message to the server.
            buffer = Encoding.UTF8.GetBytes(msg);
            s.Send(buffer);
        }
    }
}
