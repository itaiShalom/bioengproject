using System;
using Microsoft.SPOT;
using System.Threading;
using BioProject1._4.Utils;
using BioProject1._4.ProgramHandeling;

namespace BioProject1._4.RemoteControl
{
    class ScenarioRunner
    {
        private Thread m_thread = null;
        private Scenario m_Scenario;
        private Definitions.LastGui m_enGoBackTo;
        public ScenarioRunner(Scenario sc, Definitions.LastGui enCalledFrom) { m_Scenario = sc; m_enGoBackTo = enCalledFrom; }
        public void Start()
        {
            m_thread = new Thread(_Run);
            m_thread.Priority = ThreadPriority.Highest;
            m_thread.Start();
        }
        public bool isAlive()
        {
            return m_thread.IsAlive;
        }

        public void Abort()
        {
            m_thread.Abort();
        }
        public void Join()
        {
            m_thread.Join();
        }
        public bool isStageOn()
        {
            return m_Scenario.m_bIsOn;
            // m_thread.Join();
        }
        private void _Run()
        {
            m_Scenario.run(m_enGoBackTo);
        }
    }
}
