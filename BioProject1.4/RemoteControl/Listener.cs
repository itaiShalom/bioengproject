using System;
using System.IO;
using System.Text;
using System.Net;
using Microsoft.SPOT;
using System.Collections;
using System.Threading;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;
using Gadgeteer.Networking;
using System.Xml;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using System.Net.Sockets;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer.Modules.ItaiShalom;
using GHI.Premium.Net;
using GHI.Glide;
using GHI.Glide.Display;
using BioProject1._4.Utils;
using BioProject1._4.GUIs;
using BioProject1._4.Wrappers;

namespace BioProject1._4.RemoteControl
{
    class Listener
    {
        private Thread m_thread = null;
        public Listener() { }

        public void Start()
        {
            m_thread = new Thread(_Run);
            m_thread.Start();
        }

        public void Abort()
        {
            m_thread.Abort();
        }
        public void Join()
        {
            m_thread.Join();
        }
        /*
                      Timer MyTimer = new Timer(new TimerCallback(ClockTimer_Tick), null, 5000, 0);
            Glide.MessageBoxManager.Show("Connected to Internet","Notification");

            GUIMainManu.show();
          // getConnections();
        }
        static void ClockTimer_Tick(object sender)
        {
            if (Glide.MessageBoxManager.IsOpen)
                Glide.MessageBoxManager.Hide();
     //       GUIMainManu.showDisabled();
        }
         */


        public void _Run()
        {

            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ipaddressReady = IPAddress.Parse(Definitions.SERVER_IP);
            IPEndPoint localEndPoint = new IPEndPoint(ipaddressReady, Definitions.PORT);
            server.Bind(localEndPoint);
            server.Listen(1);

            while (true)
            {
                // Wait for a client to connect.
                Debug.Print("waiting for connection");

                try
                {
                    Socket clientSocket = server.Accept();
                    IPEndPoint remoteIpEndPoint = clientSocket.RemoteEndPoint as IPEndPoint;
                    if (!Definitions.CLIENT_IP.Equals(remoteIpEndPoint.Address.ToString()))
                    {
                        Definitions.CLIENT_IP = remoteIpEndPoint.Address.ToString();
                        Timer MyTimer = new Timer(new TimerCallback(ClockTimer_Tick), null, 5000, 0);
                        Glide.MessageBoxManager.Show("New session from: "+Definitions.CLIENT_IP, "Notification");
                    }
                    
                    Definitions.WAS_REMOTLEY_CONTROLED = true;

                    Debug.Print("Connection accpeted, new thread");
                    // Process the client request.  true means asynchronous.
                    new ProcessClientRequest(clientSocket, true);
                }
                catch (System.Net.Sockets.SocketException e)
                {
                    Debug.Print("Socket erorr");
                    server.Close();
                    Definitions.SERVER_IP = "";
                    Definitions.CLIENT_IP = "";
                    GUIMainManu.show();
                    break;
                   
                }
         //       GUIMainManu.show();

            }
        }
        static void ClockTimer_Tick(object sender)
        {
            if (Glide.MessageBoxManager.IsOpen)
                Glide.MessageBoxManager.Hide();
        }
    }
}
