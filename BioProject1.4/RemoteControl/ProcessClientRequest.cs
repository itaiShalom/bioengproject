using System;
using Microsoft.SPOT;
using System.Net.Sockets;
using System.Threading;
using GHI.Glide;
using GHI.Glide.UI;
using System.Text;
using System.IO;
using System.Collections;
using Microsoft.SPOT.Presentation;
using GT = Gadgeteer;
using BioProject1._4.GUIs;
using BioProject1._4.Utils;
using GHI.Premium.Hardware;
using BioProject1._4.ProgramHandeling;
namespace BioProject1._4.RemoteControl
{
    public class ProcessClientRequest
    {

        private Socket m_clientSocket;

        /// <summary>
        /// The constructor calls another method to handle the request, but can 
        /// optionally do so in a new thread.
        /// </summary>
        /// <param name="clientSocket"></param>
        /// <param name="asynchronously"></param>
        public ProcessClientRequest(Socket clientSocket, Boolean asynchronously)
        {
            m_clientSocket = clientSocket;

            if (asynchronously)
            {
                // Spawn a new thread to handle the request.
                Thread t = new Thread(ProcessRequest);
                t.Priority = ThreadPriority.Lowest;
                t.Start();
            }
            else
            {
                ProcessRequest();
            }
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        private void ProcessRequest()
        {

            const Int32 c_microsecondsPerSecond = 1000000;

            // 'using' ensures that the client's socket gets closed.
            using (m_clientSocket)
            {
                Debug.Print("conncetion");
                // Wait for the client request to start to arrive.
                Byte[] buffer = new Byte[1024];
                if (m_clientSocket.Poll(500 * c_microsecondsPerSecond,
                    SelectMode.SelectRead))
                {
                    // If 0 bytes in buffer, then the connection has been closed, 
                    // reset, or terminated.
                    if (m_clientSocket.Available == 0)
                        return;

                    // Read the first chunk of the request (we don't actually do 
                    // anything with it).
                    String message = readMessage(m_clientSocket);
                    if (message.IndexOf("Introduction") > -1)
                    {
                        sendMessage(m_clientSocket, "OK");
                        message = readMessage(m_clientSocket);

                        if (message.IndexOf("SendRTC") > -1)
                        {
                            sendMessage(m_clientSocket, ("OK"));
                            string sRTC = readMessage(m_clientSocket);
                            string[] s_arrClock = sRTC.Split('_');
                            int[] arr_iDate = new int[s_arrClock.Length];
                            for (int i = 0; i < arr_iDate.Length; i++)
                            {
                                arr_iDate[i] = Convert.ToInt32(s_arrClock[i]);
                            }
                            DateTime DT = new DateTime(arr_iDate[0], arr_iDate[1], arr_iDate[2], arr_iDate[3], arr_iDate[4], 0); // This will set the clock to 9:30:00 on 9/15/2014
                            RealTimeClock.SetTime(DT); //This will set the hardware Real-time Clock to what is in DT
                            sendMessage(m_clientSocket, ("OK"));
                            message = readMessage(m_clientSocket);
                        }

                        if (message.IndexOf("GUIMainManu") > -1)
                        {
                            GUIMainManu.showDisabled();
                            sendMessage(m_clientSocket, ("OK"));
                        }
                        return;
                    }



                    if (message.IndexOf("GUITestingSwitchboards") > -1)
                    {
                        GUITestingSwitchboards.showDisabled(0);
                        sendMessage(m_clientSocket, ("OK"));
                        return;
                    }

                    if (message.IndexOf("GUIMainManu") > -1)
                    {
                        GUIMainManu.showDisabled();
                        sendMessage(m_clientSocket, ("OK"));
                        return;
                    }

                    if (message.IndexOf("About") > -1)
                    {
                        About.show();
                        sendMessage(m_clientSocket, ("OK"));
                        return;
                    }

                    if (message.IndexOf("GUITestingManifolds") > -1)
                    {
                        GUITestingManifolds.showDisabled();
                        sendMessage(m_clientSocket, ("OK"));
                        return;
                    }

                    if (message.IndexOf("CreateScenario") > -1)
                    {
                        sendMessage(m_clientSocket, ("OK"));

                        int iXmlSize = Convert.ToInt32(readMessage(m_clientSocket));
                        sendMessage(m_clientSocket, ("OK"));
                        String sXML = "";
                        while (sXML.Length < iXmlSize)
                        {
                            sXML = readMessage(m_clientSocket, iXmlSize);
                            if (sXML.Length < iXmlSize)
                            {

                                sendMessage(m_clientSocket, ("BAD"));
                            }
                        }
                        Debug.Print(sXML.Length.ToString());
                        sendMessage(m_clientSocket, ("OK"));
                        Scenario sc = new Scenario(sXML);
                        ScenarioRunner scThread;
                        if (sc.getScenarioType().Equals("Switchboard"))
                        {
                            scThread = new ScenarioRunner(sc, Definitions.LastGui.enSwtichboardDisabled);
                        }
                        else
                        {
                            scThread = new ScenarioRunner(sc, Definitions.LastGui.enManifoldDisabled);
                        }
                        scThread.Start();
                        while (scThread.isAlive() || !sc.isEmpty())
                        {
                            if (scThread.isAlive() && sc.isEmpty())
                            {
                                Thread.Sleep(1000);
                                continue;
                            }
                            ImageObj ioImageData = sc.getImage();

                            sendMessage(m_clientSocket, ("SENDING_IMAGE"));
                            readMessage(m_clientSocket);
                            byte[] arr = ioImageData.imageData;
                            int iStageNum = ioImageData.iStageNumber;
                            sendMessage(m_clientSocket, (iStageNum.ToString()));
                            readMessage(m_clientSocket);
                            sendMessage(m_clientSocket, (arr.Length.ToString()));
                            readMessage(m_clientSocket);
                            while (scThread.isStageOn())
                            {    
                                Thread.Sleep(100);
                            }
                            sendMessage(m_clientSocket, arr);
                            readMessage(m_clientSocket);
                            // sendMessage(m_clientSocket, Encoding.UTF8.GetBytes("OK"));

                            Thread.Sleep(1000);
                        }
                        sendMessage(m_clientSocket, ("DONE_SENDING_IMAGE"));
                        //  readMessage(m_clientSocket);
                        //sc.run(Definitions.LastGui.enSwtichboardDisabled);
                    }

                    m_clientSocket.Close();
                    Debug.Print("Session ended");
                }
            }

        }

        private void sendMessage(Socket m_clientSocket, byte[] buf)
        {
            Debug.Print("Sending array of byes");
            //      byte[] buf = Encoding.UTF8.GetBytes(message);
            // byte[] buf = Encoding.UTF8.GetBytes(message);

            int offset = 0;
            int ret = 0;
            int len = buf.Length;
            while (len > 0)
            {
                ret = m_clientSocket.Send(buf, offset, len, SocketFlags.None);
                len -= ret;
                offset += ret;
            }
        }

        private void sendMessage(Socket m_clientSocket, string message)
        {
            Debug.Print("Sending: " + message);
            byte[] buf = Encoding.UTF8.GetBytes(message);
            // byte[] buf = Encoding.UTF8.GetBytes(message);

            int offset = 0;
            int ret = 0;
            int len = buf.Length;
            while (len > 0)
            {
                ret = m_clientSocket.Send(buf, offset, len, SocketFlags.None);
                len -= ret;
                offset += ret;
            }
        }

        private String readMessage(Socket myScoket)
        {
            int totalread = 0, currentread = 0;
            byte[] sizeinfo = new byte[1024];
            currentread = totalread = myScoket.Receive(sizeinfo, 1024, SocketFlags.None);
            currentread = myScoket.Receive(sizeinfo, totalread, sizeinfo.Length - totalread, SocketFlags.None);
            String message = new String(System.Text.Encoding.UTF8.GetChars(sizeinfo));


            if (message.Length < currentread)
            {
                Debug.Print("Clearing buffer");
                totalread = 0; currentread = 0;
                sizeinfo = new byte[1024];
                currentread = totalread = myScoket.Receive(sizeinfo, 1024, SocketFlags.None);
                currentread = myScoket.Receive(sizeinfo, totalread, sizeinfo.Length - totalread, SocketFlags.None);
                return "";
            }
            Debug.Print("Read: " + message);
            Debug.Print("Actual size read: " + message.Length);
            return message;
        }
        private String readMessage(Socket myScoket, int expected)
        {
            int totalread = 0, currentread = 0;
            byte[] sizeinfo = new byte[1024];
            currentread = totalread = myScoket.Receive(sizeinfo, 1024, SocketFlags.None);
            currentread = myScoket.Receive(sizeinfo, totalread, sizeinfo.Length - totalread, SocketFlags.None);
            String message = new String(System.Text.Encoding.UTF8.GetChars(sizeinfo));


            if (message.Length < expected)
            {

                Debug.Print("Clearing buffer");

                sizeinfo = new byte[1024];
                //    currentread = totalread = myScoket.Receive(sizeinfo, 1024, SocketFlags.None);
                currentread = myScoket.Receive(sizeinfo, totalread, sizeinfo.Length - totalread, SocketFlags.None);
                String message2 = new String(System.Text.Encoding.UTF8.GetChars(sizeinfo));
                return message + message2;
            }
            Debug.Print("Read: " + message);
            Debug.Print("Actual size read: " + message.Length);
            return message;
        }
    }
}
