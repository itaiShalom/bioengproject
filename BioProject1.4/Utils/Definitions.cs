using System;
using Microsoft.SPOT;

namespace BioProject1._4.Utils
{
    public class Definitions
    {
        public static readonly string SOCKET = "Sockets";
        public static readonly string MANIFOLD = "Manifolds";
        public static readonly string SWITCHBOARD = "Switchboards";
        public static readonly string RESISTORS = "Resistors";
        public static string PROGRAM_DIRECTORY = "\\Programs\\"; //Will be updated to root\program
        public static string IMAGES_DIRECTORY = "\\Images\\"; //Will be updated to root\program
        public static readonly string PROGRAM_CONFIGURATION = "ConfigFile.txt";
        public static string ROOT_DIRECTORY = ""; //Will be updated to root directory of storage device
        public static readonly string HUJI_LOGO = "hujiLogo";
        public static readonly int MAIN_MANU_OPTIONS = 3;
        public static readonly int LOAD_PROGRAM_OPTIONS = 6;
        public static readonly int TESTING_MANU_OPTIONS = 5;
        public static readonly int X = 480;
        public static readonly int Y = 272;
        public static readonly string MANU_OPTION = "option";
        public static readonly int PORT = 3000;
        public static readonly Microsoft.SPOT.Presentation.Media.Color CLOSE_COLOR = GHI.Glide.Colors.Red;
        public static readonly Microsoft.SPOT.Presentation.Media.Color OPEN_COLOR = GHI.Glide.Colors.Green;
        public static readonly Microsoft.SPOT.Presentation.Media.Color NOT_VALID_COLOR = GHI.Glide.Colors.Fuchsia;
        public static readonly string SYSTEM_IP_XML = "ServerIP";
        public static readonly string CLIENT_IP_XML = "ClientIP";
        public static bool WAS_REMOTLEY_CONTROLED = false;
        public static readonly string INTERNET_STATUS = "Internet Status: ";
        public static string INITIAL_INTERNET_STATUS = INTERNET_STATUS+"Not Connected";
        public static string SERVER_IP = "";
        public static string CLIENT_IP = "";
        public static readonly string NO_CLIENT_CONNECTED = "No Client Connected";

        public enum TunnelStatus
        {
            OPEN,
            CLOSE,
            NOT_VALID
        }
        public  enum LastGui
        {
            enMainManu,
            enTestingGUI,
            enTestingSocketsGui,
            enTestingPins,
            enManifold,
            enResistors,
            enLoadProgramGUI,
            enSwtichboardDisabled,
            enManifoldDisabled,
        }
        public enum CommandsToRasp
        {
            enTakePicture
        }
    }
}
