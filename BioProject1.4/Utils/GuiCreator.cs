using System.Threading;
using System.Text;
using Microsoft.SPOT;
using System.IO;
using Microsoft.SPOT.IO;
using GHI.Glide;
using GHI.Glide.Display;
using BioProject1._4.GUIs;
using BioProject1._4.Wrappers;
namespace BioProject1._4.Utils
{
    class GuiCreator
    {
     //   public static string createWelcomeScreen()
        private static string sSocketGUI = "";
        private static string sManifoldGUI = "";
        private static string sSwitchboardGUI = "";
//        private static string sPinsGUI = "";
        private static bool bPinsInit = false;
        private static string[] allPinsManus;
        private static int ibuttonSpace = 10;
       /*
        private void writeToFile()
        {
            string path = Definitions.ROOT_DIRECTORY + @"\" + "OpenGui1" + ".txt";
            StreamWriter textFile = new StreamWriter(path);
            string xml = "<Glide Version=\"1.0.7\">  " + "<Window Name=\"GUI1\" Width=\"400\" Height=\"240\" BackColor=\"FFFFFF\">    " + "<Button Name=\"option1\" X=\"95\" Y=\"54\" Width=\"130\" Height=\"32\" Alpha=\"255\" Text=\"Free Mode\" Font=\"2\" FontColor=\"000000\" DisabledFontColor=\"808080\" TintColor=\"000000\" TintAmount=\"0\"/>    " + "</Window>" + "</Glide>";
            textFile.Write("<Glide Version=\"1.0.7\">  ");
            textFile.Write("<Window Name=\"GUI1\" Width=\"400\" Height=\"240\" BackColor=\"FFFFFF\">    ");
            textFile.Write("<Button Name=\"option1\" X=\"95\" Y=\"54\" Width=\"130\" Height=\"32\" Alpha=\"255\" Text=\"Free Mode\" Font=\"2\" FontColor=\"000000\" DisabledFontColor=\"808080\" TintColor=\"000000\" TintAmount=\"0\"/>    ");
            textFile.Write("</Window>");
            textFile.Write("</Glide>");
            textFile.Flush();
            textFile.Close();
        }
        */
        public static void loadLogo(GHI.Glide.Display.Window window)
        {
            GHI.Glide.UI.Image logo = (GHI.Glide.UI.Image)window.GetChildByName(Definitions.HUJI_LOGO);
            logo.Bitmap = Resources.GetBitmap(Resources.BitmapResources.hujiLogo);
        }

        public static void loadSwitchboard(GHI.Glide.Display.Window window)
        {
            GHI.Glide.UI.Image logo = (GHI.Glide.UI.Image)window.GetChildByName(Definitions.SWITCHBOARD);
            logo.Bitmap = Resources.GetBitmap(Resources.BitmapResources.switchb);
        }
        public static string createGuiManifoldTesting()
        {
            if (sManifoldGUI.Length != 0)
                return sManifoldGUI;
            sManifoldGUI = addOpen();
            sManifoldGUI += addWindow("Manifolds");
            sManifoldGUI += addManifoldButtons();

            sManifoldGUI += addBack(0);
            sManifoldGUI += addLogo();
            sManifoldGUI += addClose();
            return sManifoldGUI;
        }

        public static string createGuiPinsTesting(int iChosenSocketIndex)
        {
            if (!bPinsInit)
                InitializePinsManus();
            if (allPinsManus[iChosenSocketIndex].Length != 0)
                return allPinsManus[iChosenSocketIndex];
            MySocket ms = (Program.TestingTool.getSockets()[iChosenSocketIndex]);
            allPinsManus[iChosenSocketIndex] = addOpen();
            allPinsManus[iChosenSocketIndex] += addWindow("Pins");
            allPinsManus[iChosenSocketIndex] += addPinsCheckBox(ms);                    //-1 since no option 0
            allPinsManus[iChosenSocketIndex] += addBack(ms.getPinArr().Length + 1); //  for back button and for all button
            allPinsManus[iChosenSocketIndex] += addLogo();
            allPinsManus[iChosenSocketIndex] += addClose();
            return allPinsManus[iChosenSocketIndex];
        }

        private static string addManifoldButtons()
        {
         //   int iX = (ibuttonSpace * (i )) + (iButtonWidth *(i-1));
            string s = "";
            int iButtonWidth;
            int iYStart = 50;
            int iY = 30;
            int iXStart = 10;
            int iX = 0;
            string sName = "";

            int iNumOfManifolds = Program.TestingTool.getManifolds().Length;
            string sCurrentMan = "";
            int iButtonHight = ((Definitions.Y - (ibuttonSpace * 2 * (iNumOfManifolds + 1))) -32)/ iNumOfManifolds;
            if (iButtonHight > 30) { iButtonHight = 30; }
            for (int i = 0; i < iNumOfManifolds; i++)
            {
                 iY = 30;
                 iX = 20;
                 int iNumOfTunnels = Program.TestingTool.getManifolds()[i].getNumOfTunnels();
                 sCurrentMan = "M" + (i).ToString(); 
               
                 iButtonWidth = (Definitions.X - (ibuttonSpace * (iNumOfTunnels + 1))) / iNumOfTunnels;
                 iY = iYStart + (ibuttonSpace * 2 * (i)) + (iButtonHight * i);
                 s += "<TextBlock Name=\"" + sCurrentMan + "\" X=\"" + iXStart + "\" Y=\""+iY+"\" Width=\"20\" Height=\"20\" Alpha=\"255\" Text=\"" + sCurrentMan + "\" TextAlign=\"Left\" TextVerticalAlign=\"Top\" Font=\"2\" FontColor=\"0\" BackColor=\"000000\" ShowBackColor=\"False\"/> ";
                for (int j = 0; j < iNumOfTunnels; j++)
                {
                    sName = "M:" + (i) + ":P:" + (j);
                     
                    iX = ( (ibuttonSpace-3) * (j+1)) + (iButtonWidth * (j )) + iXStart*3;
                    s+= "<TextBlock Name=\""+sName+"\" X=\""+iX+"\" Y=\""+iY+"\" Width=\""+iButtonWidth+"\" Height=\""+iButtonHight+"\" Alpha=\"255\" Text=\""+(j)+"\" TextAlign=\"Center\" TextVerticalAlign=\"Top\" Font=\"2\" FontColor=\"0\" BackColor=\"fc2a05\" ShowBackColor=\"True\"/> ";
                }
            }
            return s;
        }

        public static string createGuiSwitchboardTesting(int iSwitchboardNum)
        {
            if (sSwitchboardGUI.Length != 0)
                return sSwitchboardGUI;
            sSwitchboardGUI = addOpen();
            sSwitchboardGUI += addWindow("Switchboard");
            sSwitchboardGUI += addSwitchbardButtons(iSwitchboardNum);
            sSwitchboardGUI += addBack(Program.TestingTool.getSwitchboard()[iSwitchboardNum].getTunnelsAmount());
            sSwitchboardGUI += addLogo();
            sSwitchboardGUI += addClose();
            return sSwitchboardGUI;
        }
        public static string addSwitchbardButtons(int iSwitchboardNum)
        {
            string s="";
            int iNumOfTunnels = Program.TestingTool.getSwitchboard()[iSwitchboardNum].getTunnelsAmount();// Length;
            string sName = Definitions.MANU_OPTION;
            int iXStart = 25;
            int iJumps = 34;
            int iLocation;
            for (int i = 0; i < iNumOfTunnels; i++)
            {
                iLocation = iXStart + (iJumps * i);
                s+= "<TextBlock Name=\""+sName+i.ToString()+"\" X=\""+iLocation.ToString()+"\" Y=\"60\" Width=\"20\" Height=\"20\" Alpha=\"255\" Text=\""+i.ToString()+"\" TextAlign=\"Center\" TextVerticalAlign=\"Center\" Font=\"4\" FontColor=\"0\" BackColor=\"000000\" ShowBackColor=\"True\"/>";
            }
            s += "<Image Name=\"Switchboards\" X=\"20\" Y=\"98\" Width=\"460\" Height=\"100\" Alpha=\"255\"/>";
            return s;
        }

        public static string createGuiSocketsTesting()
        {
            if (sSocketGUI.Length != 0)
                return sSocketGUI;
            sSocketGUI = addOpen();
            sSocketGUI += addWindow("Sockets");
            sSocketGUI += addSocketsButtons();
            sSocketGUI += addBack(Program.TestingTool.getSockets().Length);
            sSocketGUI += addLogo();
            sSocketGUI += addClose();
            return sSocketGUI;
        }

        private static void InitializePinsManus()
        {
            bPinsInit = true;
            allPinsManus = new string[Program.TestingTool.getSockets().Length];
            for (int i = 0; i < Program.TestingTool.getSockets().Length; i++)
            {
                allPinsManus[i] = "";
            }
        }

        private static string addOpen()
        {
            return "<Glide Version=\"1.0.7\">";
        }
        private static string addWindow(string name)
        {
            return "<Window Name=\"" + name + "\" Width=\"" + Definitions.X + "\" Height=\"" + Definitions.Y + "\" BackColor=\"FFFFFF\">";
        }
        private static string addClose()
        {
            return " </Window> </Glide>";
        }
        //
        private static string addBack(int iBackOptionNumber)
        {
            int iButtonWidth = 100;
            string sName = "Back";
            string sButtonWidth = "100";
            string sY = (Definitions.Y - 32).ToString();
            string sX = ((Definitions.X / 2) - (iButtonWidth / 2)).ToString();
            string sOp = Definitions.MANU_OPTION + (iBackOptionNumber).ToString();
            return "<Button Name=\"" + sOp + "\" X=\"" + sX + "\" Y=\""+sY+"\" Width=\"" + sButtonWidth + "\" Height=\"32\" Alpha=\"255\" Text=\"" + sName + "\" Font=\"4\" FontColor=\"000000\" DisabledFontColor=\"808080\" TintColor=\"000000\" TintAmount=\"0\"/> ";
        }

        private static string addSocketsButtons()
        {
            string s="";
            int iNumOfSockets = Program.TestingTool.getSockets().Length;
            int iButtonWidth = (Definitions.X - (ibuttonSpace * (iNumOfSockets + 1))) / iNumOfSockets;
            string sX="";
            string sButtonWidth="";
            for (int i = 0; i < iNumOfSockets; i++)
            {
                string sOp = "option" + (i).ToString();
                string sName = Program.TestingTool.getSockets()[i].getName();
                int iX = (ibuttonSpace * (i+1 )) + (iButtonWidth *(i));
                sX = iX.ToString();
                sButtonWidth = iButtonWidth.ToString();
                string sText = "T"+(i ).ToString();
                s += "<Button Name=\""+sOp+"\" X=\""+sX+"\" Y=\"80\" Width=\""+sButtonWidth+"\" Height=\"32\" Alpha=\"255\" Text=\""+sName+"\" Font=\"4\" FontColor=\"000000\" DisabledFontColor=\"808080\" TintColor=\"000000\" TintAmount=\"0\"/> ";
                s += "<TextBox Name=\""+sText+"\" X=\""+sX+"\" Y=\"112\" Width=\""+sButtonWidth+"\" Height=\"32\" Alpha=\"255\" Text=\"\" TextAlign=\"Left\" Font=\"1\" FontColor=\"000000\"/> ";
            }
            return s;
        }
        //<CheckBox Name="p0" X="53" Y="41" Width="32" Height="32" Alpha="255" Checked="False"/>
        private static string addPinsCheckBox(MySocket ms)
        {
            string s = "";
            int iNumOfPins = ms.getPinArr().Length;
            int iButtonWidth = (Definitions.X - (ibuttonSpace * (iNumOfPins + 2))) / (iNumOfPins+1);
            string sX = "";
            string sTextName ="";
            string sPinName ="";
            string sTextPinName = "";
            int iX =0;
      //      string sButtonWidth = "";
            for (int i = 0; i < iNumOfPins+1; i++)
            {
                iX = (ibuttonSpace * (i+1)) + (iButtonWidth * (i));
                sX = iX.ToString();
                sPinName = Definitions.MANU_OPTION + i.ToString();
                sTextName = "T" + i.ToString();
                if (i == iNumOfPins )
                { sTextPinName = "All Pins";}
                else { sTextPinName = ms.getPinArr()[i].get_name(); } 
                s += "<CheckBox Name=\"" + sPinName + "\" X=\"" + sX + "\" Y=\"100\" Width=\"" + iButtonWidth + "\" Height=\"100\" Alpha=\"255\" Checked=\"False\"/>";
                s += "<TextBlock Name=\"" + sTextName + "\" X=\"" + sX + "\" Y=\"83\" Width=\"100\" Height=\"32\" Alpha=\"255\" Text=\""+sTextPinName+"\" TextAlign=\"Left\" TextVerticalAlign=\"Top\" Font=\"4\" FontColor=\"0\" BackColor=\"000000\" ShowBackColor=\"False\"/>";
            }
            
            return s;
        }
        private static string addLogo()
        {
            return "<Image Name=\"" + Definitions.HUJI_LOGO + "\" X=\"" + (Definitions.X - 40) + "\" Y=\"0\" Width=\"40\" Height=\"44\" Alpha=\"255\"/>";
        }

        public static void GoBackGUI(Definitions.LastGui enSenderGUI)
        {
            switch (enSenderGUI)
            {
                case Definitions.LastGui.enMainManu:
                    {
                    //    Program.enLast = enCurrent;
                        GUIMainManu.show();
                        return;
                    }
                case Definitions.LastGui.enTestingGUI:
                    {
                    //    Program.enLast = enCurrent;
                        GUITesting.show();
                        return;
                    }
                case Definitions.LastGui.enTestingSocketsGui:
                    {
                   //     Program.enLast = enCurrent;
                        GUITestingSockets.show();
                        return;
                    }
                case Definitions.LastGui.enSwtichboardDisabled:
                    {
                        //TODO
                        //     Program.enLast = enCurrent;
                        GUITestingSwitchboards.showDisabled(0);
                        return;
                    }
                case Definitions.LastGui.enManifoldDisabled:
                    {
                        //TODO
                        //     Program.enLast = enCurrent;
                        GUITestingManifolds.showDisabled();
                        return;
                    }
                case Definitions.LastGui.enLoadProgramGUI:
                    {
                        GUILoadProgram.show();
                        return;  
                    }
            }
        }
    }
}
