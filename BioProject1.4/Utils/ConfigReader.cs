using System;
using System.IO;
using System.Text;
using Microsoft.SPOT;
using System.Collections;
using System.Threading;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;
using Gadgeteer.Networking;
using System.Xml;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer.Modules.ItaiShalom;


using GHI.Glide;
using GHI.Glide.Display;
using BioProject1._4.GUIs;
using BioProject1._4.Wrappers;
using BioProject1._4.RemoteControl;
namespace BioProject1._4.Utils
{
    class ConfigReader
    {
    
        enum en_CurrentSectionAvilable
        {
            Sockets,
            Switchboard,
            Manifold,
            Resistors
        }
        private const string FilePath = "Application.config";
        private static MySocket[] arr_msReadySockets2 = null;
        private static MCP4131[] arr_mcpReadyResistors2 = null;
        private static MyManifold[] arr_mmReadyManiflod2 = null;
        private static MySwitchboard[] arr_mswReadySwitchboard2 = null;
        private static int iCurrentRes = 0;
        private static int iCurrentSocket = 0;
        private static int iCurrentManifold = 0;
        private static int iCurrentSwitchboard = 0;
 
        private Thread m_thread = null;
  //      en_CurrentSectionAvilable m_enCurrentSection;
        string m_sConfigPath;
        static int m_iMaxSizeFile = 10000;
  //      string m_sCurrentParsed;
        private static string sVersion = "1.0";
        public ConfigReader() { }

        public void Start()
        {
            m_thread = new Thread(_Run);
            m_thread.Start();
        }

        public void Abort()
        {
            m_thread.Abort();
        }
        public void Join()
        {
            m_thread.Join();
        }

        private void _Run()
        {
            setUpSdCard();
            startGadAsAClient();
            LoadConfig(readConfigFileToString(m_sConfigPath));
            GUIMainManu.show();
        }

        //TODO - GET IP FROM SOMEWHERE
        private void startGadAsAClient()
        {
              GadgeteerAsACleint Inst =  GadgeteerAsACleint.Instance("192.168.1.17",3000);
        }

        private static bool LoadConfig(string xmlStr)
        {
            return LoadConfig(UTF8Encoding.UTF8.GetBytes(xmlStr));
        }

        /// <summary>
        /// Loads a Window from XML bytes.
        /// </summary>
        /// <param name="xmlBytes">XML bytes.</param>
        /// <returns>Window object.</returns>
        public static bool LoadConfig(byte[] xmlBytes)
        {
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();

            xmlReaderSettings.IgnoreComments = true;
            xmlReaderSettings.IgnoreProcessingInstructions = true;
            xmlReaderSettings.IgnoreWhitespace = true;

            MemoryStream stream = new MemoryStream(xmlBytes);

            XmlReader reader = XmlReader.Create(stream, xmlReaderSettings);


            if (!reader.ReadToDescendant("SystemConfiguration"))
                throw new ArgumentException("SystemConfiguration not detected.");

            string version = reader.GetAttribute(0);
            if (version != sVersion)
                throw new ArgumentException("XML (V " + version + ") is incompatible with SystemConfiguration V " + sVersion + ".");

            stream.Seek(0, SeekOrigin.Begin);
            reader = XmlReader.Create(stream, xmlReaderSettings);

            if (!reader.ReadToDescendant("Components"))
                throw new ArgumentException("XML does not contain a Components element.");

            initializeArrays(reader);
            
           
            bool bFoundComponent;
            while (reader.Read() && !(reader.NodeType == XmlNodeType.EndElement && reader.Name == "Components"))
            {
                bFoundComponent = GetComponent(reader);

                if (bFoundComponent == false)
                {
                    ModalResult resultDel = Glide.MessageBoxManager.Show("Invalid XML file, please fix and restart",
               "Notification", ModalButtons.Ok);
                    Thread.Sleep(1);
                }
                    // throw new Exception(reader.Name + " is not a valid UI component.");
            }

            reader.Close();

//            _window.Render();

            return true;
        }

        private static void initializeArrays(XmlReader reader)
        {
            arr_mcpReadyResistors2 = new MCP4131[Convert.ToInt32(reader.GetAttribute("Resistors"))];

            arr_msReadySockets2 = new MySocket[Convert.ToInt32(reader.GetAttribute("Sockets"))];

            arr_mmReadyManiflod2 = new MyManifold[Convert.ToInt32(reader.GetAttribute("Manifolds"))];

            arr_mswReadySwitchboard2 = new MySwitchboard[Convert.ToInt32(reader.GetAttribute("Switchboards"))];
        }

        private static bool GetComponent(XmlReader reader)
        {
            switch (reader.Name)
            {
                case "Resistor":
                    arr_mcpReadyResistors2[iCurrentRes]= LoadResistor(reader);
                    iCurrentRes++;
                    return true;
                case "Socket":
                    arr_msReadySockets2[iCurrentSocket]= LoadSocket(reader);
                    iCurrentSocket++;
                    return true;
                case "Manifold":
                    arr_mmReadyManiflod2[iCurrentManifold]= LoadManifold(reader);
                    iCurrentManifold++;
                    return true;
                case "Switchboard":
                     arr_mswReadySwitchboard2[iCurrentSwitchboard]=LoadSwitchboard(reader);
                     iCurrentSwitchboard++;
                     return true;
                case "Images":
                     isSaveToFile(reader);
                     return true;
                default:
                     return false;
            }
        }

        private static MCP4131 LoadResistor(XmlReader reader)
        {
            int iSocketNumber = Convert.ToInt32(reader.GetAttribute("SocketNumber"));
            switch (iSocketNumber)
            {
                case 6:
                    Program.mcpResistor6.setTap(Convert.ToInt32(reader.GetAttribute("Resistance")));
                    return Program.mcpResistor6;
                    
                case 9:
                    Program.mcpResistor9.setTap(Convert.ToInt32(reader.GetAttribute("Resistance")));
                    return Program.mcpResistor9;

            }
            return null;
        }
        private static void isSaveToFile(XmlReader reader)
        {
            string sSaveToImageFile = reader.GetAttribute("SaveToSD");
            if (sSaveToImageFile.ToUpper().Equals("FALSE"))
            {
                Program.g_bSaveImageToFile = false;
            }
            else
            {
                Program.g_bSaveImageToFile = true;
            }
        }

        private static MySocket LoadSocket(XmlReader reader)
        {
            int iSocketNum = Convert.ToInt32(reader.GetAttribute("SocketNumber"));
            string[] arr_sPins = reader.GetAttribute("Pins").Split('-');
            return new MySocket(iSocketNum, arr_sPins);
        }
            

        private static MyManifold LoadManifold(XmlReader reader)
        {
            int iManifoldNum = Convert.ToInt32(reader.GetAttribute("ManifoldNumber"));
            string[] arr_sSockets = reader.GetAttribute("FromSockets").Split(':');
            string sToOpen = reader.GetAttribute("ToOpen");
            return new MyManifold(iManifoldNum, stringToMySockets(arr_sSockets), sToOpen);
        }

        private static MySwitchboard LoadSwitchboard(XmlReader reader)
        {
//            string[] sCommand = sLine.Split(',');
            int iSwitchBoardNum = Convert.ToInt32(reader.GetAttribute("SwitchboardNumber"));
            string[] arr_sManifolds = reader.GetAttribute("FromManifolds").Split(':');
            int iTunnelsNumber = Convert.ToInt32(reader.GetAttribute("Tunnels"));
            return new MySwitchboard(stringToMyManifolds(arr_sManifolds), iSwitchBoardNum, iTunnelsNumber);
        }

        public static string readConfigFileToString(string path)
        {
             FileStream FileHandle = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] data = new byte[m_iMaxSizeFile];
            int read_count = FileHandle.Read(data, 0, data.Length);
            FileHandle.Close();
            Debug.Print("The size of data read is: " + read_count.ToString());
            Debug.Print("Data from file:");
            
            return new string(Encoding.UTF8.GetChars(data));//, 0, read_count).Split('\r');
             
        }

        public MySocket[] socketsArrayReady()
        {
            if (arr_msReadySockets2 != null && iCurrentSocket == arr_msReadySockets2.Length)
                return arr_msReadySockets2;
            return null;
        }

        public MCP4131[] resistorsArrayReady()
        {
            if (arr_mcpReadyResistors2 != null && iCurrentRes == arr_mcpReadyResistors2.Length)
                return arr_mcpReadyResistors2;
            return null;
        }

        public MyManifold[] ManifoldsArrayReady()
        {
            if (arr_mmReadyManiflod2 != null && iCurrentManifold == arr_mmReadyManiflod2.Length)
                return arr_mmReadyManiflod2;
            return null;
        }

        public MySwitchboard[] SwitchboardArrayReady()
        {
            if (arr_mswReadySwitchboard2 != null && iCurrentSwitchboard == arr_mswReadySwitchboard2.Length)
                return arr_mswReadySwitchboard2;
            return null;
        }

        private static MySocket[] stringToMySockets(string[] arr_sMySocks)
        {
            int k = 0;
            MySocket[] arr_msSocketsToManifold = new MySocket[arr_sMySocks.Length];
            for (int i = 0; i < arr_sMySocks.Length; i++)
            {
                for (int j = 0; j < arr_msReadySockets2.Length; j++)
                {
                    if (Convert.ToInt16(arr_sMySocks[i]) == arr_msReadySockets2[j].getNum())
                    {
                        arr_msSocketsToManifold[k] = arr_msReadySockets2[j];
                        k++;
                        break;
                    }
                }
            }
            return arr_msSocketsToManifold;
        }

        private static MyManifold[] stringToMyManifolds(string[] arr_sMyMans)
        {
            int k = 0;
            MyManifold[] arr_mmManifoldsToSwitchboard = new MyManifold[arr_sMyMans.Length];
            for (int i = 0; i < arr_sMyMans.Length; i++)
            {
                for (int j = 0; j < arr_mmReadyManiflod2.Length; j++)
                {
                    if (Convert.ToInt16(arr_sMyMans[i]) == arr_mmReadyManiflod2[j].getNum())
                    {
                        arr_mmManifoldsToSwitchboard[k] = arr_mmReadyManiflod2[j];
                        k++;
                        break;
                    }
                }
            }
            return arr_mmManifoldsToSwitchboard;
        }

        void setUpSdCard()
        {
            Debug.Print("SDcard Start");
            if (Program._sd.IsCardInserted)
            {
                Program._sd.MountSDCard();


                System.Threading.Thread.Sleep(500);
                Definitions.ROOT_DIRECTORY = Program._sd.GetStorageDevice().RootDirectory;
                Definitions.PROGRAM_DIRECTORY = Definitions.ROOT_DIRECTORY + Definitions.PROGRAM_DIRECTORY;
                Definitions.IMAGES_DIRECTORY = Definitions.ROOT_DIRECTORY + Definitions.IMAGES_DIRECTORY;
                m_sConfigPath = Definitions.ROOT_DIRECTORY + "\\" + Definitions.PROGRAM_CONFIGURATION;
                Debug.Print("SD Mounted");
                MountCard();
            }
            else
            {
                GlideTouch.Initialize();
                ModalResult resultRun = Glide.MessageBoxManager.Show("No SD card found, can't continue", "ERROR", ModalButtons.Ok);
                    m_thread.Abort();
            }

        }
        static void MountCard()
        {
            GT.StorageDevice SDCards = Program._sd.GetStorageDevice();
            //Reads configuration file.
            using (Stream configStream = SDCards.OpenRead(FilePath))
            {
                //Loads settings.
                ConfigurationManager.Load(configStream);
            }

            //Retrieves values.
            var points = ConfigurationManager.GetAppSetting("Points");

            var calibrationPointCount = int.Parse(points);

            if (calibrationPointCount <= 0) return;

            var strsx = ConfigurationManager.GetAppSetting("Sx");
            var strsy = ConfigurationManager.GetAppSetting("Sy");
            var strcx = ConfigurationManager.GetAppSetting("Cx");
            var strcy = ConfigurationManager.GetAppSetting("Cy");

            // Create the calibration point array.
            var cx = strcx.ToShortArray();
            var cy = strcy.ToShortArray();
            var sx = strsx.ToShortArray();
            var sy = strsy.ToShortArray();

            Touch.ActiveTouchPanel.SetCalibration(calibrationPointCount, sx, sy, cx, cy);

            Debug.Print("Settings Loaded");

        }

    }
}
