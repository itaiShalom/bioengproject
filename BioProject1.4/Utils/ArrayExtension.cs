namespace BioProject1._4.Utils
{
    //Extension methods must be defined in a static class
    public static class ArrayExtension
    {
        // This is the extension method.
        // The first parameter takes the "this" modifier
        // and specifies the type for which the method is defined.
        /// <summary>
        /// Join short array with "|" and return string.
        /// </summary>
        /// <returns>string</returns>
        public static string Join(this short[] arr)
        {
            var count = arr.Length;
            var str = "";
            for (var i = 0; i < count; i++)
            {
                str += arr[i];
                if (i < count - 1)
                    str += "|";
            }
            return str;
        }
    }
    public static class StringExtension
    {
        /// <summary>
        /// Split string with "|" and return short array.
        /// </summary>
        /// <returns>Short array</returns>
        public static short[] ToShortArray(this string str)
        {
            var strarr = str.Split('|');
            var count = strarr.Length;
            var arr = new short[count];
            for (var i = 0; i < count; i++)
            {
                var s = short.Parse(strarr[i]);
                arr[i] = s;
            }
            return arr;
        }
    }
}