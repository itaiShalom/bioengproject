/* Copyright 2012 Marco Minerva, marco.minerva@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Collections;
using System.IO;
using System.Xml;

namespace BioProject1._4.Utils
{
    public static class ConfigurationManager
    {
        private const string AppsettingsSection = "appSettings";
        private const string Add = "add";
        private const string Key = "key";
        private const string Value = "value";

        private static readonly Hashtable AppSettings;

        static ConfigurationManager()
        {
            AppSettings = new Hashtable();
        }

        public static string GetAppSetting(string key)
        {
            return GetAppSetting(key, null);
        }

        public static string GetAppSetting(string key, string defaultValue)
        {
            if (!AppSettings.Contains(key))
                return defaultValue;

            return (string)AppSettings[key];
        }

        public static void SetAppSetting(string key, string value)
        {
            AppSettings[key] = value;
        }

        public static void Load(Stream xmlStream)
        {
            AppSettings.Clear();
            using (var reader = XmlReader.Create(xmlStream))
            {
                while (reader.Read())
                {
                    switch (reader.Name)
                    {
                        case AppsettingsSection:
                            while (reader.Read())
                            {
                                if (reader.Name == AppsettingsSection)
                                    break;

                                if (reader.Name != Add) continue;
                                var key = reader.GetAttribute(Key);
                                var value = reader.GetAttribute(Value);

                                //Debug.Print(key + "=" + value);
                                AppSettings.Add(key, value);
                            }

                            break;
                    }
                }
            }
        }

        public static void Save(Stream xmlStream)
        {
            using (var writer = new StreamWriter(xmlStream))
            {
                writer.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                writer.WriteLine("<configuration>");
                writer.WriteLine("\t<appSettings>");

                foreach (DictionaryEntry item in AppSettings)
                {
                    var add = "\t\t<add key=\"" + item.Key + "\" value=\"" + item.Value + "\" />";
                    writer.WriteLine(add);
                }

                writer.WriteLine("\t</appSettings>");
                writer.WriteLine("</configuration>");
            }
        }
    }
}
