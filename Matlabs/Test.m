classdef Test
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        testRGB
        testRGB_Delta
        testPins
        delta;
        testImage
        testImageMat
        testRating
        combNum
        bits
    end
    
    methods
        function obj =createTest(arrOfPins,RGB,RGB_Delta,image)
            obj.testRGB = RGB;
            obj.testRGB_Delta = RGB_Delta;
            obj.testImage = image;
            obj.testPins = arrOfPins;
        end
        function obj= calcDelta (obj)
            obj.delta=0;
            for i =1:3
                obj.delta = obj.delta + obj.testRGB_Delta{i};
            end
        end
        function obj = createBitArray(obj)
         obj.bits= zeros(1,length(obj.testPins));
         for i = 1:length(obj.testPins)
            if(get(obj.testPins{i},'Value')==1)
                obj.bits(i) = 1;
            end
         end
        end
    end
end

