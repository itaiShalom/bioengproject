function varargout = GUI3(varargin)
% GUI3 MATLAB code for GUI3.fig
%      GUI3, by itself, creates a new GUI3 or raises the existing
%      singleton*.
%
%      H = GUI3 returns the handle to a new GUI3 or the handle to
%      the existing singleton*.
%
%      GUI3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI3.M with the given input arguments.
%
%      GUI3('Property','Value',...) creates a new GUI3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI3

% Last Modified by GUIDE v2.5 05-Sep-2015 15:10:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI3_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI3 is made visible.
function GUI3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI3 (see VARARGIN)
details = varargin{1};
handles.ip = details{1,1};
if(length(str2num(details{2,1}))>0)
    handles.port = str2num(details{2,1});
else
    handles.port = (details{2,1});
end
%{
handles.ip = varargin{1};
handles.port = varargin{2};
%}
% Choose default command line output for GUI3
handles.output = hObject;
handles.arr1 = {handles.Out1, handles.Out2,handles.Out3,handles.Out4,...
    handles.Out5,handles.Out6,handles.Out7,handles.Out8,handles.Out9,handles.Out10,...
    handles.Out11,handles.Out12};

handles.arr2 = {handles.Outa1, handles.Outa2,handles.Outa3,handles.Outa4,...
    handles.Outa5,handles.Outa6,handles.Outa7,handles.Outa8,handles.Outa9,handles.Outa10,...
    handles.Outa11,handles.Outa12};

fid = fopen('Resistors.txt');
r6 = fgets(fid);
set(handles.slider1,'Value', str2double(r6));
set(handles.R6Text,'String', r6);
r9 = fgets(fid);
set(handles.slider2,'Value', str2double(r9));
set(handles.R9Text,'String', r9);
fclose(fid);

handles.sliders = {handles.slider1,handles.slider2};

axes(handles.ax)
%axes(handles.ax)
imshow('Switch.png')
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Out1.
function Out1_Callback(hObject, eventdata, handles)
% hObject    handle to Out1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out1,'Value')==1)
    set(handles.Outa1,'Value',1);
    set(handles.Out1, 'BackgroundColor','green')
else
    set(handles.Out1, 'BackgroundColor','red')
         set(handles.Outa1,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out1


% --- Executes on button press in Out2.
function Out2_Callback(hObject, eventdata, handles)
% hObject    handle to Out2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out2,'Value')==1)
       set(handles.Outa2,'Value',1);
set(handles.Out2, 'BackgroundColor','green')
else
          set(handles.Outa2,'Value',0);
    set(handles.Out2, 'BackgroundColor','red')
end
% Hint: get(hObject,'Value') returns toggle state of Out2


% --- Executes on button press in Out3.
function Out3_Callback(hObject, eventdata, handles)
% hObject    handle to Out3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out3,'Value')==1)
   set(handles.Outa3,'Value',1)
    set(handles.Out3, 'BackgroundColor','green')
else
    set(handles.Out3, 'BackgroundColor','red')
       set(handles.Outa3,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out3


% --- Executes on button press in Out4.
function Out4_Callback(hObject, eventdata, handles)
% hObject    handle to Out4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out4,'Value')==1)
   set(handles.Outa4,'Value',1)
    set(handles.Out4, 'BackgroundColor','green')
else
    set(handles.Out4, 'BackgroundColor','red')
       set(handles.Outa4,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out4


% --- Executes on button press in Out5.
function Out5_Callback(hObject, eventdata, handles)
% hObject    handle to Out5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out5,'Value')==1)
   set(handles.Outa5,'Value',1);
    set(handles.Out5, 'BackgroundColor','green')
else
    set(handles.Out5, 'BackgroundColor','red')
       set(handles.Outa5,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out5


% --- Executes on button press in Out6.
function Out6_Callback(hObject, eventdata, handles)
% hObject    handle to Out6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out6,'Value')==1)
   set(handles.Outa6,'Value',1);
    set(handles.Out6, 'BackgroundColor','green')
else
    set(handles.Out6, 'BackgroundColor','red')
       set(handles.Outa6,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out6


% --- Executes on button press in Out7.
function Out7_Callback(hObject, eventdata, handles)
% hObject    handle to Out7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out7,'Value')==1)
   set(handles.Outa7,'Value',1);
    set(handles.Out7, 'BackgroundColor','green')
else
    set(handles.Out7, 'BackgroundColor','red')
       set(handles.Outa7,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out7


% --- Executes on button press in Out8.
function Out8_Callback(hObject, eventdata, handles)
% hObject    handle to Out8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out8,'Value')==1)
   set(handles.Outa8,'Value',1);
    set(handles.Out8, 'BackgroundColor','green')
else
    set(handles.Out8, 'BackgroundColor','red')
      set(handles.Outa8,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out8


% --- Executes on button press in Out9.
function Out9_Callback(hObject, eventdata, handles)
% hObject    handle to Out9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out9,'Value')==1)
   set(handles.Outa9,'Value',1);
    set(handles.Out9, 'BackgroundColor','green')
else
    set(handles.Out9, 'BackgroundColor','red')
     set(handles.Outa9,'Value',0);
end
% Hint: get(hObject,'Value') returns toggle state of Out9


% --- Executes on button press in Out10.
function Out10_Callback(hObject, eventdata, handles)
% hObject    handle to Out10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out10,'Value')==1)
   set(handles.Outa10,'Value',1);
    set(handles.Out10, 'BackgroundColor','green')
else
     set(handles.Outa10,'Value',0);
    set(handles.Out10, 'BackgroundColor','red')
end
% Hint: get(hObject,'Value') returns toggle state of Out10


% --- Executes on button press in Out11.
function Out11_Callback(hObject, eventdata, handles)
% hObject    handle to Out11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out11,'Value')==1)
   set(handles.Outa11,'Value',1);
    set(handles.Out11, 'BackgroundColor','green')
else
     set(handles.Outa11,'Value',0);
    set(handles.Out11, 'BackgroundColor','red')
end
% Hint: get(hObject,'Value') returns toggle state of Out11


% --- Executes on button press in Out12.
function Out12_Callback(hObject, eventdata, handles)
% hObject    handle to Out12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(handles.Out12,'Value')==1)
   set(handles.Outa12,'Value',1);
    set(handles.Out12, 'BackgroundColor','green')
else
       set(handles.Outa12,'Value',0);
    set(handles.Out12, 'BackgroundColor','red')
end
% Hint: get(hObject,'Value') returns toggle state of Out12


% --- Executes on button press in setupGUI.
function setupGUI_Callback(hObject, eventdata, handles)
% hObject    handle to setupGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all
changeGui(handles.ip, handles.port,'GUI2')
temp ={handles.ip; num2str(handles.port)};
GUI2(temp);

% --- Executes on button press in demo.
function demo_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUI3')
GUI4({handles.ip; num2str(handles.port)});
% hObject    handle to demo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in sendData.
function sendData_Callback(hObject, eventdata, handles)
man1 =createMan(1,handles.arr1);
man2 =createMan(2,handles.arr2);
sliders = cell(1,0);
for i=1:length(handles.sliders)
    sliders{1,length(sliders)+1} = strcat('0',int2str(ceil(get(handles.sliders{i}, 'Value'))));
end
%clientVsGad('192.168.1.19', 3000, man1,man2,sliders)
%clientVsGad(handles.ip, handles.port, man1,man2,sliders,true,'GUI3 Config')
fileID = fopen('Resistors.txt','w');
fprintf(fileID,'%d\r\n%d',(round(get(handles.slider1,'Value')) ) , round(get(handles.slider2,'Value')));
fclose(fileID);
clientVsGad(handles.ip, handles.port, man1,man2,sliders,true,'GUI3 Config')

% hObject    handle to sendData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function Outa6_Callback(hObject, eventdata, handles)
function Outa7_Callback(hObject, eventdata, handles)
function Outa8_Callback(hObject, eventdata, handles)
function Outa9_Callback(hObject, eventdata, handles)
function Outa10_Callback(hObject, eventdata, handles)
function Outa11_Callback(hObject, eventdata, handles)
function Outa1_Callback(hObject, eventdata, handles)
function Outa2_Callback(hObject, eventdata, handles)
function Outa3_Callback(hObject, eventdata, handles)
function Outa4_Callback(hObject, eventdata, handles)
function Outa5_Callback(hObject, eventdata, handles)
function Outa12_Callback(hObject, eventdata, handles)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.R6Text,'String',ceil(get(handles.slider1, 'Value')))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.R9Text,'String',ceil(get(handles.slider2, 'Value')))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in togglebutton25.
function togglebutton25_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton25
