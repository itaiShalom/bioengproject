function [ XmlReady ] = createXML2( scenarioType,numOfObject,Resistance,tun,RunTime,DelayTime,iterations,bSaveToFile)
docNode = com.mathworks.xml.XMLUtils.createDocument('Scenario')
docRootNode = docNode.getDocumentElement;
docRootNode.setAttribute('CodeId','1.0');
TestInstruction = docNode.createElement('TestInstructions');
TestInstruction.setAttribute('ScenarioType',scenarioType);
for i = 1:iterations
    Stage = docNode.createElement('Stage');
    Stage.setAttribute('Number',num2str(i-1));
    Stage.setAttribute(scenarioType,numOfObject);
%    resistorsReady = FunctionClass('createResistors',Resistance(i,:));
  %  Stage.setAttribute('Pressures',resistorsReady);
%    tunnelsReady = FunctionClass('createTunnels',Tunnels{1,i});
  %  Stage.setAttribute('Tunnels',tunnelsReady);
    Stage.setAttribute('RunTime',num2str(RunTime(i)));
    Stage.setAttribute('DelayTime',num2str(DelayTime(i)));
  TestInstruction.appendChild(Stage);
end
docRootNode.appendChild(TestInstruction);
stamp = clock;
    timeStamp ='';
    for i = 1:length(stamp)-1
        timeStamp = strcat(timeStamp,num2str(stamp(i)),'_');
    end
    timeStamp = timeStamp(1:length(timeStamp)-1);
       answer = inputdlg('Choose Name','Save To File',1,{strcat(timeStamp)});
  folder_name = uigetdir(pwd,'Choose Directory');
    xmlFileName = [strcat(folder_name,'\',answer),'.xml'];
    xmlFileName = strcat(xmlFileName{1},xmlFileName{2});
xmlwrite(xmlFileName,docNode);
   type(xmlFileName);
end

   

