function [ colors ,image] = avarageRGB( image )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
counter = 0;
r=double(0);
g=double(0);
b=double(0);
for i= 110:130
    for j = 140:160
        counter = counter+1;
        r =r+double(image(i,j,1));
        g = g+double(image(i,j,2));
        b = b+double(image(i,j,3));
        image(i,j,1)=0;
        image(i,j,2)=0;
        image(i,j,3)=0;
    end
end
r = r/counter;
g= g/counter;
b = b/counter;
colors = {r;g;b};
end

