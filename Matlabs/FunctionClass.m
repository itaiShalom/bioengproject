function [ result ] = FunctionClass(functionName, input )
%FUNCTIONCLASS Summary of this function goes here
%   Detailed explanation goes here
switch functionName
    case 'createResistors'
        result = createResistors(input);
        return
    case 'createTunnels'
        result = createTunnels(input);
        return
    case 'setResistorsInitialValue'
       result = setResistorsInitialValue(input);
        return
        case 'parseXML'
            result = parseXML(input);
        return
       case 'getClock'
            result = getClock();
        return
end

function result=createResistors(arr)
    result ='';
for i=1:size(arr,2)
    result = strcat(result,num2str(ceil(arr(i))),':' );   
end
if (isempty(result)==0)
    result = result(1:length(result)-1);
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function result = getClock()
    stamp = clock;
     timeStamp ='';
     for i = 1:length(stamp)-1
        timeStamp = strcat(timeStamp,num2str(stamp(i)),'_');
      end
      result = timeStamp(1:length(timeStamp)-1);
 
 function result=createTunnels(arr)
    tempArr =[];
     result ='';
    for  i=1:length(arr)
        %  result = strcat(result,get(arr(i),'String'),',');
           tempArr = [tempArr str2double(get(arr(i),'String'))];
          %  result = strcat(result,int2str(i-1),',' );
     end 
     tempArr = sort(tempArr);
         for i=1:length(tempArr)
            result = strcat(result,num2str(tempArr(i)),',');
         end
if (isempty(result)==0)
    result = result(1:length(result)-1);
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function result=setResistorsInitialValue(handles)
    fid = fopen('Resistors.txt');
    r6 = fgets(fid);
    set(handles.slider6,'Value', str2double(r6));
    set(handles.ValR6,'String', r6);
    r9 = fgets(fid);
    set(handles.slider9,'Value', str2double(r9));
    set(handles.ValR9,'String', r9);
    fclose(fid);
    result = handles;
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 function theStruct = parseXML(filename)
% PARSEXML Convert XML file to a MATLAB structure.
try
   tree = xmlread(filename);
catch
   error('Failed to read XML file %s.',filename);
end

% Recurse over child nodes. This could run into problems 
% with very deeply nested trees.
try
   theStruct = parseChildNodes(tree);
catch
   error('Unable to parse XML file %s.',filename);
end


% ----- Local function PARSECHILDNODES -----
function children = parseChildNodes(theNode)
% Recurse over node children.
children = [];
if theNode.hasChildNodes
   childNodes = theNode.getChildNodes;
   numChildNodes = childNodes.getLength;
   allocCell = cell(1, numChildNodes);

   children = struct(             ...
      'Name', allocCell, 'Attributes', allocCell,    ...
      'Data', allocCell, 'Children', allocCell);

    for count = 1:numChildNodes
        theChild = childNodes.item(count-1);
        children(count) = makeStructFromNode(theChild);
    end
end

% ----- Local function MAKESTRUCTFROMNODE -----
function nodeStruct = makeStructFromNode(theNode)
% Create structure of node info.

nodeStruct = struct(                        ...
   'Name', char(theNode.getNodeName),       ...
   'Attributes', parseAttributes(theNode),  ...
   'Data', '',                              ...
   'Children', parseChildNodes(theNode));

if any(strcmp(methods(theNode), 'getData'))
   nodeStruct.Data = char(theNode.getData); 
else
   nodeStruct.Data = '';
end

% ----- Local function PARSEATTRIBUTES -----
function attributes = parseAttributes(theNode)
% Create attributes structure.

attributes = [];
if theNode.hasAttributes
   theAttributes = theNode.getAttributes;
   numAttributes = theAttributes.getLength;
   allocCell = cell(1, numAttributes);
   attributes = struct('Name', allocCell, 'Value', ...
                       allocCell);

   for count = 1:numAttributes
      attrib = theAttributes.item(count-1);
      attributes(count).Name = char(attrib.getName);
      attributes(count).Value = char(attrib.getValue);
   end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%