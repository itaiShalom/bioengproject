function varargout = MenuManifold(varargin)
% MENUMANIFOLD MATLAB code for MenuManifold.fig
%      MENUMANIFOLD, by itself, creates a new MENUMANIFOLD or raises the existing
%      singleton*.
%
%      H = MENUMANIFOLD returns the handle to a new MENUMANIFOLD or the handle to
%      the existing singleton*.
%
%      MENUMANIFOLD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MENUMANIFOLD.M with the given input arguments.
%
%      MENUMANIFOLD('Property','Value',...) creates a new MENUMANIFOLD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MenuManifold_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MenuManifold_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MenuManifold

% Last Modified by GUIDE v2.5 19-Sep-2015 18:54:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MenuManifold_OpeningFcn, ...
                   'gui_OutputFcn',  @MenuManifold_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MenuManifold is made visible.
function MenuManifold_OpeningFcn(hObject, eventdata, handles, varargin)

% This function has no Tunnelput args, see TunnelputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MenuSwitchboard (see VARARGIN)

details = varargin{1};
handles.ip = details{1,1};
if(length(str2double(details{2,1}))>0)
    handles.port = str2double(details{2,1});
else
    handles.port = (details{2,1});
end


% Choose default command line output for MenuSwitchboard
handles.output = hObject;
handles.arrTunnelsMan0 = [];

handles.arrTunnelsMan1 = [];

handles.sliders = {handles.slider6,handles.slider9};
handles = FunctionClass('setResistorsInitialValue',handles);
handles.slidersVals = [ceil(get(handles.slider6,'Value')), ceil(get(handles.slider9,'Value'))];

handles = updateInitialTunnelsArray(handles);
handles.cellTunnels = cell(2,1);
% % Update handles structure
guidata(hObject, handles);
% UIWAIT makes MenuSwitchboard wait for user response (see UIRESUME)
% uiwait(handles.figure1);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Outputs from this function are returned to the command line.
function varargout = MenuManifold_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% Hint: get(hObject,'Value') returns toggle state of m0t6
function CheckTimesValidity(hObject, eventdata, handles)
% hObject    handle to textRunTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tempVal = str2double(get(hObject,'String'));
if (isnan(tempVal) ||  rem(tempVal,1)~=0 || tempVal<=0)
    set(hObject,'String','1')
else
    set(hObject,'String',tempVal)
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function UpdateResistors(hObject, eventdata, handles)
if (strcmp(get(hObject,'Tag'),'slider9')==1)
    set(handles.ValR9,'String',ceil(get(handles.slider9, 'Value')))
     handles.slidersVals(2) = ceil(get(handles.slider9, 'Value'));
else
    set(handles.ValR6,'String',ceil(get(handles.slider6, 'Value')))
    handles.slidersVals(1) = ceil(get(handles.slider6, 'Value'));
end
guidata(hObject, handles);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function ChangeColor(hObject, eventdata, handles)
    iBelongToMan = 2;
    allTunnelsButtons1  =  allchild(handles.panelMan1);
    for i=2:length(allTunnelsButtons1)
            if(hObject == allTunnelsButtons1(i))
                iBelongToMan=1;
            end
    end   
 if (get(hObject,'Value')==1)
    set(hObject, 'BackgroundColor','green')
    if(iBelongToMan==1)
        handles.arrTunnelsMan0 = [handles.arrTunnelsMan0 hObject];
    else
        handles.arrTunnelsMan1 = [handles.arrTunnelsMan1 hObject];
    end
else
    set(hObject, 'BackgroundColor','red')
    if(iBelongToMan==1)
        handles.arrTunnelsMan0 = handles.arrTunnelsMan0(handles.arrTunnelsMan0~=hObject);
    else
        handles.arrTunnelsMan1 = handles.arrTunnelsMan1(handles.arrTunnelsMan1~=hObject);
    end
 end
guidata(hObject, handles);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%{
function result=createResistors(arr)
    result ='';
for i=1:length(arr)
    result = strcat(result,num2str(ceil(get(arr{i},'Value'))),':' );   
end
if (isempty(result)==0)
    result = result(1:length(result)-1);
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 function result=createTunnels(arr)
    result ='';
for i=1:length(arr)
        if (get(arr{i},'Value') ==1)
            result = strcat(result,int2str(i-1),',' );
        end
end
if (isempty(result)==0)
    result = result(1:length(result)-1);
end
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %}
 % --- Executes on button press in isUnlimited.
function isUnlimited_Callback(hObject, eventdata, handles)
% hObject    handle to isUnlimited (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if( get(hObject,'Value') ==1)
     set(handles.DelayTime,'Enable','off') 
     set(handles.DelayTime,'String','-1') 
else
    set(handles.DelayTime,'Enable','on') 
    set(handles.DelayTime,'String','1') 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
function handles = updateInitialTunnelsArray(handles)
    allTunnelsButtons1  =  allchild(handles.panelMan1);
    for i=2:length(allTunnelsButtons1)
        if(get(allTunnelsButtons1(i),'Value')==1)
            handles.arrTunnelsMan0 = [handles.arrTunnelsMan0 allTunnelsButtons1(i)];
        end
    end
    allTunnelsButtons2  =  allchild(handles.panelMan2);
    for i=2:length(allTunnelsButtons2)
        if(get(allTunnelsButtons2(i),'Value')==1)
            handles.arrTunnelsMan1 = [handles.arrTunnelsMan1 allTunnelsButtons2(i)];
        end
    end
 
 % --- Executes on button press in SendData.
function SendData_Callback(hObject, eventdata, handles)
% hObject    handle to SendData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Tunnels = strcat(FunctionClass('createTunnels',handles.arrTunnelsMan0),':',FunctionClass('createTunnels',handles.arrTunnelsMan1));
cellTunnels{1,1} = handles.arrTunnelsMan0;
cellTunnels{2,1} = handles.arrTunnelsMan1;
%Resistors = FunctionClass('createResistors',handles.sliders);
XmlReady = createXML( 'Manifold','0:1',handles.slidersVals,cellTunnels,...
    get(handles.RunTime,'String'),get(handles.DelayTime,'String'),get(handles.askForImage,'value'),1,false);
XmlReady = XmlReady(40:length(XmlReady));
sendScenario(handles.ip, handles.port, XmlReady);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  


% --- Executes on button press in goToSwitchBoard.
function goToSwitchBoard_Callback(hObject, eventdata, handles)
% hObject    handle to goToSwitchBoard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all
changeGui(handles.ip, handles.port,'GUITestingSwitchboards')
MenuSwitchboard({handles.ip; num2str(handles.port)});


% --- Executes on button press in goToMainMenu.
function goToMainMenu_Callback(hObject, eventdata, handles)
% hObject    handle to goToMainMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all
changeGui(handles.ip, handles.port,'GUIMainManu')
GUI1({handles.ip; num2str(handles.port)});
