function [ message,byteMessage ] = readMessage( input_stream )
%READMESSAGE Summary of this function goes here
%   Detailed explanation goes here

import java.net.Socket
import java.io.*
d_input_stream = DataInputStream(input_stream);
bytes_available = input_stream.available;
while (bytes_available==0)
    bytes_available = input_stream.available;
    pause(0.1)
end
fprintf(1, 'Reading %d bytes: ',bytes_available);
message = zeros(1, bytes_available, 'uint16');
for i = 1:bytes_available
    message(i) = d_input_stream.readByte;
end
byteMessage = message;
message = char(message);
fprintf('%s\n ',message);
end

