% CLIENT connect to a server and read a message
%
% Usage - message = client(host, port, number_of_retries)
function [] = client(host, port, number_of_retries)

import java.net.Socket
import java.io.*


number_of_retries = -1; % set to -1 for infinite


retry        = 0;
mySocket = [];
message      = [];

while true
    
    retry = retry + 1;
    if ((number_of_retries > 0) && (retry > number_of_retries))
        fprintf(1, 'Too many retries\n');
        break;
    end
    
    try
        %{
            fprintf(1, 'Retry %d connecting to %s:%d\n', ...
                    retry, host, port);
        %}
        % throws if unable to connect
        mySocket = Socket(host, port);
        
        % get a buffered data input stream from the socket
        input_stream   = mySocket.getInputStream;
        d_input_stream = DataInputStream(input_stream);
        output_stream   = mySocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);
        
        
        
        
        fprintf(1, 'Connected to server\n');
        
        % read data from the socket - wait a short time first
        message = readMessage( input_stream );
        
        %%%%%%%%%%%%%%%%
        
        
        
        
        
        sendMessage( d_output_stream,strcat(message,message) )
        message = readMessage( input_stream );
        % cleanup
        mySocket.close;
        break;
        
    catch
        if ~isempty(mySocket)
            mySocket.close;
        end
        
        % pause before retrying
        pause(1);
    end
end
end