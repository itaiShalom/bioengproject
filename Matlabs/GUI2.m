function varargout = GUI2(varargin)
% GUI2 MATLAB code for GUI2.fig
%      GUI2, by itself, creates a new GUI2 or raises the existing
%      singleton*.
%
%      H = GUI2 returns the handle to a new GUI2 or the handle to
%      the existing singleton*.
%
%      GUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI2.M with the given input arguments.
%
%      GUI2('Property','Value',...) creates a new GUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI2

% Last Modified by GUIDE v2.5 02-Jun-2014 15:58:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI2_OpeningFcn, ...
    'gui_OutputFcn',  @GUI2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI2 is made visible.
function GUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI2 (see VARARGIN)

% Choose default command line output for GUI2
handles.output = hObject;
details = varargin{1};
handles.ip = details{1,1};
if(length(str2num(details{2,1}))>0)
    handles.port = str2num(details{2,1});
else
    handles.port = (details{2,1});
end

% Update handles structure
handles.arr1 = {handles.Out1, handles.Out2,handles.Out3,handles.Out4,...
    handles.Out5,handles.Out6,handles.Out7,handles.Out8,handles.Out9,handles.Out10,...
    handles.Out11,handles.Out12};
handles.arr2 = {handles.Outa1, handles.Outa2,handles.Outa3,handles.Outa4,...
    handles.Outa5,handles.Outa6,handles.Outa7,handles.Outa8,handles.Outa9,handles.Outa10,...
    handles.Outa11,handles.Outa12};
fid = fopen('Resistors.txt');
r6 = fgets(fid);
set(handles.slider1,'Value', str2double(r6));
set(handles.R6Text,'String', r6);
r9 = fgets(fid);
set(handles.slider2,'Value', str2double(r9));
set(handles.R9Text,'String', r9);
fclose(fid);

handles.sliders = {handles.slider1,handles.slider2};
guidata(hObject, handles);

% UIWAIT makes GUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.R6Text,'String',ceil(get(handles.slider1, 'Value')))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%{
% --- Executes on button press in M2M1.
function M2M1_Callback(hObject, eventdata, handles)
% hObject    handle to M2M1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of M2M1


% --- Executes on button press in M2M2.
function M2M2_Callback(hObject, eventdata, handles)
% hObject    handle to M2M2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of M2M2
%}

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.R9Text,'String',ceil(get(handles.slider2, 'Value')))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in togglebutton29.
function togglebutton29_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton29


% --- Executes on button press in Out1.
function Out1_Callback(hObject, eventdata, handles)
checkAndUpdate(1,handles,1)
% hObject    handle to Out1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Out1


% --- Executes on button press in Out2.
function Out2_Callback(hObject, eventdata, handles)
checkAndUpdate(1,handles,2)
% hObject    handle to Out2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Out2


% --- Executes on button press in Out3.
function Out3_Callback(hObject, eventdata, handles)
% hObject    handle to Out3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,3)
% Hint: get(hObject,'Value') returns toggle state of Out3


% --- Executes on button press in Out4.
function Out4_Callback(hObject, eventdata, handles)
% hObject    handle to Out4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,4)
% Hint: get(hObject,'Value') returns toggle state of Out4


% --- Executes on button press in Out5.
function Out5_Callback(hObject, eventdata, handles)
% hObject    handle to Out5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,5)
% Hint: get(hObject,'Value') returns toggle state of Out5


% --- Executes on button press in Out6.
function Out6_Callback(hObject, eventdata, handles)
% hObject    handle to Out6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,6)
% Hint: get(hObject,'Value') returns toggle state of Out6


% --- Executes on button press in Out7.
function Out7_Callback(hObject, eventdata, handles)
% hObject    handle to Out7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,7)
% Hint: get(hObject,'Value') returns toggle state of Out7


% --- Executes on button press in Out8.
function Out8_Callback(hObject, eventdata, handles)
% hObject    handle to Out8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,8)
% Hint: get(hObject,'Value') returns toggle state of Out8




% --- Executes on button press in Out9.
function Out9_Callback(hObject, eventdata, handles)
% hObject    handle to Out9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,9)
% Hint: get(hObject,'Value') returns toggle state of Out9


% --- Executes on button press in Out10.
function Out10_Callback(hObject, eventdata, handles)
% hObject    handle to Out10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,10)
% Hint: get(hObject,'Value') returns toggle state of Out10


% --- Executes on button press in Out11.
function Out11_Callback(hObject, eventdata, handles)
% hObject    handle to Out11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,11)
% Hint: get(hObject,'Value') returns toggle state of Out11


% --- Executes on button press in Out12.
function Out12_Callback(hObject, eventdata, handles)
% hObject    handle to Out12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(1,handles,12)
% Hint: get(hObject,'Value') returns toggle state of Out12


% --- Executes when selected object is changed in ModesBar1.
function ModesBar1_SelectionChangeFcn(hObject, eventdata, handles)
disp(get(handles.M1M1,'Value'))
disp(get(handles.M1M2,'Value'))
% hObject    handle to the selected object in ModesBar1
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


function Outa1_Callback(hObject, eventdata, handles)
checkAndUpdate(2,handles,1)
% hObject    handle to Outa1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Outa1


% --- Executes on button press in Outa2.
function Outa2_Callback(hObject, eventdata, handles)
checkAndUpdate(2,handles,2)
% hObject    handle to Outa2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Outa2


% --- Executes on button press in Outa3.
function Outa3_Callback(hObject, eventdata, handles)
% hObject    handle to Outa3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,3)
% Hint: get(hObject,'Value') returns toggle state of Outa3


% --- Executes on button press in Outa4.
function Outa4_Callback(hObject, eventdata, handles)
% hObject    handle to Outa4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
checkAndUpdate(2,handles,4)
% Hint: get(hObject,'Value') returns toggle state of Outa4


% --- Executes on button press in Outa5.
function Outa5_Callback(hObject, eventdata, handles)
% hObject    handle to Outa5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,5)
% Hint: get(hObject,'Value') returns toggle state of Outa5


% --- Executes on button press in Outa6.
function Outa6_Callback(hObject, eventdata, handles)
% hObject    handle to Outa6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,6)
% Hint: get(hObject,'Value') returns toggle state of Outa6


% --- Executes on button press in Outa7.
function Outa7_Callback(hObject, eventdata, handles)
% hObject    handle to Outa7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,7)
% Hint: get(hObject,'Value') returns toggle state of Outa7


% --- Executes on button press in Outa8.
function Outa8_Callback(hObject, eventdata, handles)
% hObject    handle to Outa8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,8)
% Hint: get(hObject,'Value') returns toggle state of Outa8


% --- Executes on button press in Outa9.
function Outa9_Callback(hObject, eventdata, handles)
% hObject    handle to Outa9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,9)
% Hint: get(hObject,'Value') returns toggle state of Outa9


% --- Executes on button press in Outa10.
function Outa10_Callback(hObject, eventdata, handles)
% hObject    handle to Outa10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,10)
% Hint: get(hObject,'Value') returns toggle state of Outa10


% --- Executes on button press in Outa11.
function Outa11_Callback(hObject, eventdata, handles)
% hObject    handle to Outa11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,11)
% Hint: get(hObject,'Value') returns toggle state of Outa11


% --- Executes on button press in Outa12.
function Outa12_Callback(hObject, eventdata, handles)
% hObject    handle to Outa12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
checkAndUpdate(2,handles,12)
% Hint: get(hObject,'Value') returns toggle state of Outa12


% --- Executes on button press in SendData.
function SendData_Callback(hObject, eventdata, handles)
man1 =createMan(1,handles.arr1);
man2 =createMan(2,handles.arr2);
sliders = cell(1,0);
for i=1:length(handles.sliders)
    sliders{1,length(sliders)+1} = strcat('0',int2str(ceil(get(handles.sliders{i}, 'Value'))));
end
%clientVsGad('192.168.1.19', 3000, man1,man2,sliders)
fileID = fopen('Resistors.txt','w');
fprintf(fileID,'%d\r\n%d',(round(get(handles.slider1,'Value')) ) , round(get(handles.slider2,'Value')));
fclose(fileID);
clientVsGad(handles.ip, handles.port, man1,man2,sliders,true,'GUI2 Config')



% hObject    handle to SendData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in switchBoard.
function switchBoard_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUI3');
GUI3({handles.ip; num2str(handles.port)});

% hObject    handle to switchBoard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes on button press in demo.
function demo_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUI4');
GUI4({handles.ip; num2str(handles.port)});
% hObject    handle to demo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




function checkAndUpdate(manifoldNum,handles,index)
if (manifoldNum==1)
    arr = handles.arr1;
    m =handles.M1M1;
else
    arr = handles.arr2;
    m =handles.M2M1;
end

if (get(arr{index},'Value')==0)
    set(arr{index},'BackgroundColor','red');
else if ((get(m,'Value'))==1)
        
        for i=1:length(arr)
            if (i==index)
                set(arr{i},'BackgroundColor','green');
            else
                set(arr{i},'BackgroundColor','red');
                set(arr{i},'Value',0);
            end
        end
    else
        set(arr{index},'BackgroundColor','green');
    end
end
