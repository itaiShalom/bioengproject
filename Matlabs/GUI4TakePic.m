% CLIENT connect to a server and read a message
%
% Usage - message = client(host, port, number_of_retries)
function [picChar,picByte] = GUI4TakePic(host, port)

import java.net.Socket
import java.io.*


number_of_retries = -1; % set to -1 for infinite


retry        = 0;
mySocket = [];

while true
    
    retry = retry + 1;
    if ((number_of_retries > 0) && (retry > number_of_retries))
        fprintf(1, 'Too many retries\n');
        break;
    end
    
    try
        %{
            fprintf(1, 'Retry %d connecting to %s:%d\n', ...
                    retry, host, port);
        %}
        % throws if unable to connect
        disp('Attempt to connect')
        mySocket = Socket(host, port);
        address = java.net.InetAddress.getLocalHost ;
        IPaddress = char(address.getHostAddress);
        % get a buffered data input stream from the socket
        input_stream   = mySocket.getInputStream;
        d_input_stream = DataInputStream(input_stream);
        output_stream   = mySocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);
        
        
        
        
        fprintf(1, 'Connected to server\n');
        sendMessage( d_output_stream,strcat('Matlab from:', IPaddress) );
        % read data from the socket - wait a short time first
        
        message = readMessage( input_stream );
        if(strcmp(message,'Ready'))
            sendMessage( d_output_stream,'TakePicture');
            [picChar,picByte] =    readMessage(input_stream);
            
            sendMessage( d_output_stream,'Done');
             readMessage(input_stream);
            %}
            % cleanup
            
        end
        mySocket.close;
        disp('session ended')
        
        break;
        
    catch
        if ~isempty(mySocket)
            mySocket.close;
            disp('session ended')
        end
        disp('Falid to connect...')
        % pause before retrying
        return;
    end
end
end