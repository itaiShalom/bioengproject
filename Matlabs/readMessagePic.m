function [ message,byteMessage ,msg,msg2] = readMessagePic( input_stream ,messageSize)
%READMESSAGE Summary of this function goes here
%   Detailed explanation goes here

import java.net.Socket
import java.io.*
d_input_stream = DataInputStream(input_stream);
bytes_available = input_stream.available;
while (bytes_available==0)
    bytes_available = input_stream.available;
    pause(0.1)
end
message =zeros(1,messageSize);
msg2 =zeros(1,messageSize);
msg =' ';
j=1;
while (messageSize~=0)
    messageSize=messageSize-bytes_available;
    fprintf(1, 'Reading %d\n bytes: ',bytes_available);
    
    for i = 1:bytes_available
        z = d_input_stream.readByte;
       z1= z;
        if (z<0)
       z1 = z+256;
       end
        message(j) = char(z1);
      msg(j) = char(z1);
      msg2(j) = z;
        j=j+1;
    end
    d_input_stream = DataInputStream(input_stream);
    bytes_available = input_stream.available;
end
byteMessage = message;
message = char(message);
%fprintf('%s\n ',message);
end

