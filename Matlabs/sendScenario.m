% CLIENT connect to a server and read a message
%
% Usage - message = client(host, port, number_of_retries)
function [] = sendScenario(host, port,Scenario)

import java.net.Socket
import java.io.*


number_of_retries = -1; % set to -1 for infinite
host
port

retry        = 0;
mySocket = [];

while true
    
    retry = retry + 1;
    if ((number_of_retries > 0) && (retry > number_of_retries))
        fprintf(1, 'Too many retries\n');
        break;
    end
    
    try
        %{
            fprintf(1, 'Retry %d connecting to %s:%d\n', ...
                    retry, host, port);
        %}
        % throws if unable to connect
        disp('Attempt to connect')
        mySocket = Socket(host, port);
        address = java.net.InetAddress.getLocalHost ;
        IPaddress = char(address.getHostAddress);
        % get a buffered data input stream from the socket
        input_stream   = mySocket.getInputStream;
        d_input_stream = DataInputStream(input_stream);
        output_stream   = mySocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);       
        fprintf(1, 'Connected to server\n');
        sendMessage( d_output_stream,'CreateScenario');
        % read data from the socket - wait a short time first
        if(strcmp(readMessage( input_stream ),'OK'))
             sendMessage( d_output_stream,num2str(length(Scenario)));
             readMessage( input_stream );
            sendMessage( d_output_stream,Scenario);
           
           	while (~strcmp(readMessage( input_stream ),'OK'))
                sendMessage( d_output_stream,Scenario);
           %     message = readMessage( input_stream );
            end
             %Sending Image
            nextStep =  readMessage( input_stream );
            if (strcmp(nextStep,'SENDING_IMAGE')==true)
                 
             
                while ( strcmp( nextStep, 'DONE_SENDING_IMAGE')==false)
                    sendMessage( d_output_stream,'OK');
                    
                    [ ~,~,data,~,stageNum ] = getImage( d_output_stream , input_stream );
                    name = strcat('Images\',FunctionClass('getClock',''),'_','StageNumber-',num2str(stageNum),'.bmp');
                    fid = fopen(name, 'w');
                    fprintf(fid, '%s\r\n', data);
                    fclose(fid);              
                    h= figure(str2num(stageNum)+1);
                    imshow(name);

                    nextStep = readMessage( input_stream );
                end
            elseif (strcmp(nextStep,'OK'))
                msgbox('Scenario is done','Notification')
            end
        end
        %sendMessage( d_output_stream,'OK')
        mySocket.close;
        disp('session ended')
        break;
        
    catch
        if ~isempty(mySocket)
            mySocket.close;
            disp('session ended')
        end
        disp('Falid to connect...')
        % pause before retrying
        return;
    end
end
end

function [ a,b,c,d,stageNum ] = getImage( d_output_stream , input_stream )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

   %         sendMessage( d_output_stream,'TakePicture');
            stageNum =    readMessage(input_stream);
            sendMessage( d_output_stream,'OK');

            size =    readMessage(input_stream);
            sendMessage( d_output_stream,'OK');
            [a,b,c,d] = readMessagePic(input_stream,str2double(size));
            sendMessage( d_output_stream,'OK');
           % readMessage(input_stream);
end
