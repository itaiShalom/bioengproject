function [ image_output ] = imgSat( image_input )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
HSV = rgb2hsv(image_input);
% "20% more" saturation:
HSV(:, :, 2) = HSV(:, :, 2) * 1.2;
% or add:
% HSV(:, :, 2) = HSV(:, :, 2) + 0.2;
HSV(HSV > 1) = 1;  % Limit values
image_output = hsv2rgb(HSV);
end

