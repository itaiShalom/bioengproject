function varargTunnel = MenuSwitchboard(varargin)
% MENUSWITCHBOARD MATLAB code for MenuSwitchboard.fig
%      MENUSWITCHBOARD, by itself, creates a new MENUSWITCHBOARD or raises the existing
%      singleton*.
%
%      H = MENUSWITCHBOARD returns the handle to a new MENUSWITCHBOARD or the handle to
%      the existing singleton*.
%
%      MENUSWITCHBOARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MENUSWITCHBOARD.M with the given input arguments.
%
%      MENUSWITCHBOARD('Property','Value',...) creates a new MENUSWITCHBOARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MenuSwitchboard_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MenuSwitchboard_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MenuSwitchboard

% Last Modified by GUIDE v2.5 19-Sep-2015 19:03:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MenuSwitchboard_OpeningFcn, ...
                   'gui_OutputFcn',  @MenuSwitchboard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MenuSwitchboard is made visible.
function MenuSwitchboard_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no Tunnelput args, see TunnelputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MenuSwitchboard (see VARARGIN)


%%%%%% UNCOMMENT

details = varargin{1};
handles.ip = details{1,1};
if(length(str2num(details{2,1}))>0)
    handles.port = str2num(details{2,1});
else
    handles.port = (details{2,1});
end

% Choose default command line Tunnelput for MenuSwitchboard
handles.output = hObject;

%handles.arr1 = {handles.Tunnel0, handles.Tunnel1, handles.Tunnel2,handles.Tunnel3,handles.Tunnel4,...
  %  handles.Tunnel5,handles.Tunnel6,handles.Tunnel7,handles.Tunnel8,handles.Tunnel9,handles.Tunnel10,...
    %handles.Tunnel11};
handles.sliders = {handles.slider6,handles.slider9};
handles.bIsModified = false;
handles = FunctionClass('setResistorsInitialValue',handles);
set(handles.back, 'visible','off');
set(handles.next, 'visible','off');
set(handles.textStageNum, 'visible','off');
set(handles.HLStageNum, 'visible','off');
set(handles.CreateProgramPanel,'visible','off');
handles.programIndex = -1;
handles.bIsSaved = false;
axes(handles.ax)

imshow('Switch.png')

handles.arrImage = [];
handles.slidersVals = [ceil(get(handles.slider6,'Value')), ceil(get(handles.slider9,'Value'))];
handles.arrTunnel = [];
handles.cellProgramTunnels = {};
handles.arrProgramDelayTimes = [];
handles.arrProgramRunTimes = [];
handles.matProgramResistors = [];
handles.figure = 0;
% % Update handles structure
guidata(hObject, handles);
% UIWAIT makes MenuSwitchboard wait for user response (see UIRESUME)
% uiwait(handles.figure1);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Tunnelputs from this function are returned to the command line.
function varargout = MenuSwitchboard_OutputFcn(hObject, eventdata, handles) 
% varargTunnel  cell array for returning Tunnelput args (see VARARGTunnel);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.figure = hObject;
% Get default command line output from handles structure
varargout{1} = handles.output;


function UpdateResistors(hObject, eventdata, handles)
if (strcmp(get(hObject,'Tag'),'slider9')==1)
    set(handles.ValR9,'String',ceil(get(handles.slider9, 'Value')))
    handles.slidersVals(2) = ceil(get(handles.slider9, 'Value'));
else
    set(handles.ValR6,'String',ceil(get(handles.slider6, 'Value')))
    handles.slidersVals(1) = ceil(get(handles.slider6, 'Value'));
end
handles.bIsSaved = false;
handles.bIsModified = true;
guidata(hObject, handles);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
 
function ChangeColor(hObject, eventdata, handles)
if (get(hObject,'Value')==1)
    set(hObject, 'BackgroundColor','green')
   handles.arrTunnel = [handles.arrTunnel hObject];
else
    set(hObject, 'BackgroundColor','red')
         handles.arrTunnel =  handles.arrTunnel( handles.arrTunnel~=hObject);
end
handles.bIsSaved = false;
handles.bIsModified = true;
guidata(hObject, handles);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 function result = saveUpdated(hObject,handles,index)
    disp( handles.cellProgramTunnels)
     disp( handles.arrTunnel)
     handles.bIsModified= false;
     handles.arrImage(index) = get(handles.askForImage,'Value');
    handles.arrProgramRunTimes(index) = str2double(get(handles.RunTime,'String'));
    handles.arrProgramDelayTimes(index) = str2double(get(handles.DelayTime,'String'));
    handles.matProgramResistors(index,:) = handles.slidersVals;
    handles.cellProgramTunnels{1,index} = handles.arrTunnel;
       disp( handles.cellProgramTunnels)
     disp( handles.arrTunnel)
      result = handles;
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
 
% --- Executes on button press in SendData.
function SendSaveData_Callback(hObject, eventdata, handles)
% hObject    handle to SendData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
bSaveToFile = false;
if (strcmp(get(hObject,'Tag'), 'saveScenario')==true)
    bSaveToFile = true;
    handles.bIsSaved = true;
end
XmlReady='';
if(handles.programIndex == -1|| get(handles.CreateProgram,'Value')==0 )
    tempTunnels ={handles.arrTunnel};
    disp(get(handles.RunTime,'String'));
    XmlReady = createXML( 'Switchboard','0',handles.slidersVals,tempTunnels,...
    get(handles.RunTime,'String'),get(handles.DelayTime,'String'),get(handles.askForImage,'value'),1,bSaveToFile);
else
    XmlReady = createXML( 'Switchboard','0', handles.matProgramResistors,handles.cellProgramTunnels,...
    handles.arrProgramRunTimes,handles.arrProgramDelayTimes,handles.arrImage,length(handles.arrProgramDelayTimes),bSaveToFile);
end
if (bSaveToFile~=true)
    XmlReady = XmlReady(40:length(XmlReady));
    sendScenario(handles.ip, handles.port, XmlReady);
end
handles.bIsModified = false;
guidata(hObject, handles);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function CheckTimesValidity(hObject, eventdata, handles)
% hObject    handle to textRunTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tempVal = str2double(get(hObject,'String'));
if (isnan(tempVal) ||  rem(tempVal,1)~=0 || tempVal<=0)
    set(hObject,'String','1')
else
    set(hObject,'String',tempVal)
end
handles.bIsSaved = false;
handles.bIsModified = true;
guidata(hObject, handles);
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function slider9_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in isUnlimited.
function isUnlimited_Callback(hObject, eventdata, handles)
if( get(hObject,'Value') ==1)
     set(handles.DelayTime,'Enable','off') 
     set(handles.DelayTime,'String','-1') 
     set(handles.next,'Enable','off') 
else
    set(handles.DelayTime,'Enable','on') 
    set(handles.DelayTime,'String','1') 
         set(handles.next,'Enable','on') 
end


% --- Executes on button press in back.
function back_Callback(hObject, eventdata, handles)
if (handles.programIndex>1)
    if (handles.bIsModified==true)
        handles = saveUpdated(hObject,handles,handles.programIndex);
    end
    handles.programIndex = handles.programIndex-1;
end
if (handles.programIndex==1)
    set(handles.back ,'Enable','off') 
end    
set(handles.textStageNum,'String',num2str(handles.programIndex))
handles.arrTunnel =[];
handles = loadInfo(handles);
handles.bIsModified = false;
guidata(hObject, handles);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = loadInfo(handles)
allTunnelsButtons  =  allchild(handles.panelManifold);
    for i=2:length(allTunnelsButtons)
            set(allTunnelsButtons(i), 'BackgroundColor', 'red');
            set(allTunnelsButtons(i), 'Value', 0);
        
    end
set(handles.slider6,'Value', handles.matProgramResistors(handles.programIndex,1)  ); 
set(handles.slider9,'Value', handles.matProgramResistors(handles.programIndex,2)  ); 
set(handles.ValR9,'String',ceil(get(handles.slider9, 'Value')))
set(handles.ValR6,'String',ceil(get(handles.slider6, 'Value')))

set(handles.RunTime,'String',num2str(handles.arrProgramRunTimes(handles.programIndex)) );
set(handles.DelayTime,'String',num2str(handles.arrProgramDelayTimes(handles.programIndex)) );
set(handles.askForImage,'Value',(handles.arrImage(handles.programIndex)) );
handles.arrTunnel =[];
arr = handles.cellProgramTunnels{1,handles.programIndex};
for i=1:length(arr)
    set(arr(i), 'BackgroundColor','green');
    set(arr(i), 'Value',1);
    handles.arrTunnel = [handles.arrTunnel arr(i)];
end
result = handles;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in next.
function next_Callback(hObject, eventdata, handles)
handles.programIndex = handles.programIndex+1;
set(handles.back ,'Enable','on') 
set(handles.textStageNum,'String',num2str(handles.programIndex))

toRefresh = true;
if (length(handles.arrProgramDelayTimes)< handles.programIndex)
    handles.cellProgramTunnels{1,size(handles.cellProgramTunnels,2)+1} =handles.arrTunnel;
    handles.arrProgramDelayTimes = [ handles.arrProgramDelayTimes , str2double(get(handles.DelayTime,'String')) ];
    handles.arrProgramRunTimes = [handles.arrProgramRunTimes , str2double(get(handles.RunTime,'String'))];
    handles.matProgramResistors = [handles.matProgramResistors ; handles.slidersVals];
    handles.arrImage =  [ handles.arrImage , (get(handles.askForImage,'Value')) ];
elseif (handles.bIsModified==true)
    handles = saveUpdated(hObject,handles,handles.programIndex-1);
   handles = loadInfo(handles);
    toRefresh = false;
else
    handles = loadInfo(handles);
    toRefresh = false;
end
if (toRefresh ==true)
    handles.arrTunnel =[];
  allTunnelsButtons  =  allchild(handles.panelManifold);
    for i=2:length(allTunnelsButtons)
            set(allTunnelsButtons(i), 'BackgroundColor', 'red');
            set(allTunnelsButtons(i), 'Value', 0); 
    end
end
handles.bIsModified = false;
guidata(hObject, handles);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in askForImage.
function askForImage_Callback(hObject, eventdata, handles)
% hObject    handle to askForImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%CreateProgram 
if (get(handles.CreateProgram,'Value')==1)
    if (get(hObject,'Value')==1)
        handles.arrImage(handles.programIndex) = 1;
    else
         handles.arrImage(handles.programIndex) = 0;
    end
end
disp(handles.arrImage)
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of askForImage


% --- Executes on button press in CreateProgram.
function CreateProgram_Callback(hObject, eventdata, handles)
% hObject    handle to CreateProgram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.bIsModified = false;
 allPanelButtons  =  allchild(handles.CreateProgramPanel);
if (get(hObject,'Value') ==1)
    set(handles.CreateProgramPanel,'visible','on');
    for i=1:length(allPanelButtons)
        set(allPanelButtons(i), 'visible', 'on');
    end
       set(handles.back ,'Enable','off') 
    handles.programIndex = 1;
else
   
     set(handles.CreateProgramPanel,'visible','off');
    for i=1:length(allPanelButtons)
        set(allPanelButtons(i), 'visible', 'off');
    end
end
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of CreateProgram


% --- Executes on button press in loadScenario.
function loadScenario_Callback(hObject, eventdata, handles)
% hObject    handle to loadScenario (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.bIsModified==true || handles.bIsSaved == false)
promptMessage = sprintf('Current scenario will be lost, continue?');
button = questdlg(promptMessage, 'Continue', 'Continue', 'Cancel', 'Cancel');
if strcmp(button, 'Cancel')
	return; 
end
end
handles.programIndex = 1;
[filename, pathname] = uigetfile('*.xml', 'Select a MATLAB code file');
if isequal(filename,0)
   disp('User selected Cancel')
else
   disp(['User selected ', fullfile(pathname, filename)])
end
theStruct = FunctionClass('parseXML',fullfile(pathname, filename));
handles = parsedXmlToHandles(handles,theStruct);
handles = loadInfo(handles);
set(handles.textStageNum,'String',  num2str(handles.programIndex));
set(handles.back ,'Enable','off') 
guidata(hObject, handles);

function result = parsedXmlToHandles(handles,theStruct)
    tunnelsIndex = 1;
    handles.arrTunnel = [];
    handles.cellProgramTunnels = {};
    handles.arrProgramDelayTimes = [];
    handles.arrProgramRunTimes = [];
    handles.matProgramResistors = [];
    handles.arrImage = [];
        theStruct.Children(2).Children(2)
        for i = 2:2:length(theStruct.Children(2).Children())
          cellStage =  struct2cell(theStruct.Children(2).Children(i).Attributes);
          for j=1:size(cellStage,3)
            att = cellStage(:,:,j);
            if (strcmp(att{1,1},'DelayTime')==true)
                handles.arrProgramDelayTimes = [handles.arrProgramDelayTimes str2double(att{2,1})]; 
                continue;
            end
            
            if (strcmp(att{1,1},'RunTime')==true)
                handles.arrProgramRunTimes = [handles.arrProgramRunTimes str2double(att{2,1})]; 
                continue;
            end      
            
             if (strcmp(att{1,1},'Image')==true)
                handles.arrImage = [handles.arrImage str2double(att{2,1})]; 
                continue;
            end      
            if (strcmp(att{1,1},'Pressures')==true)
                tempStr = att{2,1};
                resultResistor =[];
                C = strsplit(tempStr,':');
                for k=1:length(C)
                    resultResistor = [resultResistor str2double(C(k))];
                end
                handles.matProgramResistors = [handles.matProgramResistors ; resultResistor];
            continue;
            end
            
            if (strcmp(att{1,1},'Tunnels')==true)
                tempStr = att{2,1};
                resultTunnels =[];
                C = strsplit(tempStr,',');
                for k=1:length(C)
                    resultTunnels = [resultTunnels str2double(C(k))];
                end
                ready = [];
                for z =1:length(resultTunnels)
                    ready(z) = (findobj('style','togglebutton','String',num2str(resultTunnels(z))));
                end
                handles.cellProgramTunnels{1,tunnelsIndex} = ready;
                tunnelsIndex = tunnelsIndex+1;
            continue;
            end
          end
        end
        disp( handles.cellProgramTunnels)
        result = handles;


% --- Executes on button press in goToMain.
function goToMain_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUIMainManu')
GUI1({handles.ip; num2str(handles.port)});


% --- Executes on button press in goToManifoldMenu.
function goToManifoldMenu_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUITestingManifolds')
MenuManifold({handles.ip; num2str(handles.port)});
