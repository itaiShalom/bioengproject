function [  XmlReady] = createXML( scenarioType,numOfObject,Resistance,Tunnels,RunTime,DelayTime,Image,iterations,bSaveToFile)
docNode = com.mathworks.xml.XMLUtils.createDocument('Scenario');
docRootNode = docNode.getDocumentElement;
docRootNode.setAttribute('CodeId','1.0');
TestInstruction = docNode.createElement('TestInstructions');
TestInstruction.setAttribute('ScenarioType',scenarioType);
for i=1:iterations
    Stage = docNode.createElement('Stage');
    Stage.setAttribute('Number',num2str(i-1));
    Stage.setAttribute(scenarioType,numOfObject);
   
    resistorsReady = FunctionClass('createResistors',Resistance(i,:));
    Stage.setAttribute('Pressures',resistorsReady);
    tunnelsReady='';
    for k =1:size(Tunnels,1)
        tunnelsReady = strcat(tunnelsReady,FunctionClass('createTunnels',Tunnels{k,i}),':');
    end
    tunnelsReady = tunnelsReady(1:length(tunnelsReady)-1);
    
    
    Stage.setAttribute('Tunnels',tunnelsReady);  
%    tunnelsForMatlab = FunctionClass('createTunnelsForMatlab',Tunnels{1,i});
  %  Stage.setAttribute('Matlab',tunnelsForMatlab);  
  if(iterations==1)
             Stage.setAttribute('DelayTime',num2str(DelayTime));   
  Stage.setAttribute('RunTime',num2str(RunTime)); 
  else
       Stage.setAttribute('DelayTime',num2str(DelayTime(i)));   
  Stage.setAttribute('RunTime',num2str(RunTime(i)));    
  end

Stage.setAttribute('Image',num2str(Image(i)));
    Stage.setAttribute('DelayTime',num2str(DelayTime(i)));
    TestInstruction.appendChild(Stage);
end
docRootNode.appendChild(TestInstruction);
if (bSaveToFile==true)
    stamp = clock;
    timeStamp ='';
    for i = 1:length(stamp)-1
        timeStamp = strcat(timeStamp,num2str(stamp(i)),'_');
    end
    timeStamp = timeStamp(1:length(timeStamp)-1);
       answer = inputdlg('Choose Name','Save To File',1,{strcat(scenarioType,'_',timeStamp)});
  folder_name = uigetdir(pwd,'Choose Directory');
    xmlFileName = [strcat(folder_name,'\',answer),'.xml'];
    xmlFileName = strcat(xmlFileName{1},xmlFileName{2});
xmlwrite(xmlFileName,docNode);
 type(xmlFileName);
msgbox('Operation Completed');
 XmlReady ='';
else
    XmlReady = xmlwrite(docNode);
end
end