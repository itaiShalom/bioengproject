function varargout = GUI1(varargin)
% GUI1 MATLAB code for GUI1.fig
%      GUI1, by itself, creates a new GUI1 or raises the existing
%      singleton*.
%
%      H = GUI1 returns the handle to a new GUI1 or the handle to
%      the existing singleton*.
%
%      GUI1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI1.M with the given input arguments.
%
%      GUI1('Property','Value',...) creates a new GUI1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI1

% Last Modified by GUIDE v2.5 20-Sep-2015 21:03:47

% Begin initialization code - DO NOT EDIT
import java.net.Socket
import java.io.*

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI1_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI1 is made visible.
function GUI1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI1 (see VARARGIN)

% Choose default command line output for GUI1
handles.output = hObject;
details = varargin{1};
handles.ip = details{1,1};
if(length(str2num(details{2,1}))>0)
    handles.port = str2num(details{2,1});
else
    handles.port = (details{2,1});
end
% Update handles structure
set(handles.connection,'String',strcat('Connected to:',handles.ip));
address = java.net.InetAddress.getLocalHost ;
IPaddress = char(address.getHostAddress);

set(handles.myIP,'String',strcat('Matlab IP:',IPaddress));
guidata(hObject, handles);

% UIWAIT makes GUI1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in callSwitchBoardGUI_Callback.
%function callGUI3_Callback(hObject, eventdata, handles)
%close all
%changeGui(handles.ip, handles.port,'GUI3')
%GUI3({handles.ip; num2str(handles.port)});
% hObject    handle to callSwitchBoardGUI_Callback (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function callManifoldGUI_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUITestingManifolds')
MenuManifold({handles.ip; num2str(handles.port)});


% --- Executes on button press in ManifoldGui.
%function ManifoldGui_Callback(hObject, eventdata, handles)
%close all
%changeGui(handles.ip, handles.port,'GUI2')
%GUI2({handles.ip; num2str(handles.port)});
% hObject    handle to ManifoldGui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in callGUI2Prog.
function callGUI2Prog_Callback(hObject, eventdata, handles)
% hObject    handle to callGUI2Prog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in callGUI4.
function callGUI4_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUI3')
GUI4({handles.ip; num2str(handles.port)});
% hObject    handle to callGUI4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function callSwitchBoardGUI_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'GUITestingSwitchboards')
MenuSwitchboard({handles.ip; num2str(handles.port)});

% --- Executes during object creation, after setting all properties.
function connection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to connection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in showAbout.
function showAbout_Callback(hObject, eventdata, handles)
close all
changeGui(handles.ip, handles.port,'About');
About({handles.ip; num2str(handles.port)});
