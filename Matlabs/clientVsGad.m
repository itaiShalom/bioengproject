% CLIENT connect to a server and read a message
%
% Usage - message = client(host, port, number_of_retries)
function [] = clientVsGad(host, port,manifold1,manifold2,sliders,sendSliders,openMessage)

import java.net.Socket
import java.io.*


number_of_retries = -1; % set to -1 for infinite
host
port

retry        = 0;
mySocket = [];

while true
    
    retry = retry + 1;
    if ((number_of_retries > 0) && (retry > number_of_retries))
        fprintf(1, 'Too many retries\n');
        break;
    end
    
    try
        %{
            fprintf(1, 'Retry %d connecting to %s:%d\n', ...
                    retry, host, port);
        %}
        % throws if unable to connect
        disp('Attempt to connect')
        mySocket = Socket(host, port);
        address = java.net.InetAddress.getLocalHost ;
        IPaddress = char(address.getHostAddress);
        % get a buffered data input stream from the socket
        input_stream   = mySocket.getInputStream;
        d_input_stream = DataInputStream(input_stream);
        output_stream   = mySocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);
        
        
        
        
        fprintf(1, 'Connected to server\n');
        sendMessage( d_output_stream,openMessage);
        % read data from the socket - wait a short time first
        
        message = readMessage( input_stream );
        if(strcmp(message,'Ready'))
            sendMessage( d_output_stream,'Manifold1');
            readMessage(input_stream);
           if (~isempty(manifold1))
            sendMessage( d_output_stream,manifold1 );
            
            readMessage(input_stream);
           end
            sendMessage( d_output_stream,'End');
            
            
            %%%%%%%%%%%%%%%%
            message = readMessage( input_stream );
            if(strcmp(message,'Ready'))
                sendMessage( d_output_stream,'Manifold2');
                readMessage(input_stream);
                
           if (~isempty(manifold2))
            sendMessage( d_output_stream,manifold2 );
            readMessage(input_stream);
           end
                sendMessage( d_output_stream,'End');
                
                
                %%%%%%%%%%%%%%%%
                okMessage = readMessage( input_stream );
               if (sendSliders ==true)
                sendMessage( d_output_stream,'Sliders');
                readMessage(input_stream);
                for i=1:length(sliders)
               % for i=length(sliders):-1:1
                    sendMessage( d_output_stream,sliders{i} );
                    readMessage(input_stream);
                end
                sendMessage( d_output_stream,'End');
                readMessage(input_stream);
               end
                sendMessage( d_output_stream,'Done');
                  readMessage(input_stream);
                %}
                % cleanup
            end
        end
        mySocket.close;
        disp('session ended')
        break;
        
    catch
        if ~isempty(mySocket)
            mySocket.close;
            disp('session ended')
        end
        disp('Falid to connect...')
        % pause before retrying
        return;
    end
end
end