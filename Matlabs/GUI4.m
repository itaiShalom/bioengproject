function varargout = GUI4(varargin)
% GUI4 MATLAB code for GUI4.fig
%      GUI4, by itself, creates a new GUI4 or raises the existing
%      singleton*.
%
%      H = GUI4 returns the handle to a new GUI4 or the handle to
%      the existing singleton*.
%
%      GUI4('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI4.M with the given input arguments.
%
%      GUI4('Property','Value',...) creates a new GUI4 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI4_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI4_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI4

% Last Modified by GUIDE v2.5 15-Jun-2014 17:08:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @GUI4_OpeningFcn, ...
    'gui_OutputFcn',  @GUI4_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI4 is made visible.
function GUI4_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI4 (see VARARGIN)

% Choose default command line output for GUI4
handles.output = hObject;
global runLoop ;
runLoop = true;
global pauseLoop;
pauseLoop =false;
global cameraProcess;
cameraProcess= false;
global fig
handles.colors = {'Red';'Green';'Blue'};
handles. allTests ={};
handles.allIterationTests = {};
handles.waitTime = 45;
handles.waitTime2 = 10;
handles.numOfCombs=4;
handles.ratings = {handles.RateComb1,handles.RateComb2,handles.RateComb3,handles.RateComb4};
handles.c1 = {handles.c1o1, handles.c1o2,handles.c1o3,handles.c1o4,handles.c1o5,handles.c1o6,handles.c1o7,handles.c1o8,handles.c1o9,handles.c1o10};
handles.c2 = {handles.c2o1, handles.c2o2,handles.c2o3,handles.c2o4,handles.c2o5,handles.c2o6,handles.c2o7,handles.c2o8,handles.c2o9,handles.c2o10};
handles.c3 = {handles.c3o1, handles.c3o2,handles.c3o3,handles.c3o4,handles.c3o5,handles.c3o6,handles.c3o7,handles.c3o8,handles.c3o9,handles.c3o10};
handles.c4 = {handles.c4o1, handles.c4o2,handles.c4o3,handles.c4o4,handles.c4o5,handles.c4o6,handles.c4o7,handles.c4o8,handles.c4o9,handles.c4o10};
handles.allComb={handles.c1,handles.c2,handles.c3,handles.c4};
handles.allTables = {handles.c1Table,handles.c2Table,handles.c3Table,handles.c4Table};
handles.allAxes = {handles.Comb1,handles.Comb2,handles.Comb3,handles.Comb4};
details = varargin{1};
handles.ip = details{1,1};
if(length(str2num(details{2,1}))>0)
    handles.port = str2num(details{2,1});
else
    handles.port = (details{2,1});
end
%{
handles.ip = varargin{1};

handles.port = varargin{2};
%}
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI4 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI4_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function initializeScreen(handles)
for z = 1:length(handles.ratings)
    set(handles.ratings{z},'String','');
    axes(handles.allAxes{z});
    imshow('loading.bmp');
    set(handles.allTables{z},'data',[handles.colors {'';'';''} {'';'';''}]);
end

% --- Executes on button press in picture.
function picture_Callback(hObject, eventdata, handles)
initializeScreen(handles);
colors = {'Red';'Green';'Blue'};
%{
 axes(handles.image);
imshow('loading.bmp');
[~,~,msg ,~]= readPic(handles.ip, handles.port);
 fid = fopen('c0.bmp', 'w');
 fprintf(fid, '%s\r\n', msg);
 fclose(fid);
 axes(handles.image);
 [ originalVals,p ] = avarageRGB( imread('c0.bmp') );
 set(handles.orgTable,'data',[colors originalVals]);
imshow(p);
return
%}
[ originalVals,~ ] = avarageRGB( imread('purple.bmp') );
set(handles.orgTable,'data',[colors originalVals]);
axes(handles.image);
imshow('purple.bmp');
randomizeArray(handles.c1)
randomizeArray(handles.c2)
randomizeArray(handles.c3)
randomizeArray(handles.c4)
bestDistance=[];
totalTimes=[];
set(handles.orgTable,'data',[colors originalVals]);
j = 1;
global runLoop
global pauseLoop
global fig
while runLoop
    set(handles.iter,'String',strcat('Iteration number:',num2str(j)));
    i=1;
    while i<=handles.numOfCombs
        
        myTest =      proccessTest(handles,i,j,originalVals);
        
        i=i+1;
        if (pauseLoop)
            choice = questdlg('Paused pressed, Continue?', ...
                'Process', ...
                'Yes','No','Yes');
            if (strcmp(choice,'No'))
                return
            else
                pauseLoop =false;
                i = i-1;
                continue
            end
        end
        handles. allTests{length(handles.allTests)+1} = myTest;
    end
    iterationRating ={};
    index = 1;
    for k=(1+(j-1)*4) : (4+(j-1)*4)
        iterationRating{index} = handles. allTests{k};
        index = index+1;
    end
    [iterationRating] = mySort(iterationRating);
    for z = 1:length(handles.ratings)
        for zz=1:length(iterationRating)
            if (iterationRating{zz}.combNum==z)
                set(handles.ratings{z},'String',num2str(zz));
            end
        end
    end
    
    if (j==1)
        fig = figure(1);
        xlabel('Number of iterations');
        ylabel('Difference RGB from original image');
        title('Genetic algorithm results')
        grid on
        hold on
    end
    bestDistance = [bestDistance iterationRating{1}.delta];
    totalTimes = [totalTimes j];
    figure(fig);
    plot(totalTimes,bestDistance,'r*','LineWidth',2,'LineStyle','-')
    h= msgbox('Calculating new combinations');
    pause(handles.waitTime2);
    try
        delete(h)
    catch
    end
    if (runLoop==true)
        Replacement(iterationRating{1}.testPins,iterationRating{2}.testPins,iterationRating{3}.testPins);
        Replacement(iterationRating{2}.testPins,iterationRating{1}.testPins,iterationRating{4}.testPins);
        randomizeBit( iterationRating{1}.testPins);
        randomizeBit( iterationRating{2}.testPins);
        j = j+1;
        
        initializeScreen(handles)
        
        
    end
end
clientVsGad(handles.ip, handles.port, '','','',false,'GUI3 Config');
runLoop = true;
[allTestSorted] = mySort(handles. allTests);
for x = 1:length(handles. allTests)
    if (handles. allTests{x}.delta ==allTestSorted{1}.delta)
        break;
    end
end

co = 1;
while (x-4>0)
    x = x-4;
    co= co+1;
end
mini = allTestSorted{1}.delta;
maxi = allTestSorted{length(allTestSorted)}.delta;
rady = (maxi+mini)/4;

%radx = round((length(newArr)+1)/4);
radx = 0.5;
th = 0:2*pi/40:2*pi;
xunit = radx * cos(th) + co;
yunit = rady * sin(th) + allTestSorted{1}.delta;
plot(xunit, yunit);
saveas(fig,'Images\Output.fig');
fig2= figure(2);
figure(fig2);
imshow(allTestSorted{1}.testImage);
for l=1:length(handles.ratings)
    set(handles.ratings{l},'String',num2str(l));
    setPinsFinal(handles.allComb{l}, allTestSorted{l}.bits);
    axes(handles.allAxes{l});
    
    imshow(allTestSorted{l}.testImage);
    [ vals ] = avarageRGB( allTestSorted{l}.testImageMat );
    deltac ={0;0;0};
    for k = 1:3
        deltac {k}=abs(vals{k}-originalVals{k});
    end
    set(handles.allTables{l},'data',[handles.colors vals deltac]);
end
pause(5);
close( fig)
close (fig2)
function []= setPinsFinal(copyTo, copyFrom)
for i=1:length(copyFrom)
    if (copyFrom(i)==1)
        set(copyTo{i},'Value',1);
        set(copyTo{i},'BackgroundColor','green');
    else
        set(copyTo{i},'Value',0);
        set(copyTo{i},'BackgroundColor','red');
    end
end

function [myTest] = proccessTest(handles,i,loopNum,originalVals)
clientVsGad(handles.ip, handles.port, createMan(1,handles.allComb{i}),createMan(2,handles.allComb{i}),'',false,'GUI3 Config');
h= msgbox(strcat('Creating combination -',num2str(i),'-','Loop number-',num2str(loopNum)));
pause(handles.waitTime);
try
    delete(h)
catch
end
global cameraProcess
%name = strcat('c',num2str(round(rand(1)*10)+1),'.bmp');
h= msgbox(strcat('Combination set, Taking picture -',num2str(i),'-','Loop number-',num2str(loopNum)));
cameraProcess = true;

name = strcat('Images\','c-',num2str(i),'-','Loop number-',num2str(loopNum),'.bmp');
[~,~,msg ,~]= readPic(handles.ip, handles.port);
fid = fopen(name, 'w');
fprintf(fid, '%s\r\n', msg);
fclose(fid);

axes(handles.allAxes{i});

imshow(name);
cameraProcess = false;
global pauseLoop;
if(pauseLoop)
    myTest =Test;
    try
    delete(h);
catch
end
    return;
end
[ vals ] = avarageRGB( imread(name) );
deltac ={0;0;0};
for k = 1:3
    deltac {k}=abs(vals{k}-originalVals{k});
end
set(handles.allTables{i},'data',[handles.colors vals deltac]);
myTest =Test;
myTest.testRGB = vals;
myTest.testRGB_Delta = deltac;
myTest.testImage = name;
myTest.testImageMat = imread(name);
myTest.testPins = handles.allComb{i};
myTest.combNum = i;
myTest = createBitArray(myTest);
myTest = calcDelta (myTest);
try
    delete(h);
catch
end

%{
[za,zaa,msg ,msg2]= readPic(handles.ip, handles.port);
fid = fopen('myTemp.bmp', 'w');
fprintf(fid, '%s\r\n', msg);
fclose(fid);
axes(handles.image);
imshow('myTemp.bmp');
%}

%mypic = mypic(55:length(mypic1));
% mypic = reshape(mypic,320,240,3);
%imwrite(mypic, 'picture.jpg');
% imshow(mypic);
%[picChar, picbyte] = GUI4TakePic(handles.ip,handles. port);


%picbyte = reshape(picbyte);
%imwrite(picbyte, 'pic.jpg')

%image = typecast(picbyte,'png' );

%axes(handles.ax)

% hObject    handle to picture (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in callGui2.
function callGui2_Callback(hObject, eventdata, handles)
% hObject    handle to callGui2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

changeGui(handles.ip, handles.port,'GUI2')
GUI2({handles.ip; num2str(handles.port)});

% --- Executes on button press in callGui3.
function callGui3_Callback(hObject, eventdata, handles)
% hObject    handle to callGui3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

changeGui(handles.ip, handles.port,'GUI3')
GUI3({handles.ip; num2str(handles.port)});


% --- Executes on button press in c4o1.
function c4o1_Callback(hObject, eventdata, handles)
% hObject    handle to c4o1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o1


% --- Executes on button press in c4o2.
function c4o2_Callback(hObject, eventdata, handles)
% hObject    handle to c4o2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o2


% --- Executes on button press in c4o3.
function c4o3_Callback(hObject, eventdata, handles)
% hObject    handle to c4o3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o3


% --- Executes on button press in c4o4.
function c4o4_Callback(hObject, eventdata, handles)
% hObject    handle to c4o4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o4


% --- Executes on button press in c4o5.
function c4o5_Callback(hObject, eventdata, handles)
% hObject    handle to c4o5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o5


% --- Executes on button press in c4o6.
function c4o6_Callback(hObject, eventdata, handles)
% hObject    handle to c4o6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o6


% --- Executes on button press in c4o7.
function c4o7_Callback(hObject, eventdata, handles)
% hObject    handle to c4o7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o7


% --- Executes on button press in c4o8.
function c4o8_Callback(hObject, eventdata, handles)
% hObject    handle to c4o8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o8


% --- Executes on button press in c4o9.
function c4o9_Callback(hObject, eventdata, handles)
% hObject    handle to c4o9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o9


% --- Executes on button press in c4o10.
function c4o10_Callback(hObject, eventdata, handles)
% hObject    handle to c4o10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c4o10


% --- Executes on button press in c2o1.
function c2o1_Callback(hObject, eventdata, handles)
% hObject    handle to c2o1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o1


% --- Executes on button press in c2o2.
function c2o2_Callback(hObject, eventdata, handles)
% hObject    handle to c2o2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o2


% --- Executes on button press in c2o3.
function c2o3_Callback(hObject, eventdata, handles)
% hObject    handle to c2o3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o3


% --- Executes on button press in c2o4.
function c2o4_Callback(hObject, eventdata, handles)
% hObject    handle to c2o4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o4


% --- Executes on button press in c2o5.
function c2o5_Callback(hObject, eventdata, handles)
% hObject    handle to c2o5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o5


% --- Executes on button press in c2o6.
function c2o6_Callback(hObject, eventdata, handles)
% hObject    handle to c2o6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o6


% --- Executes on button press in c2o7.
function c2o7_Callback(hObject, eventdata, handles)
% hObject    handle to c2o7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o7


% --- Executes on button press in c2o8.
function c2o8_Callback(hObject, eventdata, handles)
% hObject    handle to c2o8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o8


% --- Executes on button press in c2o9.
function c2o9_Callback(hObject, eventdata, handles)
% hObject    handle to c2o9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o9


% --- Executes on button press in c2o10.
function c2o10_Callback(hObject, eventdata, handles)
% hObject    handle to c2o10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c2o10



% --- Executes on button press in c1o1.
function c1o1_Callback(hObject, eventdata, handles)
% hObject    handle to c1o1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o1


% --- Executes on button press in c1o2.
function c1o2_Callback(hObject, eventdata, handles)
% hObject    handle to c1o2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o2


% --- Executes on button press in c1o3.
function c1o3_Callback(hObject, eventdata, handles)
% hObject    handle to c1o3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o3


% --- Executes on button press in c1o4.
function c1o4_Callback(hObject, eventdata, handles)
% hObject    handle to c1o4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o4


% --- Executes on button press in c1o5.
function c1o5_Callback(hObject, eventdata, handles)
% hObject    handle to c1o5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o5


% --- Executes on button press in c1o6.
function c1o6_Callback(hObject, eventdata, handles)
% hObject    handle to c1o6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o6


% --- Executes on button press in c1o7.
function c1o7_Callback(hObject, eventdata, handles)
% hObject    handle to c1o7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o7


% --- Executes on button press in c1o8.
function c1o8_Callback(hObject, eventdata, handles)
% hObject    handle to c1o8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o8


% --- Executes on button press in c1o9.
function c1o9_Callback(hObject, eventdata, handles)
% hObject    handle to c1o9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o9


% --- Executes on button press in c1o10.
function c1o10_Callback(hObject, eventdata, handles)
% hObject    handle to c1o10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c1o10



% --- Executes on button press in c3o10.
function c3o10_Callback(hObject, eventdata, handles)
% hObject    handle to c3o10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o10


% --- Executes on button press in c3o9.
function c3o9_Callback(hObject, eventdata, handles)
% hObject    handle to c3o9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o9


% --- Executes on button press in c3o8.
function c3o8_Callback(hObject, eventdata, handles)
% hObject    handle to c3o8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o8


% --- Executes on button press in c3o7.
function c3o7_Callback(hObject, eventdata, handles)
% hObject    handle to c3o7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o7


% --- Executes on button press in c3o6.
function c3o6_Callback(hObject, eventdata, handles)
% hObject    handle to c3o6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o6


% --- Executes on button press in c3o5.
function c3o5_Callback(hObject, eventdata, handles)
% hObject    handle to c3o5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o5


% --- Executes on button press in c3o4.
function c3o4_Callback(hObject, eventdata, handles)
% hObject    handle to c3o4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o4


% --- Executes on button press in c3o3.
function c3o3_Callback(hObject, eventdata, handles)
% hObject    handle to c3o3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o3


% --- Executes on button press in c3o2.
function c3o2_Callback(hObject, eventdata, handles)
% hObject    handle to c3o2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o2


% --- Executes on button press in c3o1.
function c3o1_Callback(hObject, eventdata, handles)
% hObject    handle to c3o1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of c3o1

function [] = randomizeArray(arr)
for i=1:length(arr)
    p =round(rand(1));
    if p==1
        set(arr{i},'BackgroundColor','green');
        set(arr{i},'Value',p);
    else
        set(arr{i},'BackgroundColor','red');
        set(arr{i},'Value',p);
    end
end

function[] = randomizeBit(arr)
bitNum = round(rand(1)*9 + 1);
if (get(arr{bitNum},'Value')==0)
    set(arr{bitNum},'Value',1);
    set(arr{bitNum},'BackgroundColor','green');
else
    set(arr{bitNum},'Value',0);
    set(arr{bitNum},'BackgroundColor','red');
end

function[] = Replacement(arr1,arr2,arr3)

for i=1:5
    set(arr3{i},'Value', get(arr1{i},'Value') );
    set(arr3{i},'BackgroundColor',get(arr1{i},'BackgroundColor')) ;
end
for i=6:10
    set(arr3{i},'Value', get(arr2{i},'Value') );
    set(arr3{i},'BackgroundColor',get(arr2{i},'BackgroundColor')) ;
end

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
h= msgbox('wait');
pause(2)
delete(h)

randomizeBit(handles.c1)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function [arr] = mySort(arr)
for i = 1:length(arr)
    for j=1:length(arr)
        if (arr{i}.delta<arr{j}.delta)
            temp = arr{i};
            arr{i} = arr{j};
            arr{j} = temp;
        end
    end
end

% --- Executes on button press in stopProcess.
function stopProcess_Callback(hObject, eventdata, handles)
global runLoop;
runLoop= false;

% hObject    handle to stopProcess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in PausePro.
function PausePro_Callback(hObject, eventdata, handles)
global pauseLoop;
global cameraProcess;
while cameraProcess
    puase(1);
end
clientVsGad(handles.ip, handles.port, '','','',false,'GUI3 Config');
pauseLoop = true;

% hObject    handle to PausePro (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
