function [Success] = sayHello(host,port,bSendRTC  )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


import java.net.Socket
import java.io.*


number_of_retries = -1; % set to -1 for infinite
host
port

retry        = 0;
mySocket = [];
Success = false;
while true
 
    retry = retry + 1;
    if ((number_of_retries > 0) && (retry > number_of_retries))
        fprintf(1, 'Too many retries\n');
        
        break;
    end
    
    try
        %{
            fprintf(1, 'Retry %d connecting to %s:%d\n', ...
                    retry, host, port);
        %}
        % throws if unable to connect
        disp('Attempt to connect')
        mySocket = Socket(host, port);
        address = java.net.InetAddress.getLocalHost ;
        IPaddress = char(address.getHostAddress);
        % get a buffered data input stream from the socket
        input_stream   = mySocket.getInputStream;
        % d_input_stream = DataInputStream(input_stream);
        output_stream   = mySocket.getOutputStream;
        d_output_stream = DataOutputStream(output_stream);     
        fprintf(1, 'Connected to server\n');
        sendMessage( d_output_stream,strcat('Introduction:', IPaddress) );
        % read data from the socket - wait a short time first   
        message = readMessage( input_stream );
        if (strcmp(message,'OK')==0)
            msgbox(strcat('Expected to get ok but received-',message),'Protocol error')
        end
        if (bSendRTC == true)
            sendMessage( d_output_stream,'SendRTC');
            message = readMessage( input_stream ); 
            if (strcmp(message,'OK')==0)
                 msgbox(strcat('Expected to get ok but received-',message),'Protocol error')
            end

            timeStamp = FunctionClass('getClock','');
            sendMessage( d_output_stream,timeStamp);
            if (strcmp(readMessage( input_stream ) ,'OK')==0)
                msgbox(strcat('Expected to get ok but received-',message),'Protocol error')
            end
        end
        %%%
        sendMessage( d_output_stream,'GUIMainManu' );
          
        if (strcmp(readMessage( input_stream ),'OK')==0)
             msgbox(strcat('Expected to get ok but received-',message),'Protocol error')
        end
        mySocket.close;
        disp('session ended')
        Success = true;
        break;
        
    catch
        if ~isempty(mySocket)
            mySocket.close;
            disp('session ended')
        end
        disp('Falid to connect...')
        % pause before retrying
        return;
    end
end
